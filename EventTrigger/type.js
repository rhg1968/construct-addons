"use strict";

{
	const PLUGIN_CLASS = SDK.Plugins.RGBZ_EventTrigger;
	
	PLUGIN_CLASS.Type = class RGBZ_EventTriggerType extends SDK.ITypeBase
	{
		constructor(sdkPlugin, iObjectType)
		{
			super(sdkPlugin, iObjectType);
		}
	};
}