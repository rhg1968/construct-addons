"use strict";

{
	const C3 = self.C3;
	C3.Plugins.Rex_WaitEvent.Instance = class Rex_WaitEventInstance extends C3.SDKInstanceBase
	{
		constructor(inst, properties)
		{
			super(inst);
			this.events = {};
			this.exp_EventName = "";
			this.exp_Tag = null;
			if (properties)
			{
			}
		}
		Release()
		{
			super.Release();
		}
		SaveToJson()
		{
			return {
				"evts": this.events,
				"ename": this.exp_EventName,
				"tag": this.exp_Tag,
			};
		}
		LoadFromJson(o)
		{
			this.events = o["evts"];
			this.exp_EventName = o["ename"];
			this.exp_Tag = o["tag"];
		}
		eventExist(event_name, tag)
		{
			return (this.events[tag] != null) && (this.events[tag][event_name] != null);
		}
		runTrigEvent(method, tag, event_name)
		{
			this.exp_EventName = event_name;
			this.exp_Tag = tag;
			this.Trigger(method);
			this.exp_Tag = null;
		}
		GetDebuggerProperties()
		{
			var cur_event_list = [];
			var tag, name, _evts;
			for (tag in this.events)
			{
				_evts = this.events[tag];
				for (name in _evts)
				{
					cur_event_list.push({name: tag+"-"+name, value: ""});
				}
			}
			
			return [{
				title: "WaitEvent",
				properties: cur_event_list
			}];
		}
	};
}