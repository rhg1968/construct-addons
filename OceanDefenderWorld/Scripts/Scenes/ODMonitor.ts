import ODTrash from './../GameObjects/ODTrash';
import ODState from './../Enums/ODState';
import ODMissile from './../GameObjects/ODMissile';
import ODBaseEnum from './../Enums/ODBaseEnum';
import MonitorClickEvent from './../Models/MonitorClickEvent';
import ODRedExplosion from '../GameObjects/ODRedExplosion';
import ODGameInfo from './../Models/ODGameInfo';
import RadarEvent from './../Models/RadarEvent';
import RadarDetailEvent from './../Models/RadarDetailEvent';
import ODWhiteExplosion from './../GameObjects/ODWhiteExplosion';
import GameEvent from './../Models/GameEvent';

class ODMonitor extends Phaser.Scene
{
    private activeRadar:string;
    private trashPool:Phaser.GameObjects.Group;
    private trashTrackingPool:Phaser.GameObjects.Group;
    private missilePool:Phaser.GameObjects.Group;
    private explosionPool:Phaser.GameObjects.Group;
    private explosionTrackingPool:Phaser.GameObjects.Group;
    private cloudPool:Phaser.GameObjects.Group;
    private fishPool:Phaser.GameObjects.Group;
    private particleManager:Phaser.GameObjects.Particles.ParticleEmitterManager;
    private base:Phaser.GameObjects.Sprite;
    private gun:Phaser.GameObjects.Sprite;
    private statictv:Phaser.GameObjects.Sprite;
    private ignoreInput:boolean;
    private fadeTime:number;
    private scoreText:Phaser.GameObjects.Text;
    private oceanoutterbar:Phaser.GameObjects.Sprite;
    private oceaninnerbar:Phaser.GameObjects.Sprite;
    private oceaninnerbardefault:number;
    private isPaused:boolean;
    private pauseButton:Phaser.GameObjects.Sprite;

    constructor(public key: string, public x:number, public y:number, public displayWidth:number, 
        public displayHeight:number, fadeTime:number) {
        super({
            key
        });

        this.activeRadar = '';
        this.ignoreInput = false;
        this.fadeTime = fadeTime;
        this.isPaused = false;
    }

    preload():void
    {
        this.trashPool = this.add.group({
            maxSize: 20,
            defaultKey: 'ODImages',
            defaultFrame: 'trashcollection-000.png',
            classType: ODTrash as any,
            createCallback: (trash:ODTrash)=>{},
            removeCallback: (trash:ODTrash)=>{},
            runChildUpdate: false
        });

        this.trashTrackingPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'trashcollection-000.png',
            classType: ODTrash as any,
            createCallback: (trash:ODTrash)=>{
                trash.ignore=true;
            },
            removeCallback: (trash:ODTrash)=>{},
            runChildUpdate: false
        });

        this.missilePool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'greenmissile.png',
            classType: ODMissile as any,
            createCallback: (trash:ODMissile)=>{},
            removeCallback: (trash:ODMissile)=>{},
            runChildUpdate: false
        });

        this.explosionPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'redexplode0004.png',
            classType: ODRedExplosion as any,
            createCallback: (trash:ODRedExplosion)=>{},
            removeCallback: (trash:ODRedExplosion)=>{},
            runChildUpdate: false
        });

        this.explosionTrackingPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'explodewhite0003.png',
            classType: ODWhiteExplosion as any,
            createCallback: (trash:ODRedExplosion)=>{},
            removeCallback: (trash:ODRedExplosion)=>{},
            runChildUpdate: false
        });

        this.cloudPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'cloud1.png',
            classType: Phaser.GameObjects.Sprite as any,
            createCallback: (trash:any)=>{},
            removeCallback: (trash:any)=>{},
            runChildUpdate: false
        });

        this.fishPool = this.add.group({
            maxSize:5,
            defaultKey: 'ODImages',
            defaultFrame: 'fish1.png',
            classType: Phaser.GameObjects.Sprite as any,
            createCallback: (trash:any)=>{},
            removeCallback: (trash:any)=>{},
            runChildUpdate: false
        });
    }

    create():void
    {
        this.events.on('resume', (data:any)=>{
            if (ODGameInfo.instance.world.currLevel>0 && this.pauseButton && this.pauseButton.active) {
                this.pauseButton.setVisible(true);
                this.isPaused=false;
            }
        });

        this.cameras.main.setViewport(this.x, this.y, this.displayWidth, this.displayHeight);

        (this.add as any).sky(this.displayWidth/2.0,this.displayHeight/2.0).setDisplaySize(this.displayWidth, this.displayHeight);

        this.cloudPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, (0.5769*this.displayWidth), (0.5784*this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.25,0.20),
            (new Phaser.GameObjects.Sprite(this, (0.3077*this.displayWidth), (0.1735*this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.20,0.25),
            (new Phaser.GameObjects.Sprite(this, (0.1538*this.displayWidth), (0.3470*this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.15,0.15),
            (new Phaser.GameObjects.Sprite(this, (0.7692*this.displayWidth), (0.1735*this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.20,0.15),
            (new Phaser.GameObjects.Sprite(this, (0.3846*this.displayWidth), (0.2892*this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.25,0.20),
            (new Phaser.GameObjects.Sprite(this, (0.7692*this.displayWidth), (0.3470*this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.35,0.20),
            (new Phaser.GameObjects.Sprite(this, (0.2692*this.displayWidth), (0.4627*this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.15,0.20),
            (new Phaser.GameObjects.Sprite(this, (0.7692*this.displayWidth), (0.4916*this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.15,0.15)
        ], true);
        
        this.base=(this.add as any).yellowbase(this.displayWidth/2.0, this.displayHeight-(0.1420*this.displayHeight)).setScale(0.4).setDepth(2);
        this.gun=(this.add as any).yellowgun(this.base.x, this.base.y-7).setScale(0.25).setDepth(2).setAngle(0);

        this.SetBaseAndGunColor();
        
        (this.add as any).ocean(this.displayWidth/2.0, this.displayHeight-(0.0825*this.displayHeight)).setDisplaySize(this.displayWidth-20, (0.0968*this.displayHeight)).setDepth(1);

        this.fishPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, (0.3077*this.displayWidth), (0.8965*this.displayHeight), 'ODImages', 'fish1.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.5000*this.displayWidth), (0.9254*this.displayHeight), 'ODImages', 'fish2.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.6538*this.displayWidth), (0.8965*this.displayHeight), 'ODImages', 'fish3.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.7308*this.displayWidth), (0.9254*this.displayHeight), 'ODImages', 'fish4.png')).setScale(0.35).setDepth(1)
        ], true);

        this.tweens.timeline({
            targets: [this.base],
            loop: -1,
            tweens: [
                {
                    angle: -25,
                    ease: 'Sine.easeInOut',
                    duration: 3000
                },
                {
                    offset: 3000,
                    angle: 25,
                    ease: 'Sine.easeInOut',
                    duration: 6000
                },
                {
                    offset: 9000,
                    angle: 0,
                    ease: 'Sine.easeInOut',
                    duration: 3000
                }
            ]
        });

        //Add monitor graphic and set to interactive
        (this.add as any).tvmonitorempty(0, 0).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(2).setInteractive().on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, (pointer:any, event:any, event2:any, event3:any, event4:any)=>{
            if (!this.ignoreInput) {
                this.ProcessClick(pointer);
            }
        });

        this.statictv = ((this.add as any).statictv(0,0) as Phaser.GameObjects.Sprite).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(3).setActive(false).setVisible(false);

        this.oceanoutterbar = ((this.add as any).oceanouterbar((this.displayWidth/2.0)-((this.displayWidth/260.0) * 35), this.displayHeight-((this.displayHeight/345.8) * 18)) as Phaser.GameObjects.Sprite).setDisplaySize((this.displayWidth/260.0)*70.84, (this.displayHeight/345.8)*10.08).setDepth(1);
        (((this.add)as any).oceanmiddlebar(this.oceanoutterbar.x, this.oceanoutterbar.y)as Phaser.GameObjects.Sprite).setDisplaySize(this.oceanoutterbar.displayWidth, this.oceanoutterbar.displayHeight).setDepth(1);
        this.oceaninnerbar = ((this.add as any).oceaninnerbar(this.oceanoutterbar.x+2, this.oceanoutterbar.y+2) as Phaser.GameObjects.Sprite).setDisplaySize(this.oceanoutterbar.displayWidth-4, this.oceanoutterbar.displayHeight-4).setDepth(1);

        this.oceaninnerbardefault = this.oceaninnerbar.displayWidth;

        ((this.add as any).shield(100 * (this.displayWidth/260.0), 30 * (this.displayHeight/345.8)) as Phaser.GameObjects.Sprite).setScale(0.35 * (this.displayWidth/260.0)).setDepth(2);
        
        this.scoreText = this.add.text(125 * (this.displayWidth/260.0), 15 * (this.displayHeight/345.8), ODGameInfo.instance.world.currPoints.toString(), {
            fontFamily: 'ethnocentric_rg',
            fontSize: 24* (this.displayWidth/260.0),
            color: '#FFCC02'
        });

        if (ODGameInfo.instance.world.currLevel>0) {
            this.pauseButton = ((this.add as any).pausebutton(this.displayWidth as number - 25,25) as Phaser.GameObjects.Sprite).setScale(0.30).setDepth(2).setInteractive();
            
            this.pauseButton.on('pointerdown', (pointer:any)=>{
                this.PauseButtonClick(pointer);
            });
        }

        this.SetUpParticalSystem();

        this.registry.events.on('changedata', (parent:any, key:string, data:RadarEvent)=>{
            switch(key) {
                case 'RadarUpdateCycle1':
                    this.ProcessRadarUpdateCycle(data);
                    break;
                case 'RadarUpdateCycle2':
                    this.ProcessRadarUpdateCycle(data);
                   break;
                case 'RadarUpdateCycle3':
                    this.ProcessRadarUpdateCycle(data);
                    break;
                case 'ActiveRadar':
                    this.ProcessActiveRadar(data);
                    break;
                case 'TrashAtWater':
                    this.ProcessTrashAtWater(data);
                    break;
                case 'TrashDestroyed':
                    this.ProcessTrashDestroyed(data);
                    break;
                case 'MissileInFinalPosition':
                    this.ProcessMissileInFinalPosition(data);
                    break;
                case 'SetMonitorExplosion':
                    this.ProcessSetMonitorExplosion(data);
                    break;
                case 'StartCoolDown':
                    this.ProcessStartCoolDown(data);
                    break;
                case 'EndCoolDown':
                    this.ProcessEndCoolDown(data);
                    break;
            }
        });

        this.cameras.main.fadeIn(this.fadeTime, ODGameInfo.instance.world.fadeColor.red, ODGameInfo.instance.world.fadeColor.green, ODGameInfo.instance.world.fadeColor.blue);
    }

    update(time:number, dt:number):void
    {
        Phaser.Actions.Call(this.trashPool.getChildren().filter((x:ODTrash)=>x.active), (trash:ODTrash)=>{
            trash.setRotation(trash.rotation + (trash.rotationDelta*(dt/1000.0)));
        } , this);

        let counter:number = 0;
        Phaser.Actions.Call(this.cloudPool.getChildren(), (cloud:Phaser.GameObjects.Sprite)=>{
            cloud.x += (((dt/1000)*8)+ (0.06*counter));

            if (cloud.x - (cloud.displayWidth/2.0) > this.displayWidth) {
                cloud.x = -cloud.displayWidth/2.0;
            }

            counter+=1;
        }, this);

        Phaser.Actions.Call(this.fishPool.getChildren(), (fish:Phaser.GameObjects.Sprite)=>{
            fish.x += ((dt/1000)*10);

            if (fish.x - (fish.displayWidth/2.0) > this.displayWidth) {
                fish.x = -fish.displayWidth/2.0;
            }
        }, this);
    }

    public SetIgnoreInputOn(ignore:boolean):void {
        this.ignoreInput = ignore;
    }

    public DemoCreateMissile():void
    {
        this.ProcessClick({worldX:Phaser.Math.RND.realInRange(30, this.displayWidth-30), worldY:Phaser.Math.RND.realInRange(50, this.gun.y-this.gun.height-20)});
    }

    private PauseButtonClick(pointer:any):void
    {
        this.isPaused = !this.isPaused;
        let ge:GameEvent = new GameEvent('GamePause', this.isPaused);
        this.pauseButton.setVisible(!this.isPaused);

        this.registry.set('GamePause', ge);
    }

    private ProcessClick(pointer:any):void {
        if (pointer.worldX < (0.0962*this.displayWidth) || pointer.worldX > (0.9038*this.displayWidth) 
            || pointer.worldY < (0.1301*this.displayHeight) || pointer.worldY > (0.7953*this.displayHeight)) {
            return;
        }

        let angRad:number = Phaser.Math.Angle.Between(pointer.worldX, pointer.worldY, this.gun.x, this.gun.y);
        
        let gunAngle:number = Phaser.Math.RadToDeg(angRad)-90;
        this.gun.setAngle(gunAngle);

        let clickEvent:MonitorClickEvent = new MonitorClickEvent(this.activeRadar);
        clickEvent.initialX = this.gun.x - (this.gun.displayHeight * Math.cos(angRad));
        clickEvent.initialY = this.gun.y - (this.gun.displayHeight * Math.sin(angRad));
        clickEvent.finalX = pointer.worldX;
        clickEvent.finalY = pointer.worldY;
        clickEvent.monitorHeight = this.displayHeight;
        clickEvent.monitorWidth = this.displayWidth;

        this.registry.set(this.activeRadar, clickEvent);
    }

    private SetBaseAndGunColor():void
    {
        let result:ODBaseEnum = ODBaseEnum.Yellow;

        if (this.activeRadar=='Radar2')
        {
            result = ODBaseEnum.Blue;
        }
        else if (this.activeRadar=='Radar3')
        {
            result = ODBaseEnum.Grey;
        }

        this.base.play('Base', false, result);
        this.gun.play('Gun', false, result);
    }

    private SetUpParticalSystem():void
    {
        this.particleManager = this.add.particles('ODImages', 'watersplash.png');
        
        for (let i=1; i<=3; i++) {
            this.particleManager.createEmitter({
                name: 'Radar' + i.toString(),
                x: this.displayWidth/2.0,
                y: this.displayHeight/2.0,
                angle: { min: 260, max: 280 },
                speed: { min: 100, max: 180 },
                gravityY: 850,
                accelerationY: -350,
                scale: 0.5,
                blendMode: Phaser.BlendModes.COLOR,
                maxParticles: 1000,
                lifespan: 600,
                radial: true,
                quantity: 50,
                on: false
            });
        }
    }

    private ProcessRadarUpdateCycle(data:RadarEvent):void {
        if (data.key==this.activeRadar)
        {
            data.missiles.forEach((red:RadarDetailEvent)=>{
                let foundMissile:ODMissile = (this.missilePool.getChildren() as Array<ODMissile>).find(x=>x.id==red.id && x.active);

                if (!foundMissile) {
                    foundMissile = this.missilePool.get(0,0) as ODMissile;
                    foundMissile.setScale(0.05);
                    foundMissile.id = red.id;
                    foundMissile.setODState(ODState.VisibleActive);
                    foundMissile.tag = data.key;
                }

                foundMissile.x = red.x * (this.displayWidth/red.displayWidth);
                foundMissile.y = red.y * (this.displayHeight/red.displayHeight);
            });

            data.trash.forEach((red:RadarDetailEvent)=>{
                let foundTrash:ODTrash = (this.trashPool.getChildren() as Array<ODTrash>).find(x=>x.id==red.id && x.active);

                if (!foundTrash) {
                    foundTrash = this.trashPool.get(0,0) as ODTrash;
                    foundTrash.play('Trash', false, Phaser.Math.RND.between(0,14));
                    foundTrash.rotationDelta = Phaser.Math.RND.realInRange(-5.0, 5.0);
                    foundTrash.setScale(0.25);
                    foundTrash.id = red.id;
                    foundTrash.setODState(ODState.VisibleActive);
                    foundTrash.tag = data.key;
                }

                foundTrash.x = red.x * (this.displayWidth/red.displayWidth);
                foundTrash.y = red.y * (this.displayHeight/red.displayHeight);
            });

            if (ODGameInfo.instance.world.currLevel>0) {
                let trashTackerExplosionNeeded:boolean = data.trashToClear < this.trashTrackingPool.getTotalUsed();

                //Clear out trash tracking pool
                (this.trashTrackingPool.getChildren() as Array<ODTrash>).forEach((trash:ODTrash)=>{trash.setODState(ODState.NotVisibleActive);});

                //Create new items on the screen
                let initX:number = 40;
                let initY:number = 60;

                for (let i=0;i<data.trashToClear;i++) {
                    let trash:ODTrash = this.trashTrackingPool.get(initX, initY) as ODTrash;
                    trash.setScale(0.20).setDepth(2).setODState(ODState.VisibleActive);

                    initX += 20;

                    if (initX > 150) {
                        initX = 40;
                        initY += 23;
                    }
                }

                if (trashTackerExplosionNeeded) {
                    let trackExplosion:ODWhiteExplosion = (this.explosionTrackingPool.get(initX, initY) as ODWhiteExplosion).setScale(0.20).setDepth(2);
                    trackExplosion.setODState(ODState.VisibleActive);
                    trackExplosion.play('WhiteExplode');
        
                    trackExplosion.on('animationcomplete', (animation:Phaser.Animations.Animation, frame:Phaser.Animations.AnimationFrame, gameObject:ODRedExplosion)=>{
                        gameObject.setODState(ODState.NotVisibleActive);
                    });
                }
            }

            this.oceaninnerbar.displayWidth = this.oceaninnerbardefault*data.oceanHealth;
        }
    }

    private ProcessStartCoolDown(data:RadarEvent):void {
        this.base.setAlpha(0.5);
        this.gun.setAlpha(0.5);
    }

    private ProcessEndCoolDown(data:RadarEvent):void {
        this.base.setAlpha(1.0);
        this.gun.setAlpha(1.0);
    }

    private ProcessSetMonitorExplosion(data:RadarEvent):void {
        data.missiles.forEach((detail:RadarDetailEvent)=>{
            let explosion:ODRedExplosion = (this.explosionPool.get(detail.x*(this.displayWidth/detail.displayWidth), detail.y*(this.displayHeight/detail.displayHeight)).setDepth(2).setScale(0.35).play('RedExplode') as ODRedExplosion);
            explosion.tag = data.key;
            explosion.id = detail.id;
            explosion.setODState(ODState.VisibleActive);
    
            explosion.on('animationcomplete', (animation:Phaser.Animations.Animation, frame:Phaser.Animations.AnimationFrame, gameObject:ODRedExplosion)=>{
                gameObject.setODState(ODState.NotVisibleActive);
            });
        });
    }

    private ProcessActiveRadar(data:RadarEvent):void {
        if (data.key!=this.activeRadar) {
            this.trashPool.getChildren().filter((x:ODTrash)=>x.active && x.tag==this.activeRadar).forEach((x:ODTrash)=>{
                x.setODState(ODState.NotVisibleActive);
            });

            this.missilePool.getChildren().filter((x:ODMissile)=>x.active && x.tag==this.activeRadar).forEach((x:ODMissile)=>{
                x.setODState(ODState.NotVisibleActive);
            });

            this.explosionPool.getChildren().filter((x:ODRedExplosion)=>x.active && x.tag==this.activeRadar).forEach((x:ODRedExplosion)=>{
                x.setODState(ODState.NotVisibleActive);
            });

            this.particleManager.emitters.getAll('name', this.activeRadar as any).forEach((emit:Phaser.GameObjects.Particles.ParticleEmitter)=>{
                emit.visible = false;
            });

            this.activeRadar = data.key;

            this.SetBaseAndGunColor();

            this.statictv.setActive(true).setVisible(true);

            this.time.addEvent({
                delay: 100,
                callback: ()=>{
                    this.statictv.setActive(false).setVisible(false);
                }
            });
        }
    }

    private ProcessMissileInFinalPosition(data:RadarEvent):void {
        data.missiles.forEach((detail:RadarDetailEvent)=>{
            let foundMissile:ODMissile = (this.missilePool.getChildren().find((x:ODMissile)=>x.id==detail.id && x.tag==data.key)) as ODMissile;

            if (foundMissile) {
                foundMissile.setODState(ODState.NotVisibleActive);
            }
        });
    }

    private DestroyTrash(data:RadarEvent):ODTrash {
        let foundTrash:ODTrash;

        data.trash.forEach((detail:RadarDetailEvent)=>{
            foundTrash = (this.trashPool.getChildren().find((x:ODTrash)=>x.id==detail.id && x.tag==data.key)) as ODTrash;

            if (foundTrash) {
                foundTrash.setODState(ODState.NotVisibleActive);
            }
        });
    
        return foundTrash;
    }

    private ProcessTrashDestroyed(data:RadarEvent):void {
        this.DestroyTrash(data);

        if (ODGameInfo.instance.world.currLevel>0) {
            ODGameInfo.instance.world.currPoints += 1;
            this.scoreText.setText(ODGameInfo.instance.world.currPoints.toString());
        }
    }

    private ProcessTrashAtWater(data:RadarEvent):void {
        let foundTrash:ODTrash = this.DestroyTrash(data);

        if (foundTrash) {
            this.particleManager.emitters.getAll('name', this.activeRadar as any).forEach((emit:Phaser.GameObjects.Particles.ParticleEmitter)=>{
                emit.visible=true;
                emit.emitParticleAt(foundTrash.x, this.displayHeight-(this.displayHeight*0.0965));
            });
        }
    }
}

export default ODMonitor;
