﻿// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.RGBZ_EventTrigger = function(runtime)
{
	this.runtime = runtime;
};

(function ()
{
	var pluginProto = cr.plugins_.RGBZ_EventTrigger.prototype;
		
	/////////////////////////////////////
	// Object type class
	pluginProto.Type = function(plugin)
	{
		this.plugin = plugin;
		this.runtime = plugin.runtime;
	};

	var typeProto = pluginProto.Type.prototype;

	typeProto.onCreate = function()
	{
	};

	/////////////////////////////////////
	// Instance class
	pluginProto.Instance = function(type)
	{
		this.type = type;
		this.runtime = type.runtime;
		
		// Initialise object properties
		//this.testProperty = 0;
	};
	
	var instanceProto = pluginProto.Instance.prototype;
	
	instanceProto.onCreate = function()
	{
		// Read properties set in C3
		this.testProperty = this.properties[0];

        //Set up events structure
        this.rgbzEventBuffer = [];
		this.lastMessage = '';
	};
	
	instanceProto.saveToJSON = function ()
	{
		return {};
	};
	
	instanceProto.loadFromJSON = function (o)
	{
	};
	
	/**BEGIN-PREVIEWONLY**/
	instanceProto.getDebuggerValues = function (propsections)
	{
	};
	/**END-PREVIEWONLY**/

	//////////////////////////////////////
	// Conditions
	function Cnds() {};

    Cnds.prototype.EventTriggered = function(data){
		let current_frame = this.runtime.getCurrentEventStack();
		let current_event = current_frame.current_event;

		let eventId = current_event.sid.toString() + '-' + current_frame.cndindex.toString();

		//Find the first event of this (data) type in the collection That hasn't been processed for this eventId Key.
		//This allows multiple triggers to be fired for the same event.
		let eventFound = this.rgbzEventBuffer.find(x=>x.key==data && x.handledBy.indexOf(eventId)<0);

		if (eventFound) {
			eventFound.handledBy.push(eventId);
			eventFound.lastModified = (new Date()).getTime();

			this.lastMessage = eventFound.message;

			return true;
		}
		else {
			this.lastMessage = [];
			return false;
		}
    };

	pluginProto.cnds = new Cnds();

	//////////////////////////////////////
	// Actions
	function Acts() {};

	Acts.prototype.SendEvent = function(name, message, additionalMessages){
		//Clean up old events that haven't been modified in awhile. It should be safe to assume to remove them.
		//Testing for ones that have been in existence for more than 200ms. This allows for plenty of time to
		//make sure all handlers have seen the event even at a 30 fps rate.

		//Also of note is this clean up is located in the send because the trigger method gets called numerous times
		//for each included event sheet. So this makes it so that a clean up is only performed once each time a send is issued.
		let currentTime = (new Date()).getTime();

		for (let i=0; i<this.rgbzEventBuffer.length; i++) {
			if ((currentTime-this.rgbzEventBuffer[i].lastModified)>200) {
				//Remove item from array
				this.rgbzEventBuffer.splice(i,1);
				i--;
			}
		}

		let transportObj = [];
		transportObj.push(message);

		if (additionalMessages) {
			additionalMessages.forEach(x=>transportObj.push(x));
		}

		this.rgbzEventBuffer.push(new rgbzEvent(name, transportObj));

		this.runtime.trigger(cr.plugins_.RGBZ_EventTrigger.prototype.cnds.EventTriggered, this);
	};
	
	pluginProto.acts = new Acts();

	//////////////////////////////////////
	// Expressions
	function Exps() {};

	Exps.prototype.LastEventMessage = function(ret){
		ret.set_string((this.lastMessage && this.lastMessage.length>0)?this.lastMessage[0]:"");
	};

	Exps.prototype.GetEventMessageAt = function(ret, index) {
		let result = '';

		if (this.lastMessage && index>=0 && Array.isArray(this.lastMessage) && index<this.lastMessage.length) {
			result = this.lastMessage[index];
		}

		ret.set_any(result);
	};

	Exps.prototype.GetEventMessageItemCount = function(ret) {
		let result = 0;

		if (this.lastMessage && Array.isArray(this.lastMessage)) {
			result = this.lastMessage.length;
		}

		ret.set_int(result);
	};
	
	pluginProto.exps = new Exps();

}());