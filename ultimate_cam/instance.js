"use strict";
{
    const PLUGIN_CLASS = SDK.Plugins.EMI_INDO_Ultimate_Camera;

    PLUGIN_CLASS.Instance = class Ultimate_CameraInstance extends SDK.IInstanceBase
    {
        constructor(sdkType, inst)
        {
            super(sdkType, inst);
        }

        Release()
        {}

        OnCreate()
        {}

        OnPropertyChanged(id, value)
        {}

        LoadC2Property(name, valueString)
        {
            return false; // not handled
        }
    };
}