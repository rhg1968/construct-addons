"use strict";

const C3 = globalThis.C3;

C3.Plugins.RGBZ_PubSub.Instance = class RGBZ_PubSubInstance extends globalThis.ISDKInstanceBase
{
	constructor()
	{
		super();
		const properties = this._getInitProperties();
		
		this.ConsoleLogging = (properties && properties.length && properties.length>0)?properties[0]:false;
		this.Event = null;
		this.EventPublished = null;
	}

	////////////////////////////////////////////////////////////////////////
	//Expressions
	_GetEventParameter(parameterIndex) {
		if (this.EventPublished && parameterIndex < this.EventPublished.EventParams.length) {
			return this.EventPublished.EventParams[parameterIndex];
		}
		else {
			if (this.ConsoleLogging) console.warn(`No event published to retrieve EventParams from`);
			return "";
		}
	}

	////////////////////////////////////////////////////////////////////////
	//Actions
	_CreateEvent(name) {
		if (!this.Event) {
			this.Event = {
				Name: name.toLowerCase(),
				EventParams: new Array()
			};
		}
		else {
			if (this.ConsoleLogging) console.warn(`Can not create event ${name} because an event has already been created and not published.`);
		}
	}

	_AddEventParameter(item) {
		if (this.Event) {
			this.Event.EventParams.push(item);
		}
		else {
			if (this.ConsoleLogging) console.warn(`Can not add parameter ${item} to event since no event has been created.`);
		}
	}

	_PublishEvent() {
		if (this.Event) {
			this.EventPublished = JSON.parse(JSON.stringify(this.Event));

			this.Event = null;

			this._trigger(self.C3.Plugins.RGBZ_PubSub.Cnds.SubscribeToEvent);
		}
		else {
			if (this.ConsoleLogging) console.error(`Can not publish since an event has not been created.`);
		}
	}
	
	_release()
	{
		super._release();
	}
	
	_saveToJson()
	{
		return {
			// data to be saved for savegames
		};
	}
	
	_loadFromJson(o)
	{
		// load state for savegames
	}
};