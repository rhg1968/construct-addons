//import ODApi from './ODAPI';
import 'phaser';
import ODTitleScene from './Scenes/ODTitleScene';
import ODBoot from './Scenes/ODBoot';
import ODLevelSelection from './Scenes/ODLevelSelection';
import ODGameScene from './Scenes/ODGameScene';
import ODCacheManager from './Managers/ODCacheManager';

var game:Phaser.Game;

window.onload = function()
{
    ODCacheManager.initialize();

    let gameConfig:GameConfig = {
        type: Phaser.AUTO,
        title: 'Ocean Defender II',
        width: 480,
        height: 854,
        backgroundColor: 0xcccccc,
        autoFocus: true,
        version: '1.0.0',
        fps: {
            target: 60,
            min: 30
        },
        scene: [ODBoot, ODTitleScene, ODGameScene, ODLevelSelection]
    };
    
    game = new Phaser.Game(gameConfig);
    resize();
    window.addEventListener("resize", resize, false);
};

function resize(){
    let canvas:HTMLCanvasElement = document.querySelector("canvas");
    let windowWidth:number = window.innerWidth;
    let windowHeight:number = window.innerHeight;
    let windowRatio:number = windowWidth / windowHeight;
    let gameRatio:number = (game.config.width as number) / (game.config.height as number);
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
};
