"use strict";

const SDK = self.SDK;

const PLUGIN_CLASS = SDK.Plugins.RGBZ_PubSub;

PLUGIN_CLASS.Type = class RGBZ_PubSubType extends SDK.ITypeBase
{
	constructor(sdkPlugin, iObjectType)
	{
		super(sdkPlugin, iObjectType);
	}
};
