"use strict";
{
    const PLUGIN_CLASS = SDK.Plugins.EMI_INDO_Ultimate_Camera;

    PLUGIN_CLASS.Type = class Ultimate_CameraType extends SDK.ITypeBase
    {
        constructor(sdkPlugin, iObjectType)
        {
            super(sdkPlugin, iObjectType);
        }
    };
}