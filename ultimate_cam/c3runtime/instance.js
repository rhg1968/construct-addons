"use strict";
{
    C3.Plugins.EMI_INDO_Ultimate_Camera.Instance = class Ultimate_CameraInstance extends C3.SDKInstanceBase
    {
        constructor(inst, properties)
        {
            super(inst);

            this.toback = false;
            this.tapphoto = false;
            this.tapfocus = false;
            this.previewdrag = false;
            this.storetofile = false;
            this.disableexifheader = false;

            if (properties)
            {

                this.toback = properties[0]
                this.tapphoto = properties[1]
                this.tapfocus = properties[2]
                this.previewdrag = properties[3]
                this.storetofile = properties[4]
                this.disableexifheader = properties[5]

            }

            if (typeof CameraPreview == 'undefined')
            {
                return;
            }
            else
            {};
            this.ins = C3.Plugins.EMI_INDO_Ultimate_Camera.Cnds;
            this.cd = CameraPreview;

            this.eventResponse = "";
            this.errorResponse = "";
        }

        Release()
        {
            super.Release();
        }

        SaveToJson()
        {
            return {
                // data to be saved for savegames
            };
        }

        LoadFromJson(o)
        {
            // load state for savegames
        }

        GetDebuggerProperties()
        {
            return [
            {
                title: "Ultimate_Camera_FREE",
                properties: [
                    //{name: ".current-animation",	value: this._currentAnimation.GetName(),	onedit: v => this.CallAction(Acts.SetAnim, v, 0) },
                ]
            }];
        }
    };
}