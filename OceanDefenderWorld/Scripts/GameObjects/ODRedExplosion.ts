import ODBase from './ODBase';

class ODRedExplosion extends ODBase
{
    constructor(scene:Phaser.Scene, x:number, y:number, texture:string, frame?:string|number)
    {
        super(scene, x, y, texture, frame);
    }

    update(time:number, dt:number) {
        console.log(dt);
    }
}

export default ODRedExplosion;
