import ODBase from "./ODBase";

class ODYellowGun extends ODBase
{
    constructor(scene:Phaser.Scene, x:number, y:number, texture:string, frame?:string|number)
    {
        super(scene, x, y, texture, frame);
    }
}

export default ODYellowGun;
