import GameEvent from "./../Models/GameEvent";

class ODPauseScene extends Phaser.Scene
{
    constructor(public key:string, public x:number, public y:number, public displayWidth:number, 
        public displayHeight:number)
    {
        super({
            key
        });
    }

    preload():void
    {

    }

    create():void
    {
        this.cameras.main.setViewport(this.x, this.y, this.displayWidth, this.displayHeight);
        ((this.add as any).pausepanel(this.displayWidth/2.0, this.displayHeight/2.0) as Phaser.GameObjects.Sprite).setDisplaySize(this.displayWidth, this.displayHeight);

        let playButton:Phaser.GameObjects.Sprite = ((this.add as any).playbutton(this.displayWidth/2.0, (this.displayHeight/4.0)-20) as Phaser.GameObjects.Sprite).setInteractive().setDisplaySize(115,124);
        playButton.on('pointerdown', (pointer:any)=>{
            let ge:GameEvent = new GameEvent('GamePause', false);
    
            this.registry.set('GamePause', ge);
        });

        let replayButton:Phaser.GameObjects.Sprite = ((this.add as any).replaybutton(this.displayWidth/2.0, this.displayHeight/2.0) as Phaser.GameObjects.Sprite).setInteractive().setDisplaySize(115,124);
        replayButton.on('pointerdown', (pointer:any)=>{
            let ge:GameEvent = new GameEvent('GamePause', false, true);
    
            this.registry.set('GamePause', ge);
        });

        ((this.add as any).homebutton(this.displayWidth/2.0, replayButton.y + (replayButton.y-playButton.y)) as Phaser.GameObjects.Sprite).setInteractive().setDisplaySize(115,124).on('pointerdown', (pointer:any)=>{
            let ge:GameEvent = new GameEvent('GamePause', false, false, true);
    
            this.registry.set('GamePause', ge);
        });
    }
}

export default ODPauseScene;
