"use strict";

{
	C3.Plugins.RGBZ_EventTrigger.Acts =
	{
		SendEvent(name, message, additionalMessages)
		{
			//Clean up old events that haven't been modified in awhile. It should be safe to assume to remove them.
			//Testing for ones that have been in existence for more than 200ms. This allows for plenty of time to
			//make sure all handlers have seen the event even at a 30 fps rate.

			//Also of note is this clean up is located in the send because the trigger method gets called numerous times
			//for each included event sheet. So this makes it so that a clean up is only performed once each time a send is issued.
			let currentTime = (new Date()).getTime();

			for (let i=0; i<this.rgbzEventBuffer.length; i++) {
				if ((currentTime-this.rgbzEventBuffer[i].lastModified)>200) {
					//Remove item from array
					this.rgbzEventBuffer.splice(i,1);
					i--;
				}
			}

			let transportObj = [];
			transportObj.push(message);

			if (additionalMessages) {
				additionalMessages.forEach(x=>transportObj.push(x));
			}

			this.rgbzEventBuffer.push(new rgbzEvent(name, transportObj));

			this.Trigger(C3.Plugins.RGBZ_EventTrigger.Cnds.EventTriggered);
		}
	};
}