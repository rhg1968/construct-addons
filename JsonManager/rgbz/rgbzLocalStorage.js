let idbJsonManager = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;

class rgbzLocalStoreModel {
    constructor(key='', items={}, origItems={}){
        this.key = key;
        this.items= items;
        this.origItems= origItems;
        this.isError = false;
        this.errorMessage = '';
    }
}


let rgbzDB = undefined;
let rgbzDBReady = false;
let rgbzDBName = undefined;
let rgbzObjectStoreName = undefined;

let initializeJSMDB = function(name) {
    if (!idbJsonManager) {
        console.log('This browser doesn\'t support IndexedDB');
    }
    else {
        let rgbzDBOpenRequest = undefined;
        rgbzDBName = 'json-manager-' + name + '-db';
        rgbzObjectStoreName = "jmstore-" + name;
    
        rgbzDBOpenRequest = idbJsonManager.open(rgbzDBName, 1);
    
        rgbzDBOpenRequest.onerror = function(event){
            console.log('Error opening rgbzDB - ' + event.target.errorCode.toString());
        };
    
        rgbzDBOpenRequest.onsuccess = function(event){
            rgbzDB = event.target.result;
            rgbzDBReady = true;
        };
    
        rgbzDBOpenRequest.onupgradeneeded = function(event) {
            rgbzDB = event.target.result;
    
            let objectStore = rgbzDB.createObjectStore(rgbzObjectStoreName, { keyPath: "key" });
    
            objectStore.transaction.oncomplete = function(event){
                rgbzDBReady = true;
            };
        };
    }
};

let rgbzGetFromDb = (key)=>{
    var deferred = new Promise((resolve, reject)=>{
        if (rgbzDBReady) {
            let transaction = rgbzDB.transaction([rgbzObjectStoreName], "readonly");
    
            var objectStore = transaction.objectStore(rgbzObjectStoreName);
    
            var request = objectStore.get(key);
    
            request.onerror = function(event) {
                let result = new rgbzLocalStoreModel();
                result.isError = true;
                result.errorMessage = 'rgbzGetFromDb: Error getting ' + key;
                console.log(result.errorMessage);

                reject(result);
            };
    
            request.onsuccess = function(event) {
                resolve(request.result);
            };
        }
        else {
            let result = new rgbzLocalStoreModel();
            result.isError = true;
            result.errorMessage = 'rgbzGetFromDb: Database is not available.';
            console.log(result.errorMessage);

            reject(result);
        }
    });

    return deferred;
};

let rgbzDeleteFromDb = (key)=>{
    var deferred = new Promise((resolve, reject)=>{
        if (rgbzDBReady) {
            let transaction = rgbzDB.transaction([rgbzObjectStoreName], "readwrite");
    
            var objectStore = transaction.objectStore(rgbzObjectStoreName);
    
            var request = objectStore.delete(key);
    
            request.onerror = function(event) {
                reject('rgbzDeleteFromDb: Delete ' + key + ' error.');
            };
    
            request.onsuccess = function(event) {
                resolve(key + ' deleted successfully');
            };
        }
        else {
            reject('rgbzDeleteFromDb: Database is not available.');
        }
    });

    return deferred;
};

let clearStore = ()=>{
    var deferred = new Promise((resolve, reject)=>{
        if (rgbzDBReady) {
            let transaction = rgbzDB.transaction([rgbzObjectStoreName], "readwrite");

            let objectStore = transaction.objectStore(rgbzObjectStoreName);

            let request = objectStore.clear();

            request.onerror = function(event){
                reject('clearStore: Error clearing object store.');
            };

            request.onsuccess = function(event){
                resolve('clearStore: Success.');
            };
        }
        else {
            reject('clearStore: Database is not available.');
        }
    });

    return deferred;
};

let rgbzAddToDb = (key, item)=>{
    var deferred = new Promise((resolve, reject)=>{
        rgbzGetFromDb(key).then((data)=>{
            if (rgbzDBReady) {
                let storable = (!data)?new rgbzLocalStoreModel(key, item, item):data;
                storable.items = item;
        
                let transaction = rgbzDB.transaction([rgbzObjectStoreName], "readwrite");
        
                let objectStore = transaction.objectStore(rgbzObjectStoreName);
        
                let addRequest = objectStore.put(storable);

                addRequest.onsuccess = function(event){
                    resolve(key + ' added successfully');
                };
        
                addRequest.onerror = function(event){
                    reject('rgbzAddToDb: Database add request error.');
                };
            }
            else {
                reject('rgbzAddToDb: Database is not available.');
            }
        }, (error)=>{
            error.errorMessage = 'rgbzAddToDb: ' + error.errorMessage;
            reject(error);
        });
    });

    return deferred;
};

let resetKeyFromBak = (key)=>{
    var deferred = new Promise((resolve, reject)=>{
        let result = undefined;

        rgbzGetFromDb(key).then((data)=>{
            if (data){
                // data.items = data.origItems;
    
                rgbzAddToDb(key, data.origItems).then((success)=>{
                    resolve(data.origItems);
                }, (error)=>{
                    reject('Error resetKeyFromBak: Failure to resave to the db');
                }); 
            }
            else {
                reject('Error resetKeyFromBak: No data retrieved from the db');
            }
        }, (error)=>{
            reject('Error resetKeyFromBak: Failure to get from the db');
        });
    });

    return deferred;
};
