"use strict";

{
	const BEHAVIOR_CLASS = SDK.Behaviors.wackytoasterButtons;
	
	BEHAVIOR_CLASS.Instance = class wackytoasterButtonInstance extends SDK.IBehaviorInstanceBase
	{
		constructor(sdkBehType, behInst)
		{
			super(sdkBehType, behInst);
		}
		
		Release()
		{
		}
		
		OnCreate()
		{
		}
		
		OnPropertyChanged(id, value)
		{
		}
		
		LoadC2Property(name, valueString)
		{
			return false;		// not handled
		}
	};
}