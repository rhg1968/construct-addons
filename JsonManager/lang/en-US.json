﻿{
	"languageTag": "en-US",
	"fileDescription": "Strings for Json Manager.",
	"text": {
		"plugins": {
			"rgbz_jsonmanager": {
				"name": "Json Manager",
				"description": "Load and retrieve values from json files using standard JSON notation. Convert a JSON sub node to a Construct Dictionary/Array.",
				"help-url": "https://www.construct.net",
				"properties": {
					"json-files": {
						"name": "Json Files To Load",
						"desc": "List of comma separated Json files.)"
					},
					"json-slots": {
						"name": "Json File Slot List",
						"desc": "List of comma separated slots to store files in."
					}
				},
				"aceCategories": {
					"json":"JSON",
					"jsonloop":"JSON Looping",
					"jsonconversion": "JSON Conversion"
				},
				"conditions": {
					"json-loaded": {
						"list-name": "On Json loaded",
						"display-text": "On Json loaded",
						"description": "Triggered when: All Json files have been loaded."
					},
					"json-array-loop" : {
						"list-name": "Json array loop",
						"display-text": "Loop over {0} with query {1}. {2} nested in another json loop",
						"description": "Json array loop: Over a slot using a query.",
						"params": {
							"array-loop-slot":{
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"array-loop-query" : {
								"name": "Query",
								"desc": "Json notation query."
							},
							"array-used-within-another-array-loop" :{
								"name": "Nested In another Json loop",
								"desc": "Indicates if within another Json array loop",
								"items": {
									"array-item-not-in-existing-loop": "No",
									"array-item-in-existing-loop": "Yes"
								}
							}
						}
					}
				},
				"actions": {
					"load-json" : {
						"list-name": "Load Json",
						"display-text": "Load Json: Cache Enable is {0}",
						"description": "Load Json: All files into correct slots",
						"params":{
							"clear-cache":{
								"name": "Enable Caching",
								"desc": "Enable Caching to persist json changes in local storage (Actual files not updated)."
							}
						}
					},
					"load-json-string" : {
						"list-name": "Load Json From String",
						"display-text": "Load Json From String: Cache Enable is {2} from {1} into slot {0}",
						"description": "Load Json From String: Load Json string into a slot",
						"params":{
							"json-load-string-slot":{
								"name": "Slot",
								"desc": "Slot to load the Json string into."
							},
							"json-load-string":{
								"name": "Json String",
								"desc": "The string version of a Json file to load into the manager."
							},
							"clear-cache-from-string":{
								"name": "Enable Caching",
								"desc": "Enable Caching to persist json changes in local storage (Actual files not updated)."
							}
						}
					},
					"reset-json-slot-from-backup": {
						"list-name": "Reset Json slot.",
						"display-text": "Reset Json slot: {0} back to it's original value.",
						"description": "Reset Json slot back to it's original value when it was first loaded.",
						"params": {
							"set-json-reset-slot": {
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							}
						}
					},
					"remove-json-slot": {
						"list-name": "Clear slot",
						"display-text": "Clear Slot: {0}",
						"description": "Clear Json slot.",
						"params": {
							"set-json-slot-to-remove": {
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							}
						}
					},
					"set-json-sub-object-to-new-slot": {
						"list-name": "Create new slot.",
						"display-text": "Create new slot: Query {1} and copy from {0} to {2}. {3} in json loop",
						"description": "Create new slot by duplicating a Json sub object into a new slot.",
						"params": {
							"set-json-sub-old-slot": {
								"name": "Existing Slot",
								"desc": "Named slot Json is stored in."
							},
							"set-json-sub-new-query": {
								"name": "Query",
								"desc": "Json notation query."
							},
							"set-json-sub-new-slot": {
								"name": "New slot",
								"desc": "Named slot Json is stored in."
							},
							"set-json-sub-object-to-new-slot-used-within-another-array-loop": {
								"name": "Nested In a Json loop",
								"desc": "Indicates if within a Json array loop",
								"items": {
									"set-json-sub-object-to-new-slot-item-not-in-existing-loop": "No",
									"set-json-sub-object-to-new-slot-item-in-existing-loop": "Yes"
								}
							}
						}
					},
					"set-json-value": {
						"list-name": "Set Json Value",
						"display-text": "Set Json Value: Set {3} into {0} and {2}, using {1}. {4} in json loop",
						"description": "Set Json Value: Set a value to a specific field in the Json object.",
						"params": {
							"set-json-slot": {
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"set-json-query": {
								"name": "Query",
								"desc": "Json notation query."
							},
							"set-json-property": {
								"name": "Property",
								"desc": "Property on the Json object."
							},
							"set-json-value": {
								"name": "Value",
								"desc": "Value to set into the property."
							},
							"set-json-value-used-within-another-array-loop": {
								"name": "Nested In a Json loop",
								"desc": "Indicates if within a Json array loop",
								"items": {
									"set-json-value-item-not-in-existing-loop": "No",
									"set-json-value-item-in-existing-loop": "Yes"
								}
							}
						}
					}
				},
				"expressions": {
					"get-json-loop-index": {
						"description": "Json loop index: current loop index of a Json loop.",
						"translated-name": "JsonLoopIndex"
					},
					"get-json-value": {
						"description": "Get json value: Using standard Json notation.",
						"translated-name": "GetJsonValue",
						"params" : {
							"slot":{
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"query": {
								"name": "Query",
								"desc": "Json notation query."
							},
							"get-json-value-used-within-another-array-loop": {
								"name": "Nested In a Json loop (Yes/No)",
								"desc": "Indicates if within a Json array loop"
							}
						}
					},
					"get-json-array-length": {
						"description": "Get length: Array within a Json object.",
						"translated-name": "GetJsonArrayLength",
						"params": {
							"array-slot":{
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"array-query" :{
								"name": "Query",
								"desc": "Json notation query."
							},
							"get-json-array-length-used-within-another-array-loop": {
								"name": "Nested In a Json loop (Yes/No)",
								"desc": "Indicates if within a Json array loop"
							}
						}
					},
					"get-array-from-json-path": {
						"description": "C3 array: Get formatted json string in array form.",
						"translated-name": "GetArrayFromJsonPath",
						"params": {
							"array-from-json-path-slot" : {
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"array-from-json-path-query": {
								"name": "Query",
								"desc": "Json notation query."
							},
							"get-array-from-json-path-used-within-another-array-loop": {
								"name": "Nested In a Json loop (Yes/No)",
								"desc": "Indicates if within a Json array loop"
							}
						}
					},
					"get-dictionary-from-json-path": {
						"description": "C3 dictionary: Get formatted json string in dictionary form.",
						"translated-name": "GetDictionaryFromJsonPath",
						"params": {
							"dictionary-from-json-path-slot" : {
								"name": "Slot",
								"desc": "Named slot Json is stored in."
							},
							"dictionary-from-json-path-query": {
								"name": "Query",
								"desc": "Json notation query."
							},
							"get-dictionary-from-json-path-used-within-another-array-loop": {
								"name": "Nested In a Json loop (Yes/No)",
								"desc": "Indicates if within a Json array loop"
							}
						}
					}
				}
			}
		}
	}
}