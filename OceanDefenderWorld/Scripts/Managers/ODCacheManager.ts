import localforage from 'localforage';

class ODCacheManager
{
    constructor(){ }

    public static initialize():void
    {
        localforage.config({
            name: 'ODIIDB',
            version: 1.0,
            storeName: 'ODCachedDB',
            description: 'Stores all persisent data for ODII'
        });
    }

    public static getItem(key:string):Promise<any>
    {
        return localforage.getItem(key) as Promise<any>;
    }

    public static setItem(key:string, value:any):Promise<any>
    {
        return localforage.setItem(key, value) as Promise<any>;
    }

    public static removeItem(key:string):Promise<any>
    {
        return localforage.removeItem(key) as Promise<any>;
    }

    public static clear():Promise<any>
    {
        return localforage.clear() as Promise<any>;
    }
}

export default ODCacheManager;
