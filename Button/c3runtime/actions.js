"use strict";

{
	C3.Behaviors.wackytoasterButtons.Acts =
	{
		fireClick()
		{
	
			if(!this._enabled) return;
			
			let instanceUID = [this.GetObjectInstance().GetUID()];
			
			let params = JSON.parse(this._functionParams.replace(/'/g, '"'));
			let animParams = JSON.parse(this._animationParams.replace(/'/g, '"'));
			
			params = instanceUID.concat(params);
			animParams = instanceUID.concat(animParams);
			
			c3_callFunction(this._buttonAnimation, animParams);
			c3_callFunction(this._buttonFunction, params);
		},
		
		setEnabled()
		{
			this._enabled = true;
		},
		
		setDisabled()
		{
			this._enabled = false;
		},
		
		setParams(p)
		{
			this._functionParams = p.replace(/'/g, '"');
		},
		
		setAnimParams(p)
		{
			this._animationParams = p.replace(/'/g, '"');
		}
		
	};
}