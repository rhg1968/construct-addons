/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./Scripts/ODMain.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./Scripts/Enums/ODBaseEnum.ts":
/*!*************************************!*\
  !*** ./Scripts/Enums/ODBaseEnum.ts ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var ODBaseEnum;
(function (ODBaseEnum) {
    ODBaseEnum[ODBaseEnum["Yellow"] = 0] = "Yellow";
    ODBaseEnum[ODBaseEnum["Blue"] = 1] = "Blue";
    ODBaseEnum[ODBaseEnum["Grey"] = 2] = "Grey";
})(ODBaseEnum || (ODBaseEnum = {}));
/* harmony default export */ __webpack_exports__["default"] = (ODBaseEnum);


/***/ }),

/***/ "./Scripts/Enums/ODState.ts":
/*!**********************************!*\
  !*** ./Scripts/Enums/ODState.ts ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var ODState;
(function (ODState) {
    ODState[ODState["Visible"] = 0] = "Visible";
    ODState[ODState["Active"] = 1] = "Active";
    ODState[ODState["VisibleActive"] = 2] = "VisibleActive";
    ODState[ODState["NotVisible"] = 3] = "NotVisible";
    ODState[ODState["NotActive"] = 4] = "NotActive";
    ODState[ODState["NotVisibleActive"] = 5] = "NotVisibleActive";
})(ODState || (ODState = {}));
/* harmony default export */ __webpack_exports__["default"] = (ODState);


/***/ }),

/***/ "./Scripts/GameObjects/ODBase.ts":
/*!***************************************!*\
  !*** ./Scripts/GameObjects/ODBase.ts ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../Enums/ODState */ "./Scripts/Enums/ODState.ts");

class ODBase extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
        this.direction = new Phaser.Math.Vector2();
    }
    setXY(x, y, finalX, finalY) {
        this.x = x;
        this.y = y;
        this.finalX = finalX;
        this.finalY = finalY;
        this.direction.set(this.finalX - this.x, this.finalY - this.y).normalize();
    }
    setODState(state) {
        switch (state) {
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].Active:
                this.setActive(true);
                break;
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].NotActive:
                this.setActive(false);
                break;
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].NotVisible:
                this.setVisible(false);
                break;
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].NotVisibleActive:
                this.setActive(false);
                this.setVisible(false);
                break;
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].Visible:
                this.setVisible(true);
                break;
            case _Enums_ODState__WEBPACK_IMPORTED_MODULE_0__["default"].VisibleActive:
                this.setActive(true);
                this.setVisible(true);
                break;
        }
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODBase);


/***/ }),

/***/ "./Scripts/GameObjects/ODBlueGun.ts":
/*!******************************************!*\
  !*** ./Scripts/GameObjects/ODBlueGun.ts ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODBlueGun extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODBlueGun);


/***/ }),

/***/ "./Scripts/GameObjects/ODBlueGunBase.ts":
/*!**********************************************!*\
  !*** ./Scripts/GameObjects/ODBlueGunBase.ts ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODBlueGunBase extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODBlueGunBase);


/***/ }),

/***/ "./Scripts/GameObjects/ODExplosionMarker.ts":
/*!**************************************************!*\
  !*** ./Scripts/GameObjects/ODExplosionMarker.ts ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");
/* harmony import */ var _Enums_ODState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../Enums/ODState */ "./Scripts/Enums/ODState.ts");
/* harmony import */ var _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../Models/RadarEvent */ "./Scripts/Models/RadarEvent.ts");
/* harmony import */ var _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../Models/RadarDetailEvent */ "./Scripts/Models/RadarDetailEvent.ts");




class ODExplosionMarker extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
        this.id = (new Date()).getTime();
        this.radar = scene;
    }
    SetId() {
        this.id = (new Date()).getTime();
    }
    update(time, dt) {
        if (this.active) {
            this.radar.GetTrashMarkerPool().getChildren().filter((mm) => mm.active).forEach((marker) => {
                if (Math.abs(Phaser.Math.Distance.Between(this.x, this.y, marker.x, marker.y)) < 10) {
                    marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
                    let event = new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_2__["default"](marker.tag);
                    event.trash.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_3__["default"](marker.id));
                    this.scene.registry.set('TrashDestroyed', event);
                }
            });
        }
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODExplosionMarker);


/***/ }),

/***/ "./Scripts/GameObjects/ODGreyGun.ts":
/*!******************************************!*\
  !*** ./Scripts/GameObjects/ODGreyGun.ts ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODGreyGun extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODGreyGun);


/***/ }),

/***/ "./Scripts/GameObjects/ODGreyGunBase.ts":
/*!**********************************************!*\
  !*** ./Scripts/GameObjects/ODGreyGunBase.ts ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODGreyGunBase extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODGreyGunBase);


/***/ }),

/***/ "./Scripts/GameObjects/ODMissile.ts":
/*!******************************************!*\
  !*** ./Scripts/GameObjects/ODMissile.ts ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODMissile extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODMissile);


/***/ }),

/***/ "./Scripts/GameObjects/ODMissileMaker.ts":
/*!***********************************************!*\
  !*** ./Scripts/GameObjects/ODMissileMaker.ts ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODMissileMaker extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
        this.id = (new Date()).getTime();
    }
    SetId() {
        this.id = (new Date()).getTime();
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODMissileMaker);


/***/ }),

/***/ "./Scripts/GameObjects/ODOcean.ts":
/*!****************************************!*\
  !*** ./Scripts/GameObjects/ODOcean.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODOcean extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODOcean);


/***/ }),

/***/ "./Scripts/GameObjects/ODRedExplosion.ts":
/*!***********************************************!*\
  !*** ./Scripts/GameObjects/ODRedExplosion.ts ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODRedExplosion extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
    update(time, dt) {
        console.log(dt);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODRedExplosion);


/***/ }),

/***/ "./Scripts/GameObjects/ODTrash.ts":
/*!****************************************!*\
  !*** ./Scripts/GameObjects/ODTrash.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODTrash extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
        this.ignore = false;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODTrash);


/***/ }),

/***/ "./Scripts/GameObjects/ODTrashMarker.ts":
/*!**********************************************!*\
  !*** ./Scripts/GameObjects/ODTrashMarker.ts ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODTrashMarker extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
        this.id = (new Date()).getTime();
    }
    SetId() {
        this.id = (new Date()).getTime();
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODTrashMarker);


/***/ }),

/***/ "./Scripts/GameObjects/ODWhiteExplosion.ts":
/*!*************************************************!*\
  !*** ./Scripts/GameObjects/ODWhiteExplosion.ts ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODWhiteExplosion extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODWhiteExplosion);


/***/ }),

/***/ "./Scripts/GameObjects/ODYellowGun.ts":
/*!********************************************!*\
  !*** ./Scripts/GameObjects/ODYellowGun.ts ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODYellowGun extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODYellowGun);


/***/ }),

/***/ "./Scripts/GameObjects/ODYellowGunBase.ts":
/*!************************************************!*\
  !*** ./Scripts/GameObjects/ODYellowGunBase.ts ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODBase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODBase */ "./Scripts/GameObjects/ODBase.ts");

class ODYellowGunBase extends _ODBase__WEBPACK_IMPORTED_MODULE_0__["default"] {
    constructor(scene, x, y, texture, frame) {
        super(scene, x, y, texture, frame);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODYellowGunBase);


/***/ }),

/***/ "./Scripts/Managers/ODCacheManager.ts":
/*!********************************************!*\
  !*** ./Scripts/Managers/ODCacheManager.ts ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var localforage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! localforage */ "localforage");
/* harmony import */ var localforage__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(localforage__WEBPACK_IMPORTED_MODULE_0__);

class ODCacheManager {
    constructor() { }
    static initialize() {
        localforage__WEBPACK_IMPORTED_MODULE_0___default.a.config({
            name: 'ODIIDB',
            version: 1.0,
            storeName: 'ODCachedDB',
            description: 'Stores all persisent data for ODII'
        });
    }
    static getItem(key) {
        return localforage__WEBPACK_IMPORTED_MODULE_0___default.a.getItem(key);
    }
    static setItem(key, value) {
        return localforage__WEBPACK_IMPORTED_MODULE_0___default.a.setItem(key, value);
    }
    static removeItem(key) {
        return localforage__WEBPACK_IMPORTED_MODULE_0___default.a.removeItem(key);
    }
    static clear() {
        return localforage__WEBPACK_IMPORTED_MODULE_0___default.a.clear();
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODCacheManager);


/***/ }),

/***/ "./Scripts/Models/GameEvent.ts":
/*!*************************************!*\
  !*** ./Scripts/Models/GameEvent.ts ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class GameEvent {
    constructor(key, isPaused = false, restartLevel = false, gotoTitleScreen = false) {
        this.key = key;
        this.isPaused = isPaused;
        this.restartLevel = restartLevel;
        this.gotoTitleScreen = gotoTitleScreen;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (GameEvent);


/***/ }),

/***/ "./Scripts/Models/GameInfoModels/ODLevelInfo.ts":
/*!******************************************************!*\
  !*** ./Scripts/Models/GameInfoModels/ODLevelInfo.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class ODLevelInfo {
    constructor() {
        this.cleared = false;
        this.unlocked = false;
        this.radars = new Array();
        this.coolDownTimer = 400;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODLevelInfo);


/***/ }),

/***/ "./Scripts/Models/GameInfoModels/ODRadarInfo.ts":
/*!******************************************************!*\
  !*** ./Scripts/Models/GameInfoModels/ODRadarInfo.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class ODRadarInfo {
    constructor(active, trashSpawnTime, trashMinSpeed, trashMaxSpeed, trashToClear, oceanMaxDamage) {
        this.active = active;
        this.trashSpawnTime = trashSpawnTime;
        this.trashMinSpeed = trashMinSpeed;
        this.trashMaxSpeed = trashMaxSpeed;
        this.trashToClear = trashToClear;
        this.oceanMaxDamage = oceanMaxDamage;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODRadarInfo);


/***/ }),

/***/ "./Scripts/Models/GameInfoModels/ODWorldInfo.ts":
/*!******************************************************!*\
  !*** ./Scripts/Models/GameInfoModels/ODWorldInfo.ts ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class ODWorldInfo {
    constructor() {
        this.fadeColor = {
            red: 204,
            green: 204,
            blue: 204
        };
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODWorldInfo);


/***/ }),

/***/ "./Scripts/Models/MonitorClickEvent.ts":
/*!*********************************************!*\
  !*** ./Scripts/Models/MonitorClickEvent.ts ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class MonitorClickEvent {
    constructor(key, initialX = 0, initialY = 0, finalX = 0, finalY = 0, monitorWidth = 0, monitorHeight = 0) {
        this.key = key;
        this.initialX = initialX;
        this.initialY = initialY;
        this.finalX = finalX;
        this.finalY = finalY;
        this.monitorWidth = monitorWidth;
        this.monitorHeight = monitorHeight;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (MonitorClickEvent);


/***/ }),

/***/ "./Scripts/Models/ODGameInfo.ts":
/*!**************************************!*\
  !*** ./Scripts/Models/ODGameInfo.ts ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GameInfoModels_ODWorldInfo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GameInfoModels/ODWorldInfo */ "./Scripts/Models/GameInfoModels/ODWorldInfo.ts");

class ODGameInfo {
    constructor() {
        this.world = new _GameInfoModels_ODWorldInfo__WEBPACK_IMPORTED_MODULE_0__["default"]();
        this.levels = new Array();
    }
    static GetFurthestLevelUnlocked() {
        let foundLevel = 1;
        for (let level of this.instance.levels) {
            if (level.unlocked) {
                foundLevel = level.levelNum;
            }
        }
        return foundLevel;
    }
}
ODGameInfo.instance = new ODGameInfo();
/* harmony default export */ __webpack_exports__["default"] = (ODGameInfo);


/***/ }),

/***/ "./Scripts/Models/RadarDetailEvent.ts":
/*!********************************************!*\
  !*** ./Scripts/Models/RadarDetailEvent.ts ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class RadarDetailEvent {
    constructor(id, x = 0, y = 0, displayWidth = 0, displayHeight = 0) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
    }
}
/* harmony default export */ __webpack_exports__["default"] = (RadarDetailEvent);


/***/ }),

/***/ "./Scripts/Models/RadarEvent.ts":
/*!**************************************!*\
  !*** ./Scripts/Models/RadarEvent.ts ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class RadarEvent {
    constructor(key, trashToClear = 0, oceanHealth = 0) {
        this.key = key;
        this.trashToClear = trashToClear;
        this.oceanHealth = oceanHealth;
        this.trash = new Array();
        this.missiles = new Array();
    }
}
/* harmony default export */ __webpack_exports__["default"] = (RadarEvent);


/***/ }),

/***/ "./Scripts/ODMain.ts":
/*!***************************!*\
  !*** ./Scripts/ODMain.ts ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var phaser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! phaser */ "phaser");
/* harmony import */ var phaser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(phaser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Scenes_ODTitleScene__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Scenes/ODTitleScene */ "./Scripts/Scenes/ODTitleScene.ts");
/* harmony import */ var _Scenes_ODBoot__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Scenes/ODBoot */ "./Scripts/Scenes/ODBoot.ts");
/* harmony import */ var _Scenes_ODLevelSelection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Scenes/ODLevelSelection */ "./Scripts/Scenes/ODLevelSelection.ts");
/* harmony import */ var _Scenes_ODGameScene__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Scenes/ODGameScene */ "./Scripts/Scenes/ODGameScene.ts");
/* harmony import */ var _Managers_ODCacheManager__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Managers/ODCacheManager */ "./Scripts/Managers/ODCacheManager.ts");






var game;
window.onload = function () {
    _Managers_ODCacheManager__WEBPACK_IMPORTED_MODULE_5__["default"].initialize();
    let gameConfig = {
        type: Phaser.AUTO,
        title: 'Ocean Defender II',
        width: 480,
        height: 854,
        backgroundColor: 0xcccccc,
        autoFocus: true,
        version: '1.0.0',
        fps: {
            target: 60,
            min: 30
        },
        scene: [_Scenes_ODBoot__WEBPACK_IMPORTED_MODULE_2__["default"], _Scenes_ODTitleScene__WEBPACK_IMPORTED_MODULE_1__["default"], _Scenes_ODGameScene__WEBPACK_IMPORTED_MODULE_4__["default"], _Scenes_ODLevelSelection__WEBPACK_IMPORTED_MODULE_3__["default"]]
    };
    game = new Phaser.Game(gameConfig);
    resize();
    window.addEventListener("resize", resize, false);
};
function resize() {
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}
;


/***/ }),

/***/ "./Scripts/Scenes/ODBoot.ts":
/*!**********************************!*\
  !*** ./Scripts/Scenes/ODBoot.ts ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GameObjects_ODMissile__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../GameObjects/ODMissile */ "./Scripts/GameObjects/ODMissile.ts");
/* harmony import */ var _GameObjects_ODRedExplosion__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../GameObjects/ODRedExplosion */ "./Scripts/GameObjects/ODRedExplosion.ts");
/* harmony import */ var _GameObjects_ODOcean__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../GameObjects/ODOcean */ "./Scripts/GameObjects/ODOcean.ts");
/* harmony import */ var _GameObjects_ODYellowGunBase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../GameObjects/ODYellowGunBase */ "./Scripts/GameObjects/ODYellowGunBase.ts");
/* harmony import */ var _GameObjects_ODBlueGunBase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../GameObjects/ODBlueGunBase */ "./Scripts/GameObjects/ODBlueGunBase.ts");
/* harmony import */ var _GameObjects_ODGreyGunBase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../GameObjects/ODGreyGunBase */ "./Scripts/GameObjects/ODGreyGunBase.ts");
/* harmony import */ var _GameObjects_ODYellowGun__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../GameObjects/ODYellowGun */ "./Scripts/GameObjects/ODYellowGun.ts");
/* harmony import */ var _GameObjects_ODBlueGun__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../GameObjects/ODBlueGun */ "./Scripts/GameObjects/ODBlueGun.ts");
/* harmony import */ var _GameObjects_ODGreyGun__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../GameObjects/ODGreyGun */ "./Scripts/GameObjects/ODGreyGun.ts");
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");
/* harmony import */ var _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./../Models/GameInfoModels/ODLevelInfo */ "./Scripts/Models/GameInfoModels/ODLevelInfo.ts");
/* harmony import */ var _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./../Models/GameInfoModels/ODRadarInfo */ "./Scripts/Models/GameInfoModels/ODRadarInfo.ts");
/* harmony import */ var _Managers_ODCacheManager__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./../Managers/ODCacheManager */ "./Scripts/Managers/ODCacheManager.ts");
/* harmony import */ var _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./../Models/RadarEvent */ "./Scripts/Models/RadarEvent.ts");
/* harmony import */ var _GameObjects_ODWhiteExplosion__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./../GameObjects/ODWhiteExplosion */ "./Scripts/GameObjects/ODWhiteExplosion.ts");















class ODBoot extends Phaser.Scene {
    constructor() {
        super({
            key: 'BootScene'
        });
    }
    init() {
        document.fonts.load('10pt "ethnocentric_rg"');
        this.sys.plugins.registerGameObject('redexplosion', function (x, y) {
            let redExplosion = new _GameObjects_ODRedExplosion__WEBPACK_IMPORTED_MODULE_1__["default"](this.scene, x, y, 'ODImages', 'redexplode0004.png');
            this.displayList.add(redExplosion);
            this.updateList.add(redExplosion);
            return redExplosion;
        });
        this.sys.plugins.registerGameObject('whiteexplosion', function (x, y) {
            let whiteExplosion = new _GameObjects_ODWhiteExplosion__WEBPACK_IMPORTED_MODULE_14__["default"](this.scene, x, y, 'ODImages', 'explodewhite0003.png');
            this.displayList.add(whiteExplosion);
            this.updateList.add(whiteExplosion);
            return whiteExplosion;
        });
        this.sys.plugins.registerGameObject('missile', function (x, y) {
            let missile = new _GameObjects_ODMissile__WEBPACK_IMPORTED_MODULE_0__["default"](this.scene, x, y, 'ODImages', 'greenmissile.png');
            this.displayList.add(missile);
            this.updateList.add(missile);
            return missile;
        });
        this.sys.plugins.registerGameObject('ocean', function (x, y) {
            let ocean = new _GameObjects_ODOcean__WEBPACK_IMPORTED_MODULE_2__["default"](this.scene, x, y, 'ODImages', 'ocean.png');
            this.displayList.add(ocean);
            this.updateList.add(ocean);
            return ocean;
        });
        this.sys.plugins.registerGameObject('sky', function (x, y) {
            let sky = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanborder.png');
            this.displayList.add(sky);
            this.updateList.add(sky);
            return sky;
        });
        this.sys.plugins.registerGameObject('yellowbase', function (x, y) {
            let ybase = new _GameObjects_ODYellowGunBase__WEBPACK_IMPORTED_MODULE_3__["default"](this.scene, x, y, 'ODImages', 'baseyellow.png');
            this.displayList.add(ybase);
            this.updateList.add(ybase);
            return ybase;
        });
        this.sys.plugins.registerGameObject('bluebase', function (x, y) {
            let bbase = new _GameObjects_ODBlueGunBase__WEBPACK_IMPORTED_MODULE_4__["default"](this.scene, x, y, 'ODImages', 'baseblue.png');
            this.displayList.add(bbase);
            this.updateList.add(bbase);
            return bbase;
        });
        this.sys.plugins.registerGameObject('greybase', function (x, y) {
            let gbase = new _GameObjects_ODGreyGunBase__WEBPACK_IMPORTED_MODULE_5__["default"](this.scene, x, y, 'ODImages', 'basegrey.png');
            this.displayList.add(gbase);
            this.updateList.add(gbase);
            return gbase;
        });
        this.sys.plugins.registerGameObject('yellowgun', function (x, y) {
            let ygun = new _GameObjects_ODYellowGun__WEBPACK_IMPORTED_MODULE_6__["default"](this.scene, x, y, 'ODImages', 'gunyellow.png');
            this.displayList.add(ygun);
            this.updateList.add(ygun);
            return ygun;
        });
        this.sys.plugins.registerGameObject('bluegun', function (x, y) {
            let bgun = new _GameObjects_ODBlueGun__WEBPACK_IMPORTED_MODULE_7__["default"](this.scene, x, y, 'ODImages', 'gunblue.png');
            this.displayList.add(bgun);
            this.updateList.add(bgun);
            return bgun;
        });
        this.sys.plugins.registerGameObject('greygun', function (x, y) {
            let ggun = new _GameObjects_ODGreyGun__WEBPACK_IMPORTED_MODULE_8__["default"](this.scene, x, y, 'ODImages', 'gungrey.png');
            this.displayList.add(ggun);
            this.updateList.add(ggun);
            return ggun;
        });
        this.sys.plugins.registerGameObject('tvmonitorempty', function (x, y) {
            let tv = new _GameObjects_ODGreyGunBase__WEBPACK_IMPORTED_MODULE_5__["default"](this.scene, x, y, 'ODImages', 'tvmonitoremptyframed.png');
            this.displayList.add(tv);
            this.updateList.add(tv);
            return tv;
        });
        this.sys.plugins.registerGameObject('cloud1', function (x, y) {
            let cloud1 = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'cloud1.png');
            this.displayList.add(cloud1);
            this.updateList.add(cloud1);
            return cloud1;
        });
        this.sys.plugins.registerGameObject('cloud2', function (x, y) {
            let cloud2 = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'cloud2.png');
            this.displayList.add(cloud2);
            this.updateList.add(cloud2);
            return cloud2;
        });
        this.sys.plugins.registerGameObject('statictv', function (x, y) {
            let statictv = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'staticmonitor-000.png');
            this.displayList.add(statictv);
            this.updateList.add(statictv);
            return statictv;
        });
        this.sys.plugins.registerGameObject('bluebutton', function (x, y) {
            let bluebutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'bluebutton.png');
            this.displayList.add(bluebutton);
            this.updateList.add(bluebutton);
            return bluebutton;
        });
        this.sys.plugins.registerGameObject('greenbutton', function (x, y) {
            let greenbutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'greenbutton.png');
            this.displayList.add(greenbutton);
            this.updateList.add(greenbutton);
            return greenbutton;
        });
        this.sys.plugins.registerGameObject('levellockbutton', function (x, y) {
            let levellockbutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'levellockbutton.png');
            this.displayList.add(levellockbutton);
            this.updateList.add(levellockbutton);
            return levellockbutton;
        });
        this.sys.plugins.registerGameObject('backbutton', function (x, y) {
            let backbutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'backbutton.png');
            this.displayList.add(backbutton);
            this.updateList.add(backbutton);
            return backbutton;
        });
        this.sys.plugins.registerGameObject('shield', function (x, y) {
            let shield = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'shield.png');
            this.displayList.add(shield);
            this.updateList.add(shield);
            return shield;
        });
        this.sys.plugins.registerGameObject('pausebutton', function (x, y) {
            let pausebutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'pausebutton.png');
            this.displayList.add(pausebutton);
            this.updateList.add(pausebutton);
            return pausebutton;
        });
        this.sys.plugins.registerGameObject('oceaninnerbar', function (x, y) {
            let oceaninnerbar = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceaninnerbar.png');
            this.displayList.add(oceaninnerbar);
            this.updateList.add(oceaninnerbar);
            return oceaninnerbar;
        });
        this.sys.plugins.registerGameObject('oceanmiddlebar', function (x, y) {
            let oceanmiddlebar = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanmiddlebar.png');
            this.displayList.add(oceanmiddlebar);
            this.updateList.add(oceanmiddlebar);
            return oceanmiddlebar;
        });
        this.sys.plugins.registerGameObject('oceanouterbar', function (x, y) {
            let oceanouterbar = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanouterbar.png');
            this.displayList.add(oceanouterbar);
            this.updateList.add(oceanouterbar);
            return oceanouterbar;
        });
        this.sys.plugins.registerGameObject('playbutton', function (x, y) {
            let playbutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'playbutton.png');
            this.displayList.add(playbutton);
            this.updateList.add(playbutton);
            return playbutton;
        });
        this.sys.plugins.registerGameObject('replaybutton', function (x, y) {
            let replaybutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'replaybutton.png');
            this.displayList.add(replaybutton);
            this.updateList.add(replaybutton);
            return replaybutton;
        });
        this.sys.plugins.registerGameObject('homebutton', function (x, y) {
            let homebutton = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'homebutton.png');
            this.displayList.add(homebutton);
            this.updateList.add(homebutton);
            return homebutton;
        });
        this.sys.plugins.registerGameObject('pausepanel', function (x, y) {
            let pausepanel = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'pausepanel.png');
            this.displayList.add(pausepanel);
            this.updateList.add(pausepanel);
            return pausepanel;
        });
    }
    preload() {
        this.load.multiatlas('ODImages', 'assets/ODII.json', 'assets');
        this.registry.set('Radar1', { x: -10, y: -10 });
        this.registry.set('Radar2', { x: -10, y: -10 });
        this.registry.set('Radar3', { x: -10, y: -10 });
        this.registry.set('ActiveRadar', {});
        this.registry.set('SetMonitorExplosion', {});
        this.registry.set('TrashAtWater', -1);
        this.registry.set('MissileInFinalPosition', -1);
        this.registry.set('SetRadarBorder', {});
        this.registry.set('StartCoolDown', {});
        this.registry.set('EndCoolDown', {});
        this.registry.set('TrashDestroyed', -1);
        this.registry.set('RadarUpdateCycle1', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_13__["default"]('initial1'));
        this.registry.set('RadarUpdateCycle2', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_13__["default"]('initial2'));
        this.registry.set('RadarUpdateCycle3', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_13__["default"]('initial3'));
        this.registry.set('GameEventRadarWon', {});
        this.registry.set('GameEventRadarLost', {});
        this.registry.set('GamePause', {});
    }
    create() {
        this.anims.create({
            key: 'RedExplode',
            frames: this.anims.generateFrameNames('ODImages', { start: 4, end: 35, zeroPad: 4, prefix: 'redexplode', suffix: '.png' }),
            frameRate: 18,
            repeat: 0,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'WhiteExplode',
            frames: this.anims.generateFrameNames('ODImages', { start: 3, end: 46, zeroPad: 4, prefix: 'explodewhite', suffix: '.png' }),
            frameRate: 20,
            repeat: 0,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'Trash',
            frames: this.anims.generateFrameNames('ODImages', { start: 0, end: 14, zeroPad: 3, prefix: 'trashcollection-', suffix: '.png' }),
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'StaticTv',
            frames: this.anims.generateFrameNames('ODImages', { start: 0, end: 8, zeroPad: 3, prefix: 'staticmonitor-', suffix: '.png' }),
            frameRate: 8,
            repeat: -1,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'Base',
            frames: [
                { key: 'ODImages', frame: 'baseyellow.png' },
                { key: 'ODImages', frame: 'baseblue.png' },
                { key: 'ODImages', frame: 'basegrey.png' }
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'Gun',
            frames: [
                { key: 'ODImages', frame: 'gunyellow.png' },
                { key: 'ODImages', frame: 'gunblue.png' },
                { key: 'ODImages', frame: 'gungrey.png' }
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });
        this.anims.create({
            key: 'EmptyRadars',
            frames: [
                { key: 'ODImages', frame: 'tvmonitoremptyframed.png' },
                { key: 'ODImages', frame: 'tvmonitoremptyframedgreen.png' }
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });
        this.SetGameInfo();
    }
    transitionToScene(key) {
        this.scene.start(key);
    }
    SetGameInfo() {
        _Managers_ODCacheManager__WEBPACK_IMPORTED_MODULE_12__["default"].getItem('ODGameInfo').then((data) => {
            if (data) {
                _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance = data;
                this.transitionToScene('TitleScreen');
            }
            else {
                this.SetDefaultGameInfo();
            }
        }, (error) => {
            console.log('Get ODGameInfo from cache was rejected:');
            console.log(error);
            this.SetDefaultGameInfo();
        }).catch((reason) => {
            console.log('Get ODGameInfo from cache exception:');
            console.log(reason);
            this.SetDefaultGameInfo();
        });
    }
    SetDefaultGameInfo() {
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.world.currLevel = 0;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.world.currPoints = 0;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.world.hiLevel = 0;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.world.hiPoints = 0;
        let level0 = new _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__["default"]();
        level0.cleared = true;
        level0.levelNum = 0;
        level0.missileSpeed = 300;
        level0.name = 'Title Screen';
        level0.pointValue = 0;
        level0.unlocked = true;
        level0.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 1500, 80, 150, 5, 5));
        level0.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 2000, 80, 150, 5, 5));
        level0.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 1500, 80, 150, 5, 5));
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.levels.push(level0);
        let level1 = new _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__["default"]();
        level1.cleared = false;
        level1.levelNum = 1;
        level1.missileSpeed = 300;
        level1.name = 'Level 1';
        level1.pointValue = 1;
        level1.unlocked = true;
        level1.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 3500, 80, 120, 2, 5));
        level1.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 80, 120, 2, 5));
        level1.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 80, 120, 2, 5));
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.levels.push(level1);
        let level2 = new _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__["default"]();
        level2.cleared = false;
        level2.levelNum = 2;
        level2.missileSpeed = 300;
        level2.name = 'Level 2';
        level2.pointValue = 5;
        level2.unlocked = false;
        level2.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 3000, 150, 250, 3, 5));
        level2.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        level2.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.levels.push(level2);
        let level3 = new _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__["default"]();
        level3.cleared = false;
        level3.levelNum = 3;
        level3.missileSpeed = 300;
        level3.name = 'Level 3';
        level3.pointValue = 5;
        level3.unlocked = false;
        level3.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 3000, 150, 250, 3, 5));
        level3.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        level3.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.levels.push(level3);
        let level4 = new _Models_GameInfoModels_ODLevelInfo__WEBPACK_IMPORTED_MODULE_10__["default"]();
        level4.cleared = false;
        level4.levelNum = 4;
        level4.missileSpeed = 300;
        level4.name = 'Level 4';
        level4.pointValue = 5;
        level4.unlocked = false;
        level4.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](true, 3000, 150, 250, 3, 5));
        level4.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        level4.radars.push(new _Models_GameInfoModels_ODRadarInfo__WEBPACK_IMPORTED_MODULE_11__["default"](false, 3500, 100, 200, 2, 5));
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_9__["default"].instance.levels.push(level4);
        this.transitionToScene('TitleScreen');
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODBoot);


/***/ }),

/***/ "./Scripts/Scenes/ODGameScene.ts":
/*!***************************************!*\
  !*** ./Scripts/Scenes/ODGameScene.ts ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODMonitor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODMonitor */ "./Scripts/Scenes/ODMonitor.ts");
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");
/* harmony import */ var _ODRadar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ODRadar */ "./Scripts/Scenes/ODRadar.ts");
/* harmony import */ var _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../Models/RadarEvent */ "./Scripts/Models/RadarEvent.ts");
/* harmony import */ var _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../Models/MonitorClickEvent */ "./Scripts/Models/MonitorClickEvent.ts");
/* harmony import */ var _ODPauseScene__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ODPauseScene */ "./Scripts/Scenes/ODPauseScene.ts");






class ODGameScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'GameScreen'
        });
    }
    init(data) {
        this.monitor = undefined;
        this.fadeTime = 1000;
        if (data.resetScore == true) {
            _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.currPoints = 0;
        }
        this.radars = new Array();
    }
    create() {
        for (let i = 0; i < 3; i++) {
            let key = 'Radar' + (i + 1).toString();
            let currLevel = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.currLevel;
            let radar = new _ODRadar__WEBPACK_IMPORTED_MODULE_2__["default"](key, 0 + (160 * i), 630, 160, 212.8, (i == 0) ? true : false, this.fadeTime);
            this.scene.add(key, radar, true);
            radar.SetRadarOn(_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.levels[currLevel].radars[i].active);
            this.radars.push(radar);
        }
        let monitorWidth = this.game.config.width - 40;
        this.monitor = new _ODMonitor__WEBPACK_IMPORTED_MODULE_0__["default"]('Monitor1', (this.game.config.width / 2.0) - (monitorWidth / 2.0), 30, monitorWidth, monitorWidth * 1.33, this.fadeTime);
        this.scene.add('Monitor1', this.monitor, true);
        this.pausePanel = new _ODPauseScene__WEBPACK_IMPORTED_MODULE_5__["default"]('PausePanel', (this.game.config.width / 2.0) - (350 / 2.0), (this.monitor.y) + (50), 350, 500);
        this.scene.add('PausePanel', this.pausePanel, false);
        this.registry.set('ActiveRadar', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_3__["default"]('Radar1'));
        this.registry.events.on('changedata', (parent, key, data) => {
            switch (key) {
                case 'GameEventRadarWon':
                    this.ProcessGameEventRadarWon(data);
                    break;
                case 'GameEventRadarLost':
                    this.ProcessGameEventRadarLost();
                    break;
                case 'GamePause':
                    this.ProcessGamePause(data);
                    break;
            }
        });
        this.cameras.main.fadeIn(this.fadeTime, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.fadeColor.red, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.fadeColor.green, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.fadeColor.blue);
        console.log('Show Level Info');
        this.time.addEvent({
            delay: 2000,
            callback: () => {
                console.log('Start radar timers');
                this.radars.forEach((radar) => { radar.StartTimer(); });
            }
        });
    }
    ProcessGameEventRadarLost() {
        this.transitionToScene('TitleScreen');
    }
    ProcessGamePause(data) {
        if (data.restartLevel) {
            this.restartScene();
        }
        else if (data.gotoTitleScreen) {
            this.transitionToScene('TitleScreen');
        }
        else if (data.isPaused) {
            this.scene.run('PausePanel');
            this.scene.pause(this.monitor.key);
            this.radars.forEach((radar) => {
                this.scene.pause(radar.key);
            });
        }
        else {
            this.scene.sleep('PausePanel');
            this.scene.resume(this.monitor.key);
            this.radars.forEach((radar) => {
                this.scene.resume(radar.key);
            });
        }
    }
    ProcessGameEventRadarWon(data) {
        let foundAvailableRadar = this.radars.find(x => x.GetRadarOn() && x.key != data.key);
        if (foundAvailableRadar) {
            this.registry.set('ActiveRadar', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_3__["default"](foundAvailableRadar.key));
            this.registry.set('SetRadarBorder', new _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__["default"](foundAvailableRadar.key));
        }
        else {
            this.restartScene();
        }
    }
    prepareForSceneTransition() {
        this.radars.length = 0;
        this.scene.remove('PausePanel');
        this.scene.remove('Radar1');
        this.scene.remove('Radar2');
        this.scene.remove('Radar3');
        this.scene.remove('Monitor1');
        this.registry.events.off('changedata', undefined, undefined, false);
    }
    restartScene() {
        this.prepareForSceneTransition();
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.levels[_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.currLevel].cleared = true;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.currLevel += 1;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.levels[_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_1__["default"].instance.world.currLevel].unlocked = true;
        this.scene.restart({ resetScore: false });
    }
    transitionToScene(key) {
        this.prepareForSceneTransition();
        this.scene.start(key, { resetScore: true });
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODGameScene);


/***/ }),

/***/ "./Scripts/Scenes/ODLevelSelection.ts":
/*!********************************************!*\
  !*** ./Scripts/Scenes/ODLevelSelection.ts ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");

class ODLevelSelection extends Phaser.Scene {
    constructor() {
        super({ key: 'LevelSelection' });
        this.fadeTime = 1000;
    }
    create() {
        this.add.text(5, 15, 'Select A Level', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 35,
            color: '#16559d'
        });
        this.add.backbutton(447, 38).setScale(0.5).setInteractive().on('pointerdown', (pointer) => {
            this.scene.start('TitleScreen');
        });
        let startRow = 200;
        let startCol = 90;
        for (let i = 1; i < _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levels.length; i++) {
            let levelInfo = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.levels[i];
            let buttonToRender;
            if (levelInfo.cleared && levelInfo.unlocked) {
                buttonToRender = this.add.greenbutton(startCol, startRow).setDisplaySize(121, 130).setInteractive().on('pointerdown', () => { this.processLevelClick(i); });
                this.add.text(startCol, startRow - 10, i.toString(), {
                    fontFamily: 'ethnocentric_rg',
                    fontSize: 64,
                    color: '#ffffff'
                }).setOrigin(0.5, 0.5);
            }
            else if (levelInfo.unlocked) {
                buttonToRender = this.add.bluebutton(startCol, startRow).setDisplaySize(121, 130).setInteractive().on('pointerdown', () => { this.processLevelClick(i); });
                this.add.text(startCol, startRow - 10, i.toString(), {
                    fontFamily: 'ethnocentric_rg',
                    fontSize: 64,
                    color: '#ffffff'
                }).setOrigin(0.5, 0.5);
            }
            else {
                buttonToRender = this.add.levellockbutton(startCol, startRow).setDisplaySize(121, 130);
            }
            buttonToRender.setDisplaySize(121, 130);
            startCol += buttonToRender.displayWidth + 25;
            if (startCol >= 400) {
                startCol = 90;
                startRow += buttonToRender.displayHeight + 25;
            }
        }
        this.cameras.main.fadeIn(this.fadeTime, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.world.fadeColor.red, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.world.fadeColor.green, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.world.fadeColor.blue);
    }
    processLevelClick(levelNum) {
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_0__["default"].instance.world.currLevel = levelNum;
        this.scene.start('GameScreen', { resetScore: true });
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODLevelSelection);


/***/ }),

/***/ "./Scripts/Scenes/ODMonitor.ts":
/*!*************************************!*\
  !*** ./Scripts/Scenes/ODMonitor.ts ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GameObjects_ODTrash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../GameObjects/ODTrash */ "./Scripts/GameObjects/ODTrash.ts");
/* harmony import */ var _Enums_ODState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../Enums/ODState */ "./Scripts/Enums/ODState.ts");
/* harmony import */ var _GameObjects_ODMissile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../GameObjects/ODMissile */ "./Scripts/GameObjects/ODMissile.ts");
/* harmony import */ var _Enums_ODBaseEnum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../Enums/ODBaseEnum */ "./Scripts/Enums/ODBaseEnum.ts");
/* harmony import */ var _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../Models/MonitorClickEvent */ "./Scripts/Models/MonitorClickEvent.ts");
/* harmony import */ var _GameObjects_ODRedExplosion__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../GameObjects/ODRedExplosion */ "./Scripts/GameObjects/ODRedExplosion.ts");
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");
/* harmony import */ var _GameObjects_ODWhiteExplosion__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../GameObjects/ODWhiteExplosion */ "./Scripts/GameObjects/ODWhiteExplosion.ts");
/* harmony import */ var _Models_GameEvent__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../Models/GameEvent */ "./Scripts/Models/GameEvent.ts");









class ODMonitor extends Phaser.Scene {
    constructor(key, x, y, displayWidth, displayHeight, fadeTime) {
        super({
            key
        });
        this.key = key;
        this.x = x;
        this.y = y;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
        this.activeRadar = '';
        this.ignoreInput = false;
        this.fadeTime = fadeTime;
        this.isPaused = false;
    }
    preload() {
        this.trashPool = this.add.group({
            maxSize: 20,
            defaultKey: 'ODImages',
            defaultFrame: 'trashcollection-000.png',
            classType: _GameObjects_ODTrash__WEBPACK_IMPORTED_MODULE_0__["default"],
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.trashTrackingPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'trashcollection-000.png',
            classType: _GameObjects_ODTrash__WEBPACK_IMPORTED_MODULE_0__["default"],
            createCallback: (trash) => {
                trash.ignore = true;
            },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.missilePool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'greenmissile.png',
            classType: _GameObjects_ODMissile__WEBPACK_IMPORTED_MODULE_2__["default"],
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.explosionPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'redexplode0004.png',
            classType: _GameObjects_ODRedExplosion__WEBPACK_IMPORTED_MODULE_5__["default"],
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.explosionTrackingPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'explodewhite0003.png',
            classType: _GameObjects_ODWhiteExplosion__WEBPACK_IMPORTED_MODULE_7__["default"],
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.cloudPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'cloud1.png',
            classType: Phaser.GameObjects.Sprite,
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.fishPool = this.add.group({
            maxSize: 5,
            defaultKey: 'ODImages',
            defaultFrame: 'fish1.png',
            classType: Phaser.GameObjects.Sprite,
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
    }
    create() {
        this.events.on('resume', (data) => {
            if (_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currLevel > 0 && this.pauseButton && this.pauseButton.active) {
                this.pauseButton.setVisible(true);
                this.isPaused = false;
            }
        });
        this.cameras.main.setViewport(this.x, this.y, this.displayWidth, this.displayHeight);
        this.add.sky(this.displayWidth / 2.0, this.displayHeight / 2.0).setDisplaySize(this.displayWidth, this.displayHeight);
        this.cloudPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, (0.5769 * this.displayWidth), (0.5784 * this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.25, 0.20),
            (new Phaser.GameObjects.Sprite(this, (0.3077 * this.displayWidth), (0.1735 * this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.20, 0.25),
            (new Phaser.GameObjects.Sprite(this, (0.1538 * this.displayWidth), (0.3470 * this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.15, 0.15),
            (new Phaser.GameObjects.Sprite(this, (0.7692 * this.displayWidth), (0.1735 * this.displayHeight), 'ODImages', 'cloud1.png')).setScale(0.20, 0.15),
            (new Phaser.GameObjects.Sprite(this, (0.3846 * this.displayWidth), (0.2892 * this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.25, 0.20),
            (new Phaser.GameObjects.Sprite(this, (0.7692 * this.displayWidth), (0.3470 * this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.35, 0.20),
            (new Phaser.GameObjects.Sprite(this, (0.2692 * this.displayWidth), (0.4627 * this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.15, 0.20),
            (new Phaser.GameObjects.Sprite(this, (0.7692 * this.displayWidth), (0.4916 * this.displayHeight), 'ODImages', 'cloud2.png')).setScale(0.15, 0.15)
        ], true);
        this.base = this.add.yellowbase(this.displayWidth / 2.0, this.displayHeight - (0.1420 * this.displayHeight)).setScale(0.4).setDepth(2);
        this.gun = this.add.yellowgun(this.base.x, this.base.y - 7).setScale(0.25).setDepth(2).setAngle(0);
        this.SetBaseAndGunColor();
        this.add.ocean(this.displayWidth / 2.0, this.displayHeight - (0.0825 * this.displayHeight)).setDisplaySize(this.displayWidth - 20, (0.0968 * this.displayHeight)).setDepth(1);
        this.fishPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, (0.3077 * this.displayWidth), (0.8965 * this.displayHeight), 'ODImages', 'fish1.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.5000 * this.displayWidth), (0.9254 * this.displayHeight), 'ODImages', 'fish2.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.6538 * this.displayWidth), (0.8965 * this.displayHeight), 'ODImages', 'fish3.png')).setScale(0.35).setDepth(1),
            (new Phaser.GameObjects.Sprite(this, (0.7308 * this.displayWidth), (0.9254 * this.displayHeight), 'ODImages', 'fish4.png')).setScale(0.35).setDepth(1)
        ], true);
        this.tweens.timeline({
            targets: [this.base],
            loop: -1,
            tweens: [
                {
                    angle: -25,
                    ease: 'Sine.easeInOut',
                    duration: 3000
                },
                {
                    offset: 3000,
                    angle: 25,
                    ease: 'Sine.easeInOut',
                    duration: 6000
                },
                {
                    offset: 9000,
                    angle: 0,
                    ease: 'Sine.easeInOut',
                    duration: 3000
                }
            ]
        });
        this.add.tvmonitorempty(0, 0).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(2).setInteractive().on(Phaser.Input.Events.GAMEOBJECT_POINTER_DOWN, (pointer, event, event2, event3, event4) => {
            if (!this.ignoreInput) {
                this.ProcessClick(pointer);
            }
        });
        this.statictv = this.add.statictv(0, 0).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(3).setActive(false).setVisible(false);
        this.oceanoutterbar = this.add.oceanouterbar((this.displayWidth / 2.0) - ((this.displayWidth / 260.0) * 35), this.displayHeight - ((this.displayHeight / 345.8) * 18)).setDisplaySize((this.displayWidth / 260.0) * 70.84, (this.displayHeight / 345.8) * 10.08).setDepth(1);
        (this.add).oceanmiddlebar(this.oceanoutterbar.x, this.oceanoutterbar.y).setDisplaySize(this.oceanoutterbar.displayWidth, this.oceanoutterbar.displayHeight).setDepth(1);
        this.oceaninnerbar = this.add.oceaninnerbar(this.oceanoutterbar.x + 2, this.oceanoutterbar.y + 2).setDisplaySize(this.oceanoutterbar.displayWidth - 4, this.oceanoutterbar.displayHeight - 4).setDepth(1);
        this.oceaninnerbardefault = this.oceaninnerbar.displayWidth;
        this.add.shield(100 * (this.displayWidth / 260.0), 30 * (this.displayHeight / 345.8)).setScale(0.35 * (this.displayWidth / 260.0)).setDepth(2);
        this.scoreText = this.add.text(125 * (this.displayWidth / 260.0), 15 * (this.displayHeight / 345.8), _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currPoints.toString(), {
            fontFamily: 'ethnocentric_rg',
            fontSize: 24 * (this.displayWidth / 260.0),
            color: '#FFCC02'
        });
        if (_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currLevel > 0) {
            this.pauseButton = this.add.pausebutton(this.displayWidth - 25, 25).setScale(0.30).setDepth(2).setInteractive();
            this.pauseButton.on('pointerdown', (pointer) => {
                this.PauseButtonClick(pointer);
            });
        }
        this.SetUpParticalSystem();
        this.registry.events.on('changedata', (parent, key, data) => {
            switch (key) {
                case 'RadarUpdateCycle1':
                    this.ProcessRadarUpdateCycle(data);
                    break;
                case 'RadarUpdateCycle2':
                    this.ProcessRadarUpdateCycle(data);
                    break;
                case 'RadarUpdateCycle3':
                    this.ProcessRadarUpdateCycle(data);
                    break;
                case 'ActiveRadar':
                    this.ProcessActiveRadar(data);
                    break;
                case 'TrashAtWater':
                    this.ProcessTrashAtWater(data);
                    break;
                case 'TrashDestroyed':
                    this.ProcessTrashDestroyed(data);
                    break;
                case 'MissileInFinalPosition':
                    this.ProcessMissileInFinalPosition(data);
                    break;
                case 'SetMonitorExplosion':
                    this.ProcessSetMonitorExplosion(data);
                    break;
                case 'StartCoolDown':
                    this.ProcessStartCoolDown(data);
                    break;
                case 'EndCoolDown':
                    this.ProcessEndCoolDown(data);
                    break;
            }
        });
        this.cameras.main.fadeIn(this.fadeTime, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.fadeColor.red, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.fadeColor.green, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.fadeColor.blue);
    }
    update(time, dt) {
        Phaser.Actions.Call(this.trashPool.getChildren().filter((x) => x.active), (trash) => {
            trash.setRotation(trash.rotation + (trash.rotationDelta * (dt / 1000.0)));
        }, this);
        let counter = 0;
        Phaser.Actions.Call(this.cloudPool.getChildren(), (cloud) => {
            cloud.x += (((dt / 1000) * 8) + (0.06 * counter));
            if (cloud.x - (cloud.displayWidth / 2.0) > this.displayWidth) {
                cloud.x = -cloud.displayWidth / 2.0;
            }
            counter += 1;
        }, this);
        Phaser.Actions.Call(this.fishPool.getChildren(), (fish) => {
            fish.x += ((dt / 1000) * 10);
            if (fish.x - (fish.displayWidth / 2.0) > this.displayWidth) {
                fish.x = -fish.displayWidth / 2.0;
            }
        }, this);
    }
    SetIgnoreInputOn(ignore) {
        this.ignoreInput = ignore;
    }
    DemoCreateMissile() {
        this.ProcessClick({ worldX: Phaser.Math.RND.realInRange(30, this.displayWidth - 30), worldY: Phaser.Math.RND.realInRange(50, this.gun.y - this.gun.height - 20) });
    }
    PauseButtonClick(pointer) {
        this.isPaused = !this.isPaused;
        let ge = new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_8__["default"]('GamePause', this.isPaused);
        this.pauseButton.setVisible(!this.isPaused);
        this.registry.set('GamePause', ge);
    }
    ProcessClick(pointer) {
        if (pointer.worldX < (0.0962 * this.displayWidth) || pointer.worldX > (0.9038 * this.displayWidth)
            || pointer.worldY < (0.1301 * this.displayHeight) || pointer.worldY > (0.7953 * this.displayHeight)) {
            return;
        }
        let angRad = Phaser.Math.Angle.Between(pointer.worldX, pointer.worldY, this.gun.x, this.gun.y);
        let gunAngle = Phaser.Math.RadToDeg(angRad) - 90;
        this.gun.setAngle(gunAngle);
        let clickEvent = new _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__["default"](this.activeRadar);
        clickEvent.initialX = this.gun.x - (this.gun.displayHeight * Math.cos(angRad));
        clickEvent.initialY = this.gun.y - (this.gun.displayHeight * Math.sin(angRad));
        clickEvent.finalX = pointer.worldX;
        clickEvent.finalY = pointer.worldY;
        clickEvent.monitorHeight = this.displayHeight;
        clickEvent.monitorWidth = this.displayWidth;
        this.registry.set(this.activeRadar, clickEvent);
    }
    SetBaseAndGunColor() {
        let result = _Enums_ODBaseEnum__WEBPACK_IMPORTED_MODULE_3__["default"].Yellow;
        if (this.activeRadar == 'Radar2') {
            result = _Enums_ODBaseEnum__WEBPACK_IMPORTED_MODULE_3__["default"].Blue;
        }
        else if (this.activeRadar == 'Radar3') {
            result = _Enums_ODBaseEnum__WEBPACK_IMPORTED_MODULE_3__["default"].Grey;
        }
        this.base.play('Base', false, result);
        this.gun.play('Gun', false, result);
    }
    SetUpParticalSystem() {
        this.particleManager = this.add.particles('ODImages', 'watersplash.png');
        for (let i = 1; i <= 3; i++) {
            this.particleManager.createEmitter({
                name: 'Radar' + i.toString(),
                x: this.displayWidth / 2.0,
                y: this.displayHeight / 2.0,
                angle: { min: 260, max: 280 },
                speed: { min: 100, max: 180 },
                gravityY: 850,
                accelerationY: -350,
                scale: 0.5,
                blendMode: Phaser.BlendModes.COLOR,
                maxParticles: 1000,
                lifespan: 600,
                radial: true,
                quantity: 50,
                on: false
            });
        }
    }
    ProcessRadarUpdateCycle(data) {
        if (data.key == this.activeRadar) {
            data.missiles.forEach((red) => {
                let foundMissile = this.missilePool.getChildren().find(x => x.id == red.id && x.active);
                if (!foundMissile) {
                    foundMissile = this.missilePool.get(0, 0);
                    foundMissile.setScale(0.05);
                    foundMissile.id = red.id;
                    foundMissile.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
                    foundMissile.tag = data.key;
                }
                foundMissile.x = red.x * (this.displayWidth / red.displayWidth);
                foundMissile.y = red.y * (this.displayHeight / red.displayHeight);
            });
            data.trash.forEach((red) => {
                let foundTrash = this.trashPool.getChildren().find(x => x.id == red.id && x.active);
                if (!foundTrash) {
                    foundTrash = this.trashPool.get(0, 0);
                    foundTrash.play('Trash', false, Phaser.Math.RND.between(0, 14));
                    foundTrash.rotationDelta = Phaser.Math.RND.realInRange(-5.0, 5.0);
                    foundTrash.setScale(0.25);
                    foundTrash.id = red.id;
                    foundTrash.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
                    foundTrash.tag = data.key;
                }
                foundTrash.x = red.x * (this.displayWidth / red.displayWidth);
                foundTrash.y = red.y * (this.displayHeight / red.displayHeight);
            });
            if (_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currLevel > 0) {
                let trashTackerExplosionNeeded = data.trashToClear < this.trashTrackingPool.getTotalUsed();
                this.trashTrackingPool.getChildren().forEach((trash) => { trash.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive); });
                let initX = 40;
                let initY = 60;
                for (let i = 0; i < data.trashToClear; i++) {
                    let trash = this.trashTrackingPool.get(initX, initY);
                    trash.setScale(0.20).setDepth(2).setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
                    initX += 20;
                    if (initX > 150) {
                        initX = 40;
                        initY += 23;
                    }
                }
                if (trashTackerExplosionNeeded) {
                    let trackExplosion = this.explosionTrackingPool.get(initX, initY).setScale(0.20).setDepth(2);
                    trackExplosion.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
                    trackExplosion.play('WhiteExplode');
                    trackExplosion.on('animationcomplete', (animation, frame, gameObject) => {
                        gameObject.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
                    });
                }
            }
            this.oceaninnerbar.displayWidth = this.oceaninnerbardefault * data.oceanHealth;
        }
    }
    ProcessStartCoolDown(data) {
        this.base.setAlpha(0.5);
        this.gun.setAlpha(0.5);
    }
    ProcessEndCoolDown(data) {
        this.base.setAlpha(1.0);
        this.gun.setAlpha(1.0);
    }
    ProcessSetMonitorExplosion(data) {
        data.missiles.forEach((detail) => {
            let explosion = this.explosionPool.get(detail.x * (this.displayWidth / detail.displayWidth), detail.y * (this.displayHeight / detail.displayHeight)).setDepth(2).setScale(0.35).play('RedExplode');
            explosion.tag = data.key;
            explosion.id = detail.id;
            explosion.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
            explosion.on('animationcomplete', (animation, frame, gameObject) => {
                gameObject.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            });
        });
    }
    ProcessActiveRadar(data) {
        if (data.key != this.activeRadar) {
            this.trashPool.getChildren().filter((x) => x.active && x.tag == this.activeRadar).forEach((x) => {
                x.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            });
            this.missilePool.getChildren().filter((x) => x.active && x.tag == this.activeRadar).forEach((x) => {
                x.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            });
            this.explosionPool.getChildren().filter((x) => x.active && x.tag == this.activeRadar).forEach((x) => {
                x.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            });
            this.particleManager.emitters.getAll('name', this.activeRadar).forEach((emit) => {
                emit.visible = false;
            });
            this.activeRadar = data.key;
            this.SetBaseAndGunColor();
            this.statictv.setActive(true).setVisible(true);
            this.time.addEvent({
                delay: 100,
                callback: () => {
                    this.statictv.setActive(false).setVisible(false);
                }
            });
        }
    }
    ProcessMissileInFinalPosition(data) {
        data.missiles.forEach((detail) => {
            let foundMissile = (this.missilePool.getChildren().find((x) => x.id == detail.id && x.tag == data.key));
            if (foundMissile) {
                foundMissile.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            }
        });
    }
    DestroyTrash(data) {
        let foundTrash;
        data.trash.forEach((detail) => {
            foundTrash = (this.trashPool.getChildren().find((x) => x.id == detail.id && x.tag == data.key));
            if (foundTrash) {
                foundTrash.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            }
        });
        return foundTrash;
    }
    ProcessTrashDestroyed(data) {
        this.DestroyTrash(data);
        if (_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currLevel > 0) {
            _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currPoints += 1;
            this.scoreText.setText(_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_6__["default"].instance.world.currPoints.toString());
        }
    }
    ProcessTrashAtWater(data) {
        let foundTrash = this.DestroyTrash(data);
        if (foundTrash) {
            this.particleManager.emitters.getAll('name', this.activeRadar).forEach((emit) => {
                emit.visible = true;
                emit.emitParticleAt(foundTrash.x, this.displayHeight - (this.displayHeight * 0.0965));
            });
        }
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODMonitor);


/***/ }),

/***/ "./Scripts/Scenes/ODPauseScene.ts":
/*!****************************************!*\
  !*** ./Scripts/Scenes/ODPauseScene.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Models_GameEvent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../Models/GameEvent */ "./Scripts/Models/GameEvent.ts");

class ODPauseScene extends Phaser.Scene {
    constructor(key, x, y, displayWidth, displayHeight) {
        super({
            key
        });
        this.key = key;
        this.x = x;
        this.y = y;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
    }
    preload() {
    }
    create() {
        this.cameras.main.setViewport(this.x, this.y, this.displayWidth, this.displayHeight);
        this.add.pausepanel(this.displayWidth / 2.0, this.displayHeight / 2.0).setDisplaySize(this.displayWidth, this.displayHeight);
        let playButton = this.add.playbutton(this.displayWidth / 2.0, (this.displayHeight / 4.0) - 20).setInteractive().setDisplaySize(115, 124);
        playButton.on('pointerdown', (pointer) => {
            let ge = new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_0__["default"]('GamePause', false);
            this.registry.set('GamePause', ge);
        });
        let replayButton = this.add.replaybutton(this.displayWidth / 2.0, this.displayHeight / 2.0).setInteractive().setDisplaySize(115, 124);
        replayButton.on('pointerdown', (pointer) => {
            let ge = new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_0__["default"]('GamePause', false, true);
            this.registry.set('GamePause', ge);
        });
        this.add.homebutton(this.displayWidth / 2.0, replayButton.y + (replayButton.y - playButton.y)).setInteractive().setDisplaySize(115, 124).on('pointerdown', (pointer) => {
            let ge = new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_0__["default"]('GamePause', false, false, true);
            this.registry.set('GamePause', ge);
        });
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODPauseScene);


/***/ }),

/***/ "./Scripts/Scenes/ODRadar.ts":
/*!***********************************!*\
  !*** ./Scripts/Scenes/ODRadar.ts ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GameObjects_ODMissileMaker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../GameObjects/ODMissileMaker */ "./Scripts/GameObjects/ODMissileMaker.ts");
/* harmony import */ var _Enums_ODState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../Enums/ODState */ "./Scripts/Enums/ODState.ts");
/* harmony import */ var _GameObjects_ODTrashMarker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../GameObjects/ODTrashMarker */ "./Scripts/GameObjects/ODTrashMarker.ts");
/* harmony import */ var _GameObjects_ODExplosionMarker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../GameObjects/ODExplosionMarker */ "./Scripts/GameObjects/ODExplosionMarker.ts");
/* harmony import */ var _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../Models/MonitorClickEvent */ "./Scripts/Models/MonitorClickEvent.ts");
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");
/* harmony import */ var _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../Models/RadarEvent */ "./Scripts/Models/RadarEvent.ts");
/* harmony import */ var _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../Models/RadarDetailEvent */ "./Scripts/Models/RadarDetailEvent.ts");
/* harmony import */ var _Models_GameEvent__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../Models/GameEvent */ "./Scripts/Models/GameEvent.ts");









class ODRadar extends Phaser.Scene {
    constructor(key, xPos, yPos, displayWidth, displayHeight, isHighlighted, fadeTime) {
        super({
            key
        });
        this.key = key;
        this.xPos = xPos;
        this.yPos = yPos;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
        this.isHighlighted = isHighlighted;
        this.radarOn = true;
        this.fadeTime = fadeTime;
    }
    init() {
        this.currLevel = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.world.currLevel;
        this.radarIndex = parseInt(this.key[this.key.length - 1]) - 1;
        this.coolDownTime = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].coolDownTimer;
        this.oceanHealth = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].oceanMaxDamage;
        this.oceanHealthMax = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].oceanMaxDamage;
        this.trashTimer = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].trashSpawnTime;
        this.trashToClear = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].trashToClear;
        this.trashMinSpeed = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].trashMinSpeed;
        this.trashMaxSpeed = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].radars[this.radarIndex].trashMaxSpeed;
        this.missileSpeed = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.levels[this.currLevel].missileSpeed;
        this.flashTimer = undefined;
        this.coolDownTimer = undefined;
        this.canFire = true;
    }
    preload() {
        this.explosionMarkerPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'explosionmarker.png',
            classType: _GameObjects_ODExplosionMarker__WEBPACK_IMPORTED_MODULE_3__["default"],
            runChildUpdate: true
        });
        this.missileMarkerPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'missilemarker.png',
            classType: _GameObjects_ODMissileMaker__WEBPACK_IMPORTED_MODULE_0__["default"],
            createCallback: (missile) => { },
            removeCallback: (missile) => { },
            runChildUpdate: false
        });
        this.trashMarkerPool = this.add.group({
            maxSize: 20,
            defaultKey: 'ODImages',
            defaultFrame: 'trashmarker.png',
            classType: _GameObjects_ODTrashMarker__WEBPACK_IMPORTED_MODULE_2__["default"],
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.cloudPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'cloud1.png',
            classType: Phaser.GameObjects.Sprite,
            createCallback: (trash) => { },
            removeCallback: (trash) => { },
            runChildUpdate: false
        });
        this.cameras.main.fadeIn(this.fadeTime, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.world.fadeColor.red, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.world.fadeColor.green, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_5__["default"].instance.world.fadeColor.blue);
    }
    create() {
        this.cameras.main.setViewport(this.xPos, this.yPos, this.displayWidth, this.displayHeight);
        this.add.sky((this.displayWidth / 2.0), (this.displayHeight / 2.0)).setDisplaySize(this.displayWidth, this.displayHeight);
        this.cloudPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, 92.31, 123.08, 'ODImages', 'cloud1.png')).setScale(0.15, 0.10),
            (new Phaser.GameObjects.Sprite(this, 49.23, 36.92, 'ODImages', 'cloud1.png')).setScale(0.10, 0.15),
            (new Phaser.GameObjects.Sprite(this, 24.62, 73.85, 'ODImages', 'cloud1.png')).setScale(0.05, 0.05),
            (new Phaser.GameObjects.Sprite(this, 123.08, 36.92, 'ODImages', 'cloud1.png')).setScale(0.10, 0.05),
            (new Phaser.GameObjects.Sprite(this, 61.54, 61.54, 'ODImages', 'cloud2.png')).setScale(0.15, 0.10),
            (new Phaser.GameObjects.Sprite(this, 123.08, 73.85, 'ODImages', 'cloud2.png')).setScale(0.25, 0.10),
            (new Phaser.GameObjects.Sprite(this, 43.08, 98.46, 'ODImages', 'cloud2.png')).setScale(0.05, 0.10),
            (new Phaser.GameObjects.Sprite(this, 123.08, 104.62, 'ODImages', 'cloud2.png')).setScale(0.05, 0.05)
        ], true);
        this.add.ocean((this.displayWidth / 2.0), this.displayHeight - 20).setDisplaySize(this.displayWidth - 20, 25).setDepth(1);
        this.monitor = this.add.tvmonitorempty(0, 0);
        this.monitor.setDisplaySize(this.displayWidth, this.displayHeight).setDepth(2).setInteractive().play('EmptyRadars', false, (this.isHighlighted) ? 1 : 0).on('pointerdown', (pointer) => {
            if (this.radarOn) {
                this.registry.set('ActiveRadar', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key));
                this.registry.set('SetRadarBorder', new _Models_MonitorClickEvent__WEBPACK_IMPORTED_MODULE_4__["default"](this.key));
            }
        });
        this.statictv = this.add.statictv(0, 0).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(3).setActive(false).setVisible(false);
        this.oceanoutterbar = this.add.oceanouterbar((this.displayWidth / 2.0) - 35, this.displayHeight - 18).setDisplaySize(70.84, 10.08).setDepth(1);
        (this.add).oceanmiddlebar(this.oceanoutterbar.x, this.oceanoutterbar.y).setDisplaySize(this.oceanoutterbar.displayWidth, this.oceanoutterbar.displayHeight).setDepth(1);
        this.oceaninnerbar = this.add.oceaninnerbar(this.oceanoutterbar.x + 2, this.oceanoutterbar.y + 2).setDisplaySize(this.oceanoutterbar.displayWidth - 4, this.oceanoutterbar.displayHeight - 4).setDepth(1);
        this.healthRatio = this.oceanHealth / this.oceanHealthMax;
        this.registry.events.on('changedata', (parent, key, data) => {
            if (this.radarOn) {
                switch (key) {
                    case this.key:
                        this.createMissile(key, data);
                        break;
                    case 'SetRadarBorder':
                        this.monitor.play('EmptyRadars', false, (data.key == this.key) ? 1 : 0);
                        break;
                    case 'TrashDestroyed':
                        if (this.currLevel > 0 && data.key == this.key) {
                            this.trashToClear -= 1;
                            if (this.trashToClear == 0) {
                                this.SetRadarOn(false);
                                this.registry.set('GameEventRadarWon', new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_8__["default"](this.key));
                            }
                        }
                        break;
                }
            }
        });
    }
    StartTimer() {
        this.time.addEvent({
            delay: this.trashTimer,
            loop: true,
            callback: () => {
                if (this.radarOn) {
                    this.createTrash();
                }
            }
        });
    }
    SetRadarOn(isOn) {
        this.radarOn = isOn;
        if (this.radarOn) {
            this.statictv.setActive(false).setVisible(false);
        }
        else {
            this.statictv.setActive(true).setVisible(true).play('StaticTv', false, Phaser.Math.RND.integerInRange(0, 6));
        }
    }
    GetRadarOn() {
        return this.radarOn;
    }
    GetTrashMarkerPool() {
        return this.trashMarkerPool;
    }
    createTrash() {
        let marker = this.trashMarkerPool.get(0, 0).setScale(0.35);
        marker.SetId();
        marker.tag = this.key;
        marker.speed = Phaser.Math.RND.realInRange(this.trashMinSpeed, this.trashMaxSpeed);
        marker.setXY(Phaser.Math.RND.realInRange(15, this.displayWidth - 15), 10, Phaser.Math.RND.realInRange(15, this.displayWidth - 15), this.displayHeight - 30);
        marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
    }
    RunCoolDownTimer() {
        this.registry.set('StartCoolDown', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key));
        this.coolDownTimer = this.time.addEvent({
            delay: this.coolDownTime,
            callback: () => {
                this.registry.set('EndCoolDown', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key));
                this.canFire = true;
                this.coolDownTimer.destroy();
            }
        });
    }
    createMissile(radarName, event) {
        if (this.canFire || this.currLevel == 0) {
            let marker = this.missileMarkerPool.get(0, 0).setScale(0.35);
            marker.SetId();
            marker.tag = this.key;
            marker.speed = this.missileSpeed;
            marker.setXY(event.initialX * (this.displayWidth / event.monitorWidth), event.initialY * (this.displayHeight / event.monitorHeight), event.finalX * (this.displayWidth / event.monitorWidth), event.finalY * (this.displayHeight / event.monitorHeight));
            marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
            if (this.currLevel > 0) {
                this.canFire = false;
                this.RunCoolDownTimer();
            }
        }
    }
    CreateExplosion(x, y) {
        let marker = this.explosionMarkerPool.get(x, y).setScale(0.25);
        marker.SetId();
        marker.tag = this.key;
        marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].VisibleActive);
        let event = new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key, this.trashToClear, this.healthRatio);
        event.missiles.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__["default"](marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));
        this.registry.set('SetMonitorExplosion', event);
        this.time.addEvent({
            delay: 1900,
            loop: false,
            repeat: 0,
            callback: () => {
                marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
            }
        });
    }
    update(time, dt) {
        if (this.radarOn) {
            let radarUpdateInfo = new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key, this.trashToClear, this.healthRatio);
            Phaser.Actions.Call(this.trashMarkerPool.getChildren().filter((x) => x.active == true), (marker) => {
                marker.x += (marker.direction.x * marker.speed * (dt / 1000));
                marker.y += (marker.direction.y * marker.speed * (dt / 1000));
                if (marker.y >= marker.finalY) {
                    this.ProcessTrashAtWater(marker);
                }
                else {
                    radarUpdateInfo.trash.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__["default"](marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));
                }
            }, this);
            Phaser.Actions.Call(this.missileMarkerPool.getChildren().filter((x) => x.active == true), (marker) => {
                marker.x += (marker.direction.x * marker.speed * (dt / 1000));
                marker.y += (marker.direction.y * marker.speed * (dt / 1000));
                if (marker.y <= marker.finalY) {
                    this.ProcessMissileInFinalPosition(marker);
                }
                else {
                    radarUpdateInfo.missiles.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__["default"](marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));
                }
            }, this);
            let counter = 0;
            Phaser.Actions.Call(this.cloudPool.getChildren(), (cloud) => {
                cloud.x += (((dt / 1000) * 4.92) + (0.04 * counter));
                if (cloud.x - (cloud.displayWidth / 2.0) > this.displayWidth) {
                    cloud.x = -cloud.displayWidth / 2.0;
                }
                counter += 1;
            }, this);
            this.registry.set('RadarUpdateCycle' + this.key[this.key.length - 1], radarUpdateInfo);
        }
    }
    ProcessTrashAtWater(marker) {
        let event = new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key, this.trashToClear, this.healthRatio);
        event.trash.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__["default"](marker.id));
        this.registry.set('TrashAtWater', event);
        marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
        if (this.currLevel > 0) {
            this.oceanHealth -= 1;
            this.healthRatio = this.oceanHealth / this.oceanHealthMax;
            this.oceaninnerbar.displayWidth = (this.oceanoutterbar.displayWidth - 4) * this.healthRatio;
            if (this.oceanHealth == 2) {
                this.flashTimer = this.time.addEvent({
                    delay: 1000,
                    repeat: -1,
                    callback: () => {
                        this.cameras.main.flash(200, 255, 0, 0, false);
                    }
                });
            }
            else if (this.oceanHealth == 0) {
                if (this.flashTimer) {
                    this.flashTimer.destroy();
                    this.flashTimer = undefined;
                }
                this.SetRadarOn(false);
                this.registry.set('GameEventRadarLost', new _Models_GameEvent__WEBPACK_IMPORTED_MODULE_8__["default"](this.key));
            }
        }
    }
    ProcessMissileInFinalPosition(marker) {
        let event = new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_6__["default"](this.key);
        event.missiles.push(new _Models_RadarDetailEvent__WEBPACK_IMPORTED_MODULE_7__["default"](marker.id));
        this.registry.set('MissileInFinalPosition', event);
        marker.setODState(_Enums_ODState__WEBPACK_IMPORTED_MODULE_1__["default"].NotVisibleActive);
        this.CreateExplosion(marker.x, marker.y);
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODRadar);


/***/ }),

/***/ "./Scripts/Scenes/ODTitleScene.ts":
/*!****************************************!*\
  !*** ./Scripts/Scenes/ODTitleScene.ts ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ODMonitor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ODMonitor */ "./Scripts/Scenes/ODMonitor.ts");
/* harmony import */ var _ODRadar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ODRadar */ "./Scripts/Scenes/ODRadar.ts");
/* harmony import */ var _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../Models/ODGameInfo */ "./Scripts/Models/ODGameInfo.ts");
/* harmony import */ var _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../Models/RadarEvent */ "./Scripts/Models/RadarEvent.ts");




class ODTitleScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'TitleScreen'
        });
    }
    init() {
        this.playButton = undefined;
        this.monitor = undefined;
        this.fadeTime = 1000;
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.currLevel = 0;
    }
    create() {
        for (let i = 0; i < 3; i++) {
            let key = 'Radar' + (i + 1).toString();
            let currLevel = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.currLevel;
            let radar = new _ODRadar__WEBPACK_IMPORTED_MODULE_1__["default"](key, 0 + (160 * i), 500, 160, 212.8, (i == 0) ? true : false, this.fadeTime);
            this.scene.add(key, radar, true);
            radar.SetRadarOn(_Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.levels[0].radars[i].active);
            radar.StartTimer();
        }
        this.monitor = new _ODMonitor__WEBPACK_IMPORTED_MODULE_0__["default"]('Monitor1', 20, 80, 260, 345.8, this.fadeTime);
        this.scene.add('Monitor1', this.monitor, true);
        this.monitor.SetIgnoreInputOn(true);
        this.playButton = this.add.playbutton(78, 787);
        let levelButton = this.add.sprite(this.game.config.width / 2.0, 787, 'ODImages', 'levelsbutton.png');
        let settingsButton = this.add.sprite(400, 787, 'ODImages', 'settingsbutton.png');
        let monitorSwitches = this.add.sprite(100, 440, 'ODImages', 'monitorswitches.png');
        let monitorLights = this.add.sprite(220, 440, 'ODImages', 'mintorlights.png').setAngle(90);
        let monitorButtons = this.add.sprite(150, 470, 'ODImages', 'monitorbuttons.png').setScale(0.75);
        let monitorMeters = this.add.sprite(350, 190, 'ODImages', 'monitormeters.png');
        let monitorPlainDial = this.add.sprite(430, 190, 'ODImages', 'monitorplaindial.png');
        let monitorBigButton = this.add.sprite(380, 320, 'ODImages', 'monitorbigbutton.png');
        levelButton.setInteractive();
        this.playButton.setInteractive();
        levelButton.on('pointerdown', (pointer) => {
            this.levelButtonPressed();
        });
        this.playButton.on('pointerdown', (pointer) => {
            this.playButtonPressed();
        });
        this.add.text(8, 15, 'Ocean Defender II', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 34,
            color: '#16559d'
        });
        this.add.text(285, 400, 'RGBZ STUDIOS', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 18,
            color: '#bf662f'
        });
        this.registry.set('ActiveRadar', new _Models_RadarEvent__WEBPACK_IMPORTED_MODULE_3__["default"]('Radar1'));
        this.tweens.timeline({
            targets: [this.playButton],
            loop: -1,
            loopDelay: 2000,
            tweens: [
                {
                    angle: -25,
                    ease: 'Sine.easeInOut',
                    duration: 200
                },
                {
                    offset: 200,
                    angle: 25,
                    ease: 'Sine.easeInOut',
                    duration: 400
                },
                {
                    offset: 600,
                    angle: 0,
                    ease: 'Sine.easeInOut',
                    duration: 200
                }
            ]
        });
        this.time.addEvent({
            delay: 1000,
            loop: true,
            callback: () => {
                this.monitor.DemoCreateMissile();
            }
        });
        this.cameras.main.fadeIn(this.fadeTime, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.fadeColor.red, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.fadeColor.green, _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.fadeColor.blue);
    }
    update(time, dt) {
    }
    levelButtonPressed() {
        this.transitionToScene('LevelSelection');
    }
    playButtonPressed() {
        console.log('Furthest level unlocked: ' + _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].GetFurthestLevelUnlocked().toString());
        _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].instance.world.currLevel = _Models_ODGameInfo__WEBPACK_IMPORTED_MODULE_2__["default"].GetFurthestLevelUnlocked();
        this.transitionToScene('GameScreen');
    }
    transitionToScene(key) {
        this.scene.remove('Radar1');
        this.scene.remove('Radar2');
        this.scene.remove('Radar3');
        this.scene.remove('Monitor1');
        this.registry.events.off('changedata', undefined, undefined, false);
        this.scene.start(key, { resetScore: true });
    }
}
/* harmony default export */ __webpack_exports__["default"] = (ODTitleScene);


/***/ }),

/***/ "localforage":
/*!******************************!*\
  !*** external "localforage" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = localforage;

/***/ }),

/***/ "phaser":
/*!*************************!*\
  !*** external "Phaser" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = Phaser;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9FbnVtcy9PREJhc2VFbnVtLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvRW51bXMvT0RTdGF0ZS50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09EQmFzZS50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09EQmx1ZUd1bi50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09EQmx1ZUd1bkJhc2UudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9HYW1lT2JqZWN0cy9PREV4cGxvc2lvbk1hcmtlci50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09ER3JleUd1bi50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09ER3JleUd1bkJhc2UudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9HYW1lT2JqZWN0cy9PRE1pc3NpbGUudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9HYW1lT2JqZWN0cy9PRE1pc3NpbGVNYWtlci50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09ET2NlYW4udHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9HYW1lT2JqZWN0cy9PRFJlZEV4cGxvc2lvbi50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09EVHJhc2gudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9HYW1lT2JqZWN0cy9PRFRyYXNoTWFya2VyLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvR2FtZU9iamVjdHMvT0RXaGl0ZUV4cGxvc2lvbi50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL0dhbWVPYmplY3RzL09EWWVsbG93R3VuLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvR2FtZU9iamVjdHMvT0RZZWxsb3dHdW5CYXNlLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTWFuYWdlcnMvT0RDYWNoZU1hbmFnZXIudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9Nb2RlbHMvR2FtZUV2ZW50LnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTW9kZWxzL0dhbWVJbmZvTW9kZWxzL09ETGV2ZWxJbmZvLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTW9kZWxzL0dhbWVJbmZvTW9kZWxzL09EUmFkYXJJbmZvLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTW9kZWxzL0dhbWVJbmZvTW9kZWxzL09EV29ybGRJbmZvLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTW9kZWxzL01vbml0b3JDbGlja0V2ZW50LnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvTW9kZWxzL09ER2FtZUluZm8udHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9Nb2RlbHMvUmFkYXJEZXRhaWxFdmVudC50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL01vZGVscy9SYWRhckV2ZW50LnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvT0RNYWluLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvU2NlbmVzL09EQm9vdC50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL1NjZW5lcy9PREdhbWVTY2VuZS50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL1NjZW5lcy9PRExldmVsU2VsZWN0aW9uLnRzIiwid2VicGFjazovLy8uL1NjcmlwdHMvU2NlbmVzL09ETW9uaXRvci50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL1NjZW5lcy9PRFBhdXNlU2NlbmUudHMiLCJ3ZWJwYWNrOi8vLy4vU2NyaXB0cy9TY2VuZXMvT0RSYWRhci50cyIsIndlYnBhY2s6Ly8vLi9TY3JpcHRzL1NjZW5lcy9PRFRpdGxlU2NlbmUudHMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibG9jYWxmb3JhZ2VcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJQaGFzZXJcIiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsZ0NBQWdDO0FBQ2xCLHlFQUFVLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNOMUI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQywwQkFBMEI7QUFDWixzRUFBTyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDVHZCO0FBQUE7QUFBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixzREFBTztBQUN4QjtBQUNBO0FBQ0EsaUJBQWlCLHNEQUFPO0FBQ3hCO0FBQ0E7QUFDQSxpQkFBaUIsc0RBQU87QUFDeEI7QUFDQTtBQUNBLGlCQUFpQixzREFBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsc0RBQU87QUFDeEI7QUFDQTtBQUNBLGlCQUFpQixzREFBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSxxRUFBTSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDdEN0QjtBQUFBO0FBQThCO0FBQzlCLHdCQUF3QiwrQ0FBTTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNlLHdFQUFTLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNOekI7QUFBQTtBQUE4QjtBQUM5Qiw0QkFBNEIsK0NBQU07QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDZSw0RUFBYSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDTjdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBOEI7QUFDVztBQUNPO0FBQ1k7QUFDNUQsZ0NBQWdDLCtDQUFNO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQyxzREFBTztBQUM3QyxvQ0FBb0MsMERBQVU7QUFDOUMseUNBQXlDLGdFQUFnQjtBQUN6RDtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNlLGdGQUFpQixFQUFDOzs7Ozs7Ozs7Ozs7O0FDMUJqQztBQUFBO0FBQThCO0FBQzlCLHdCQUF3QiwrQ0FBTTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNlLHdFQUFTLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNOekI7QUFBQTtBQUE4QjtBQUM5Qiw0QkFBNEIsK0NBQU07QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDZSw0RUFBYSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDTjdCO0FBQUE7QUFBOEI7QUFDOUIsd0JBQXdCLCtDQUFNO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ2Usd0VBQVMsRUFBQzs7Ozs7Ozs7Ozs7OztBQ056QjtBQUFBO0FBQThCO0FBQzlCLDZCQUE2QiwrQ0FBTTtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsNkVBQWMsRUFBQzs7Ozs7Ozs7Ozs7OztBQ1Y5QjtBQUFBO0FBQThCO0FBQzlCLHNCQUFzQiwrQ0FBTTtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNlLHNFQUFPLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNOdkI7QUFBQTtBQUE4QjtBQUM5Qiw2QkFBNkIsK0NBQU07QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSw2RUFBYyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDVDlCO0FBQUE7QUFBOEI7QUFDOUIsc0JBQXNCLCtDQUFNO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSxzRUFBTyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDUHZCO0FBQUE7QUFBOEI7QUFDOUIsNEJBQTRCLCtDQUFNO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSw0RUFBYSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDVjdCO0FBQUE7QUFBOEI7QUFDOUIsK0JBQStCLCtDQUFNO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsK0VBQWdCLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNOaEM7QUFBQTtBQUE4QjtBQUM5QiwwQkFBMEIsK0NBQU07QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDZSwwRUFBVyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDTjNCO0FBQUE7QUFBOEI7QUFDOUIsOEJBQThCLCtDQUFNO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsOEVBQWUsRUFBQzs7Ozs7Ozs7Ozs7OztBQ04vQjtBQUFBO0FBQUE7QUFBc0M7QUFDdEM7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxRQUFRLGtEQUFXO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxlQUFlLGtEQUFXO0FBQzFCO0FBQ0E7QUFDQSxlQUFlLGtEQUFXO0FBQzFCO0FBQ0E7QUFDQSxlQUFlLGtEQUFXO0FBQzFCO0FBQ0E7QUFDQSxlQUFlLGtEQUFXO0FBQzFCO0FBQ0E7QUFDZSw2RUFBYyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDeEI5QjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSx3RUFBUyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDUnpCO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLDBFQUFXLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNSM0I7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLDBFQUFXLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNWM0I7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSwwRUFBVyxFQUFDOzs7Ozs7Ozs7Ozs7O0FDVDNCO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLGdGQUFpQixFQUFDOzs7Ozs7Ozs7Ozs7O0FDWGpDO0FBQUE7QUFBdUQ7QUFDdkQ7QUFDQTtBQUNBLHlCQUF5QixtRUFBVztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLHlFQUFVLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNqQjFCO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsK0VBQWdCLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNUaEM7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDZSx5RUFBVSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDVDFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBZ0I7QUFDaUM7QUFDWjtBQUNvQjtBQUNWO0FBQ1E7QUFDdkQ7QUFDQTtBQUNBLElBQUksZ0VBQWM7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxnQkFBZ0Isc0RBQU0sRUFBRSw0REFBWSxFQUFFLDJEQUFXLEVBQUUsZ0VBQWdCO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQzFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFtRDtBQUNVO0FBQ2Q7QUFDZ0I7QUFDSjtBQUNBO0FBQ0o7QUFDSjtBQUNBO0FBQ0g7QUFDaUI7QUFDQTtBQUNQO0FBQ1Y7QUFDaUI7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsbUVBQWM7QUFDakQ7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EscUNBQXFDLHNFQUFnQjtBQUNyRDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw4QkFBOEIsOERBQVM7QUFDdkM7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLDREQUFPO0FBQ25DO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLG9FQUFlO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrRUFBYTtBQUN6QztBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw0QkFBNEIsa0VBQWE7QUFDekM7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsMkJBQTJCLGdFQUFXO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDJCQUEyQiw4REFBUztBQUNwQztBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSwyQkFBMkIsOERBQVM7QUFDcEM7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EseUJBQXlCLGtFQUFhO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyxpQkFBaUI7QUFDdEQscUNBQXFDLGlCQUFpQjtBQUN0RCxxQ0FBcUMsaUJBQWlCO0FBQ3RELDJDQUEyQztBQUMzQyxtREFBbUQ7QUFDbkQ7QUFDQTtBQUNBLDhDQUE4QztBQUM5Qyw2Q0FBNkM7QUFDN0MsMkNBQTJDO0FBQzNDO0FBQ0EsbURBQW1ELDJEQUFVO0FBQzdELG1EQUFtRCwyREFBVTtBQUM3RCxtREFBbUQsMkRBQVU7QUFDN0QsaURBQWlEO0FBQ2pELGtEQUFrRDtBQUNsRCx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrREFBK0Qsc0VBQXNFO0FBQ3JJO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsK0RBQStELHdFQUF3RTtBQUN2STtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLCtEQUErRCw0RUFBNEU7QUFDM0k7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSwrREFBK0QseUVBQXlFO0FBQ3hJO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsMkNBQTJDO0FBQzVELGlCQUFpQix5Q0FBeUM7QUFDMUQsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiwwQ0FBMEM7QUFDM0QsaUJBQWlCLHdDQUF3QztBQUN6RCxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLHFEQUFxRDtBQUN0RSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxpRUFBYztBQUN0QjtBQUNBLGdCQUFnQiwwREFBVTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFFBQVEsMERBQVU7QUFDbEIsUUFBUSwwREFBVTtBQUNsQixRQUFRLDBEQUFVO0FBQ2xCLFFBQVEsMERBQVU7QUFDbEIseUJBQXlCLDJFQUFXO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwyRUFBVztBQUMxQywrQkFBK0IsMkVBQVc7QUFDMUMsK0JBQStCLDJFQUFXO0FBQzFDLFFBQVEsMERBQVU7QUFDbEIseUJBQXlCLDJFQUFXO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwyRUFBVztBQUMxQywrQkFBK0IsMkVBQVc7QUFDMUMsK0JBQStCLDJFQUFXO0FBQzFDLFFBQVEsMERBQVU7QUFDbEIseUJBQXlCLDJFQUFXO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwyRUFBVztBQUMxQywrQkFBK0IsMkVBQVc7QUFDMUMsK0JBQStCLDJFQUFXO0FBQzFDLFFBQVEsMERBQVU7QUFDbEIseUJBQXlCLDJFQUFXO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwyRUFBVztBQUMxQywrQkFBK0IsMkVBQVc7QUFDMUMsK0JBQStCLDJFQUFXO0FBQzFDLFFBQVEsMERBQVU7QUFDbEIseUJBQXlCLDJFQUFXO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiwyRUFBVztBQUMxQywrQkFBK0IsMkVBQVc7QUFDMUMsK0JBQStCLDJFQUFXO0FBQzFDLFFBQVEsMERBQVU7QUFDbEI7QUFDQTtBQUNBO0FBQ2UscUVBQU0sRUFBQzs7Ozs7Ozs7Ozs7OztBQ3hXdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0M7QUFDWTtBQUNoQjtBQUNnQjtBQUNjO0FBQ3BCO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLDBEQUFVO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLE9BQU87QUFDOUI7QUFDQSw0QkFBNEIsMERBQVU7QUFDdEMsNEJBQTRCLGdEQUFPO0FBQ25DO0FBQ0EsNkJBQTZCLDBEQUFVO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixrREFBUztBQUNwQztBQUNBLDhCQUE4QixxREFBWTtBQUMxQztBQUNBLDZDQUE2QywwREFBVTtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsZ0RBQWdELDBEQUFVLCtCQUErQiwwREFBVSxpQ0FBaUMsMERBQVU7QUFDOUk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRCxvQkFBb0IsRUFBRTtBQUN0RTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUFpRCwwREFBVTtBQUMzRCxvREFBb0QsaUVBQWlCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSwwREFBVSxpQkFBaUIsMERBQVU7QUFDN0MsUUFBUSwwREFBVTtBQUNsQixRQUFRLDBEQUFVLGlCQUFpQiwwREFBVTtBQUM3Qyw0QkFBNEIsb0JBQW9CO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixtQkFBbUI7QUFDbEQ7QUFDQTtBQUNlLDBFQUFXLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUNsSDNCO0FBQUE7QUFBZ0Q7QUFDaEQ7QUFDQTtBQUNBLGVBQWUsd0JBQXdCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLHVCQUF1QixLQUFLLDBEQUFVLHdCQUF3QjtBQUM5RCw0QkFBNEIsMERBQVU7QUFDdEM7QUFDQTtBQUNBLDZJQUE2SSwyQkFBMkIsRUFBRTtBQUMxSztBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsNElBQTRJLDJCQUEyQixFQUFFO0FBQ3pLO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnREFBZ0QsMERBQVUsK0JBQStCLDBEQUFVLGlDQUFpQywwREFBVTtBQUM5STtBQUNBO0FBQ0EsUUFBUSwwREFBVTtBQUNsQix3Q0FBd0MsbUJBQW1CO0FBQzNEO0FBQ0E7QUFDZSwrRUFBZ0IsRUFBQzs7Ozs7Ozs7Ozs7OztBQ3JEaEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBK0M7QUFDTjtBQUNVO0FBQ0o7QUFDZTtBQUNIO0FBQ1g7QUFDaUI7QUFDbkI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLDREQUFPO0FBQzlCLHdDQUF3QyxFQUFFO0FBQzFDLHdDQUF3QyxFQUFFO0FBQzFDO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLDREQUFPO0FBQzlCO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isd0NBQXdDLEVBQUU7QUFDMUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsOERBQVM7QUFDaEMsd0NBQXdDLEVBQUU7QUFDMUMsd0NBQXdDLEVBQUU7QUFDMUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsbUVBQWM7QUFDckMsd0NBQXdDLEVBQUU7QUFDMUMsd0NBQXdDLEVBQUU7QUFDMUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIscUVBQWdCO0FBQ3ZDLHdDQUF3QyxFQUFFO0FBQzFDLHdDQUF3QyxFQUFFO0FBQzFDO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsRUFBRTtBQUMxQyx3Q0FBd0MsRUFBRTtBQUMxQztBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLEVBQUU7QUFDMUMsd0NBQXdDLEVBQUU7QUFDMUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLDBEQUFVO0FBQzFCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2R0FBNkcsMERBQVU7QUFDdkg7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULFlBQVksMERBQVU7QUFDdEI7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsZ0RBQWdELDBEQUFVLCtCQUErQiwwREFBVSxpQ0FBaUMsMERBQVU7QUFDOUk7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLDhJQUE4STtBQUN6SztBQUNBO0FBQ0E7QUFDQSxxQkFBcUIseURBQVM7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixpRUFBaUI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLHlEQUFVO0FBQy9CO0FBQ0EscUJBQXFCLHlEQUFVO0FBQy9CO0FBQ0E7QUFDQSxxQkFBcUIseURBQVU7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFFBQVE7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IscUJBQXFCO0FBQzdDLHdCQUF3QixxQkFBcUI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLHNEQUFPO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEMsc0RBQU87QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsZ0JBQWdCLDBEQUFVO0FBQzFCO0FBQ0EseUVBQXlFLGtCQUFrQixzREFBTyxtQkFBbUIsRUFBRTtBQUN2SDtBQUNBO0FBQ0EsK0JBQStCLHVCQUF1QjtBQUN0RDtBQUNBLGdFQUFnRSxzREFBTztBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDLHNEQUFPO0FBQ3JEO0FBQ0E7QUFDQSw4Q0FBOEMsc0RBQU87QUFDckQscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxzREFBTztBQUN4QztBQUNBLHNDQUFzQyxzREFBTztBQUM3QyxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLHNEQUFPO0FBQ3BDLGFBQWE7QUFDYjtBQUNBLDZCQUE2QixzREFBTztBQUNwQyxhQUFhO0FBQ2I7QUFDQSw2QkFBNkIsc0RBQU87QUFDcEMsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0Msc0RBQU87QUFDL0M7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDLHNEQUFPO0FBQzdDO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSwwREFBVTtBQUN0QixZQUFZLDBEQUFVO0FBQ3RCLG1DQUFtQywwREFBVTtBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNlLHdFQUFTLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUMvWnpCO0FBQUE7QUFBOEM7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIseURBQVM7QUFDbEM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLHlCQUF5Qix5REFBUztBQUNsQztBQUNBLFNBQVM7QUFDVDtBQUNBLHlCQUF5Qix5REFBUztBQUNsQztBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ2UsMkVBQVksRUFBQzs7Ozs7Ozs7Ozs7OztBQ2pDNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBNkQ7QUFDcEI7QUFDa0I7QUFDUTtBQUNMO0FBQ2Q7QUFDQTtBQUNZO0FBQ2Q7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsMERBQVU7QUFDbkM7QUFDQSw0QkFBNEIsMERBQVU7QUFDdEMsMkJBQTJCLDBEQUFVO0FBQ3JDLDhCQUE4QiwwREFBVTtBQUN4QywwQkFBMEIsMERBQVU7QUFDcEMsNEJBQTRCLDBEQUFVO0FBQ3RDLDZCQUE2QiwwREFBVTtBQUN2Qyw2QkFBNkIsMERBQVU7QUFDdkMsNEJBQTRCLDBEQUFVO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixzRUFBaUI7QUFDeEM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsbUVBQWM7QUFDckMsMENBQTBDLEVBQUU7QUFDNUMsMENBQTBDLEVBQUU7QUFDNUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsa0VBQWE7QUFDcEMsd0NBQXdDLEVBQUU7QUFDMUMsd0NBQXdDLEVBQUU7QUFDMUM7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdDQUF3QyxFQUFFO0FBQzFDLHdDQUF3QyxFQUFFO0FBQzFDO0FBQ0EsU0FBUztBQUNULGdEQUFnRCwwREFBVSwrQkFBK0IsMERBQVUsaUNBQWlDLDBEQUFVO0FBQzlJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCwwREFBVTtBQUMvRCx3REFBd0QsaUVBQWlCO0FBQ3pFO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkVBQTJFLHlEQUFTO0FBQ3BGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsc0RBQU87QUFDakM7QUFDQTtBQUNBLCtDQUErQywwREFBVTtBQUN6RDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsMERBQVU7QUFDL0Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEIsc0RBQU87QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsc0RBQU87QUFDakMsd0JBQXdCLDBEQUFVO0FBQ2xDLGdDQUFnQyxnRUFBZ0I7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLHNEQUFPO0FBQ3pDO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQywwREFBVTtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EQUFtRCxnRUFBZ0I7QUFDbkU7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzREFBc0QsZ0VBQWdCO0FBQ3RFO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLDBEQUFVO0FBQ2xDLDZCQUE2QixnRUFBZ0I7QUFDN0M7QUFDQSwwQkFBMEIsc0RBQU87QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDREQUE0RCx5REFBUztBQUNyRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QiwwREFBVTtBQUNsQyxnQ0FBZ0MsZ0VBQWdCO0FBQ2hEO0FBQ0EsMEJBQTBCLHNEQUFPO0FBQ2pDO0FBQ0E7QUFDQTtBQUNlLHNFQUFPLEVBQUM7Ozs7Ozs7Ozs7Ozs7QUM3UXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBb0M7QUFDSjtBQUNnQjtBQUNBO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDBEQUFVO0FBQ2xCO0FBQ0E7QUFDQSx1QkFBdUIsT0FBTztBQUM5QjtBQUNBLDRCQUE0QiwwREFBVTtBQUN0Qyw0QkFBNEIsZ0RBQU87QUFDbkM7QUFDQSw2QkFBNkIsMERBQVU7QUFDdkM7QUFDQTtBQUNBLDJCQUEyQixrREFBUztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCw2Q0FBNkMsMERBQVU7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULGdEQUFnRCwwREFBVSwrQkFBK0IsMERBQVUsaUNBQWlDLDBEQUFVO0FBQzlJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELDBEQUFVO0FBQzVELFFBQVEsMERBQVUsNEJBQTRCLDBEQUFVO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsbUJBQW1CO0FBQ2xEO0FBQ0E7QUFDZSwyRUFBWSxFQUFDOzs7Ozs7Ozs7Ozs7QUM1RzVCLDZCOzs7Ozs7Ozs7OztBQ0FBLHdCIiwiZmlsZSI6Ik9EQnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9TY3JpcHRzL09ETWFpbi50c1wiKTtcbiIsInZhciBPREJhc2VFbnVtO1xyXG4oZnVuY3Rpb24gKE9EQmFzZUVudW0pIHtcclxuICAgIE9EQmFzZUVudW1bT0RCYXNlRW51bVtcIlllbGxvd1wiXSA9IDBdID0gXCJZZWxsb3dcIjtcclxuICAgIE9EQmFzZUVudW1bT0RCYXNlRW51bVtcIkJsdWVcIl0gPSAxXSA9IFwiQmx1ZVwiO1xyXG4gICAgT0RCYXNlRW51bVtPREJhc2VFbnVtW1wiR3JleVwiXSA9IDJdID0gXCJHcmV5XCI7XHJcbn0pKE9EQmFzZUVudW0gfHwgKE9EQmFzZUVudW0gPSB7fSkpO1xyXG5leHBvcnQgZGVmYXVsdCBPREJhc2VFbnVtO1xyXG4iLCJ2YXIgT0RTdGF0ZTtcclxuKGZ1bmN0aW9uIChPRFN0YXRlKSB7XHJcbiAgICBPRFN0YXRlW09EU3RhdGVbXCJWaXNpYmxlXCJdID0gMF0gPSBcIlZpc2libGVcIjtcclxuICAgIE9EU3RhdGVbT0RTdGF0ZVtcIkFjdGl2ZVwiXSA9IDFdID0gXCJBY3RpdmVcIjtcclxuICAgIE9EU3RhdGVbT0RTdGF0ZVtcIlZpc2libGVBY3RpdmVcIl0gPSAyXSA9IFwiVmlzaWJsZUFjdGl2ZVwiO1xyXG4gICAgT0RTdGF0ZVtPRFN0YXRlW1wiTm90VmlzaWJsZVwiXSA9IDNdID0gXCJOb3RWaXNpYmxlXCI7XHJcbiAgICBPRFN0YXRlW09EU3RhdGVbXCJOb3RBY3RpdmVcIl0gPSA0XSA9IFwiTm90QWN0aXZlXCI7XHJcbiAgICBPRFN0YXRlW09EU3RhdGVbXCJOb3RWaXNpYmxlQWN0aXZlXCJdID0gNV0gPSBcIk5vdFZpc2libGVBY3RpdmVcIjtcclxufSkoT0RTdGF0ZSB8fCAoT0RTdGF0ZSA9IHt9KSk7XHJcbmV4cG9ydCBkZWZhdWx0IE9EU3RhdGU7XHJcbiIsImltcG9ydCBPRFN0YXRlIGZyb20gJy4vLi4vRW51bXMvT0RTdGF0ZSc7XHJcbmNsYXNzIE9EQmFzZSBleHRlbmRzIFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUge1xyXG4gICAgY29uc3RydWN0b3Ioc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKSB7XHJcbiAgICAgICAgc3VwZXIoc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKTtcclxuICAgICAgICB0aGlzLmRpcmVjdGlvbiA9IG5ldyBQaGFzZXIuTWF0aC5WZWN0b3IyKCk7XHJcbiAgICB9XHJcbiAgICBzZXRYWSh4LCB5LCBmaW5hbFgsIGZpbmFsWSkge1xyXG4gICAgICAgIHRoaXMueCA9IHg7XHJcbiAgICAgICAgdGhpcy55ID0geTtcclxuICAgICAgICB0aGlzLmZpbmFsWCA9IGZpbmFsWDtcclxuICAgICAgICB0aGlzLmZpbmFsWSA9IGZpbmFsWTtcclxuICAgICAgICB0aGlzLmRpcmVjdGlvbi5zZXQodGhpcy5maW5hbFggLSB0aGlzLngsIHRoaXMuZmluYWxZIC0gdGhpcy55KS5ub3JtYWxpemUoKTtcclxuICAgIH1cclxuICAgIHNldE9EU3RhdGUoc3RhdGUpIHtcclxuICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgT0RTdGF0ZS5BY3RpdmU6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldEFjdGl2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIE9EU3RhdGUuTm90QWN0aXZlOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRBY3RpdmUoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgT0RTdGF0ZS5Ob3RWaXNpYmxlOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRWaXNpYmxlKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIE9EU3RhdGUuTm90VmlzaWJsZUFjdGl2ZTpcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0QWN0aXZlKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0VmlzaWJsZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBPRFN0YXRlLlZpc2libGU6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFZpc2libGUodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBPRFN0YXRlLlZpc2libGVBY3RpdmU6XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldEFjdGl2ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0VmlzaWJsZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPREJhc2U7XHJcbiIsImltcG9ydCBPREJhc2UgZnJvbSBcIi4vT0RCYXNlXCI7XHJcbmNsYXNzIE9EQmx1ZUd1biBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EQmx1ZUd1bjtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tIFwiLi9PREJhc2VcIjtcclxuY2xhc3MgT0RCbHVlR3VuQmFzZSBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EQmx1ZUd1bkJhc2U7XHJcbiIsImltcG9ydCBPREJhc2UgZnJvbSAnLi9PREJhc2UnO1xyXG5pbXBvcnQgT0RTdGF0ZSBmcm9tICcuLy4uL0VudW1zL09EU3RhdGUnO1xyXG5pbXBvcnQgUmFkYXJFdmVudCBmcm9tICcuLy4uL01vZGVscy9SYWRhckV2ZW50JztcclxuaW1wb3J0IFJhZGFyRGV0YWlsRXZlbnQgZnJvbSAnLi8uLi9Nb2RlbHMvUmFkYXJEZXRhaWxFdmVudCc7XHJcbmNsYXNzIE9ERXhwbG9zaW9uTWFya2VyIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICAgICAgdGhpcy5pZCA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgdGhpcy5yYWRhciA9IHNjZW5lO1xyXG4gICAgfVxyXG4gICAgU2V0SWQoKSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUodGltZSwgZHQpIHtcclxuICAgICAgICBpZiAodGhpcy5hY3RpdmUpIHtcclxuICAgICAgICAgICAgdGhpcy5yYWRhci5HZXRUcmFzaE1hcmtlclBvb2woKS5nZXRDaGlsZHJlbigpLmZpbHRlcigobW0pID0+IG1tLmFjdGl2ZSkuZm9yRWFjaCgobWFya2VyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoTWF0aC5hYnMoUGhhc2VyLk1hdGguRGlzdGFuY2UuQmV0d2Vlbih0aGlzLngsIHRoaXMueSwgbWFya2VyLngsIG1hcmtlci55KSkgPCAxMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmtlci5zZXRPRFN0YXRlKE9EU3RhdGUuTm90VmlzaWJsZUFjdGl2ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGV2ZW50ID0gbmV3IFJhZGFyRXZlbnQobWFya2VyLnRhZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQudHJhc2gucHVzaChuZXcgUmFkYXJEZXRhaWxFdmVudChtYXJrZXIuaWQpKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNjZW5lLnJlZ2lzdHJ5LnNldCgnVHJhc2hEZXN0cm95ZWQnLCBldmVudCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPREV4cGxvc2lvbk1hcmtlcjtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tIFwiLi9PREJhc2VcIjtcclxuY2xhc3MgT0RHcmV5R3VuIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RHcmV5R3VuO1xyXG4iLCJpbXBvcnQgT0RCYXNlIGZyb20gXCIuL09EQmFzZVwiO1xyXG5jbGFzcyBPREdyZXlHdW5CYXNlIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RHcmV5R3VuQmFzZTtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tICcuL09EQmFzZSc7XHJcbmNsYXNzIE9ETWlzc2lsZSBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9ETWlzc2lsZTtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tICcuL09EQmFzZSc7XHJcbmNsYXNzIE9ETWlzc2lsZU1ha2VyIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICAgICAgdGhpcy5pZCA9IChuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XHJcbiAgICB9XHJcbiAgICBTZXRJZCgpIHtcclxuICAgICAgICB0aGlzLmlkID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPRE1pc3NpbGVNYWtlcjtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tIFwiLi9PREJhc2VcIjtcclxuY2xhc3MgT0RPY2VhbiBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9ET2NlYW47XHJcbiIsImltcG9ydCBPREJhc2UgZnJvbSAnLi9PREJhc2UnO1xyXG5jbGFzcyBPRFJlZEV4cGxvc2lvbiBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgfVxyXG4gICAgdXBkYXRlKHRpbWUsIGR0KSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZHQpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EUmVkRXhwbG9zaW9uO1xyXG4iLCJpbXBvcnQgT0RCYXNlIGZyb20gJy4vT0RCYXNlJztcclxuY2xhc3MgT0RUcmFzaCBleHRlbmRzIE9EQmFzZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpIHtcclxuICAgICAgICBzdXBlcihzY2VuZSwgeCwgeSwgdGV4dHVyZSwgZnJhbWUpO1xyXG4gICAgICAgIHRoaXMuaWdub3JlID0gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RUcmFzaDtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tICcuL09EQmFzZSc7XHJcbmNsYXNzIE9EVHJhc2hNYXJrZXIgZXh0ZW5kcyBPREJhc2Uge1xyXG4gICAgY29uc3RydWN0b3Ioc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKSB7XHJcbiAgICAgICAgc3VwZXIoc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKTtcclxuICAgICAgICB0aGlzLmlkID0gKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcclxuICAgIH1cclxuICAgIFNldElkKCkge1xyXG4gICAgICAgIHRoaXMuaWQgPSAobmV3IERhdGUoKSkuZ2V0VGltZSgpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EVHJhc2hNYXJrZXI7XHJcbiIsImltcG9ydCBPREJhc2UgZnJvbSAnLi9PREJhc2UnO1xyXG5jbGFzcyBPRFdoaXRlRXhwbG9zaW9uIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RXaGl0ZUV4cGxvc2lvbjtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tIFwiLi9PREJhc2VcIjtcclxuY2xhc3MgT0RZZWxsb3dHdW4gZXh0ZW5kcyBPREJhc2Uge1xyXG4gICAgY29uc3RydWN0b3Ioc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKSB7XHJcbiAgICAgICAgc3VwZXIoc2NlbmUsIHgsIHksIHRleHR1cmUsIGZyYW1lKTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPRFllbGxvd0d1bjtcclxuIiwiaW1wb3J0IE9EQmFzZSBmcm9tIFwiLi9PREJhc2VcIjtcclxuY2xhc3MgT0RZZWxsb3dHdW5CYXNlIGV4dGVuZHMgT0RCYXNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSkge1xyXG4gICAgICAgIHN1cGVyKHNjZW5lLCB4LCB5LCB0ZXh0dXJlLCBmcmFtZSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RZZWxsb3dHdW5CYXNlO1xyXG4iLCJpbXBvcnQgbG9jYWxmb3JhZ2UgZnJvbSAnbG9jYWxmb3JhZ2UnO1xyXG5jbGFzcyBPRENhY2hlTWFuYWdlciB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG4gICAgc3RhdGljIGluaXRpYWxpemUoKSB7XHJcbiAgICAgICAgbG9jYWxmb3JhZ2UuY29uZmlnKHtcclxuICAgICAgICAgICAgbmFtZTogJ09ESUlEQicsXHJcbiAgICAgICAgICAgIHZlcnNpb246IDEuMCxcclxuICAgICAgICAgICAgc3RvcmVOYW1lOiAnT0RDYWNoZWREQicsXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU3RvcmVzIGFsbCBwZXJzaXNlbnQgZGF0YSBmb3IgT0RJSSdcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIHN0YXRpYyBnZXRJdGVtKGtleSkge1xyXG4gICAgICAgIHJldHVybiBsb2NhbGZvcmFnZS5nZXRJdGVtKGtleSk7XHJcbiAgICB9XHJcbiAgICBzdGF0aWMgc2V0SXRlbShrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgcmV0dXJuIGxvY2FsZm9yYWdlLnNldEl0ZW0oa2V5LCB2YWx1ZSk7XHJcbiAgICB9XHJcbiAgICBzdGF0aWMgcmVtb3ZlSXRlbShrZXkpIHtcclxuICAgICAgICByZXR1cm4gbG9jYWxmb3JhZ2UucmVtb3ZlSXRlbShrZXkpO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIGNsZWFyKCkge1xyXG4gICAgICAgIHJldHVybiBsb2NhbGZvcmFnZS5jbGVhcigpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EQ2FjaGVNYW5hZ2VyO1xyXG4iLCJjbGFzcyBHYW1lRXZlbnQge1xyXG4gICAgY29uc3RydWN0b3Ioa2V5LCBpc1BhdXNlZCA9IGZhbHNlLCByZXN0YXJ0TGV2ZWwgPSBmYWxzZSwgZ290b1RpdGxlU2NyZWVuID0gZmFsc2UpIHtcclxuICAgICAgICB0aGlzLmtleSA9IGtleTtcclxuICAgICAgICB0aGlzLmlzUGF1c2VkID0gaXNQYXVzZWQ7XHJcbiAgICAgICAgdGhpcy5yZXN0YXJ0TGV2ZWwgPSByZXN0YXJ0TGV2ZWw7XHJcbiAgICAgICAgdGhpcy5nb3RvVGl0bGVTY3JlZW4gPSBnb3RvVGl0bGVTY3JlZW47XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgR2FtZUV2ZW50O1xyXG4iLCJjbGFzcyBPRExldmVsSW5mbyB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLmNsZWFyZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnVubG9ja2VkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5yYWRhcnMgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICB0aGlzLmNvb2xEb3duVGltZXIgPSA0MDA7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RMZXZlbEluZm87XHJcbiIsImNsYXNzIE9EUmFkYXJJbmZvIHtcclxuICAgIGNvbnN0cnVjdG9yKGFjdGl2ZSwgdHJhc2hTcGF3blRpbWUsIHRyYXNoTWluU3BlZWQsIHRyYXNoTWF4U3BlZWQsIHRyYXNoVG9DbGVhciwgb2NlYW5NYXhEYW1hZ2UpIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZSA9IGFjdGl2ZTtcclxuICAgICAgICB0aGlzLnRyYXNoU3Bhd25UaW1lID0gdHJhc2hTcGF3blRpbWU7XHJcbiAgICAgICAgdGhpcy50cmFzaE1pblNwZWVkID0gdHJhc2hNaW5TcGVlZDtcclxuICAgICAgICB0aGlzLnRyYXNoTWF4U3BlZWQgPSB0cmFzaE1heFNwZWVkO1xyXG4gICAgICAgIHRoaXMudHJhc2hUb0NsZWFyID0gdHJhc2hUb0NsZWFyO1xyXG4gICAgICAgIHRoaXMub2NlYW5NYXhEYW1hZ2UgPSBvY2Vhbk1heERhbWFnZTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPRFJhZGFySW5mbztcclxuIiwiY2xhc3MgT0RXb3JsZEluZm8ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5mYWRlQ29sb3IgPSB7XHJcbiAgICAgICAgICAgIHJlZDogMjA0LFxyXG4gICAgICAgICAgICBncmVlbjogMjA0LFxyXG4gICAgICAgICAgICBibHVlOiAyMDRcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EV29ybGRJbmZvO1xyXG4iLCJjbGFzcyBNb25pdG9yQ2xpY2tFdmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcihrZXksIGluaXRpYWxYID0gMCwgaW5pdGlhbFkgPSAwLCBmaW5hbFggPSAwLCBmaW5hbFkgPSAwLCBtb25pdG9yV2lkdGggPSAwLCBtb25pdG9ySGVpZ2h0ID0gMCkge1xyXG4gICAgICAgIHRoaXMua2V5ID0ga2V5O1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbFggPSBpbml0aWFsWDtcclxuICAgICAgICB0aGlzLmluaXRpYWxZID0gaW5pdGlhbFk7XHJcbiAgICAgICAgdGhpcy5maW5hbFggPSBmaW5hbFg7XHJcbiAgICAgICAgdGhpcy5maW5hbFkgPSBmaW5hbFk7XHJcbiAgICAgICAgdGhpcy5tb25pdG9yV2lkdGggPSBtb25pdG9yV2lkdGg7XHJcbiAgICAgICAgdGhpcy5tb25pdG9ySGVpZ2h0ID0gbW9uaXRvckhlaWdodDtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBNb25pdG9yQ2xpY2tFdmVudDtcclxuIiwiaW1wb3J0IE9EV29ybGRJbmZvIGZyb20gJy4vR2FtZUluZm9Nb2RlbHMvT0RXb3JsZEluZm8nO1xyXG5jbGFzcyBPREdhbWVJbmZvIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMud29ybGQgPSBuZXcgT0RXb3JsZEluZm8oKTtcclxuICAgICAgICB0aGlzLmxldmVscyA9IG5ldyBBcnJheSgpO1xyXG4gICAgfVxyXG4gICAgc3RhdGljIEdldEZ1cnRoZXN0TGV2ZWxVbmxvY2tlZCgpIHtcclxuICAgICAgICBsZXQgZm91bmRMZXZlbCA9IDE7XHJcbiAgICAgICAgZm9yIChsZXQgbGV2ZWwgb2YgdGhpcy5pbnN0YW5jZS5sZXZlbHMpIHtcclxuICAgICAgICAgICAgaWYgKGxldmVsLnVubG9ja2VkKSB7XHJcbiAgICAgICAgICAgICAgICBmb3VuZExldmVsID0gbGV2ZWwubGV2ZWxOdW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZvdW5kTGV2ZWw7XHJcbiAgICB9XHJcbn1cclxuT0RHYW1lSW5mby5pbnN0YW5jZSA9IG5ldyBPREdhbWVJbmZvKCk7XHJcbmV4cG9ydCBkZWZhdWx0IE9ER2FtZUluZm87XHJcbiIsImNsYXNzIFJhZGFyRGV0YWlsRXZlbnQge1xyXG4gICAgY29uc3RydWN0b3IoaWQsIHggPSAwLCB5ID0gMCwgZGlzcGxheVdpZHRoID0gMCwgZGlzcGxheUhlaWdodCA9IDApIHtcclxuICAgICAgICB0aGlzLmlkID0gaWQ7XHJcbiAgICAgICAgdGhpcy54ID0geDtcclxuICAgICAgICB0aGlzLnkgPSB5O1xyXG4gICAgICAgIHRoaXMuZGlzcGxheVdpZHRoID0gZGlzcGxheVdpZHRoO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUhlaWdodCA9IGRpc3BsYXlIZWlnaHQ7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgUmFkYXJEZXRhaWxFdmVudDtcclxuIiwiY2xhc3MgUmFkYXJFdmVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcihrZXksIHRyYXNoVG9DbGVhciA9IDAsIG9jZWFuSGVhbHRoID0gMCkge1xyXG4gICAgICAgIHRoaXMua2V5ID0ga2V5O1xyXG4gICAgICAgIHRoaXMudHJhc2hUb0NsZWFyID0gdHJhc2hUb0NsZWFyO1xyXG4gICAgICAgIHRoaXMub2NlYW5IZWFsdGggPSBvY2VhbkhlYWx0aDtcclxuICAgICAgICB0aGlzLnRyYXNoID0gbmV3IEFycmF5KCk7XHJcbiAgICAgICAgdGhpcy5taXNzaWxlcyA9IG5ldyBBcnJheSgpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IFJhZGFyRXZlbnQ7XHJcbiIsImltcG9ydCAncGhhc2VyJztcclxuaW1wb3J0IE9EVGl0bGVTY2VuZSBmcm9tICcuL1NjZW5lcy9PRFRpdGxlU2NlbmUnO1xyXG5pbXBvcnQgT0RCb290IGZyb20gJy4vU2NlbmVzL09EQm9vdCc7XHJcbmltcG9ydCBPRExldmVsU2VsZWN0aW9uIGZyb20gJy4vU2NlbmVzL09ETGV2ZWxTZWxlY3Rpb24nO1xyXG5pbXBvcnQgT0RHYW1lU2NlbmUgZnJvbSAnLi9TY2VuZXMvT0RHYW1lU2NlbmUnO1xyXG5pbXBvcnQgT0RDYWNoZU1hbmFnZXIgZnJvbSAnLi9NYW5hZ2Vycy9PRENhY2hlTWFuYWdlcic7XHJcbnZhciBnYW1lO1xyXG53aW5kb3cub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgT0RDYWNoZU1hbmFnZXIuaW5pdGlhbGl6ZSgpO1xyXG4gICAgbGV0IGdhbWVDb25maWcgPSB7XHJcbiAgICAgICAgdHlwZTogUGhhc2VyLkFVVE8sXHJcbiAgICAgICAgdGl0bGU6ICdPY2VhbiBEZWZlbmRlciBJSScsXHJcbiAgICAgICAgd2lkdGg6IDQ4MCxcclxuICAgICAgICBoZWlnaHQ6IDg1NCxcclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IDB4Y2NjY2NjLFxyXG4gICAgICAgIGF1dG9Gb2N1czogdHJ1ZSxcclxuICAgICAgICB2ZXJzaW9uOiAnMS4wLjAnLFxyXG4gICAgICAgIGZwczoge1xyXG4gICAgICAgICAgICB0YXJnZXQ6IDYwLFxyXG4gICAgICAgICAgICBtaW46IDMwXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzY2VuZTogW09EQm9vdCwgT0RUaXRsZVNjZW5lLCBPREdhbWVTY2VuZSwgT0RMZXZlbFNlbGVjdGlvbl1cclxuICAgIH07XHJcbiAgICBnYW1lID0gbmV3IFBoYXNlci5HYW1lKGdhbWVDb25maWcpO1xyXG4gICAgcmVzaXplKCk7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCByZXNpemUsIGZhbHNlKTtcclxufTtcclxuZnVuY3Rpb24gcmVzaXplKCkge1xyXG4gICAgbGV0IGNhbnZhcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJjYW52YXNcIik7XHJcbiAgICBsZXQgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcclxuICAgIGxldCB3aW5kb3dIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XHJcbiAgICBsZXQgd2luZG93UmF0aW8gPSB3aW5kb3dXaWR0aCAvIHdpbmRvd0hlaWdodDtcclxuICAgIGxldCBnYW1lUmF0aW8gPSBnYW1lLmNvbmZpZy53aWR0aCAvIGdhbWUuY29uZmlnLmhlaWdodDtcclxuICAgIGlmICh3aW5kb3dSYXRpbyA8IGdhbWVSYXRpbykge1xyXG4gICAgICAgIGNhbnZhcy5zdHlsZS53aWR0aCA9IHdpbmRvd1dpZHRoICsgXCJweFwiO1xyXG4gICAgICAgIGNhbnZhcy5zdHlsZS5oZWlnaHQgPSAod2luZG93V2lkdGggLyBnYW1lUmF0aW8pICsgXCJweFwiO1xyXG4gICAgfVxyXG4gICAgZWxzZSB7XHJcbiAgICAgICAgY2FudmFzLnN0eWxlLndpZHRoID0gKHdpbmRvd0hlaWdodCAqIGdhbWVSYXRpbykgKyBcInB4XCI7XHJcbiAgICAgICAgY2FudmFzLnN0eWxlLmhlaWdodCA9IHdpbmRvd0hlaWdodCArIFwicHhcIjtcclxuICAgIH1cclxufVxyXG47XHJcbiIsImltcG9ydCBPRE1pc3NpbGUgZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PRE1pc3NpbGUnO1xyXG5pbXBvcnQgT0RSZWRFeHBsb3Npb24gZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PRFJlZEV4cGxvc2lvbic7XHJcbmltcG9ydCBPRE9jZWFuIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RPY2Vhbic7XHJcbmltcG9ydCBPRFllbGxvd0d1bkJhc2UgZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PRFllbGxvd0d1bkJhc2UnO1xyXG5pbXBvcnQgT0RCbHVlR3VuQmFzZSBmcm9tICcuLy4uL0dhbWVPYmplY3RzL09EQmx1ZUd1bkJhc2UnO1xyXG5pbXBvcnQgT0RHcmV5R3VuQmFzZSBmcm9tICcuLy4uL0dhbWVPYmplY3RzL09ER3JleUd1bkJhc2UnO1xyXG5pbXBvcnQgT0RZZWxsb3dHdW4gZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PRFllbGxvd0d1bic7XHJcbmltcG9ydCBPREJsdWVHdW4gZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PREJsdWVHdW4nO1xyXG5pbXBvcnQgT0RHcmV5R3VuIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RHcmV5R3VuJztcclxuaW1wb3J0IE9ER2FtZUluZm8gZnJvbSAnLi8uLi9Nb2RlbHMvT0RHYW1lSW5mbyc7XHJcbmltcG9ydCBPRExldmVsSW5mbyBmcm9tICcuLy4uL01vZGVscy9HYW1lSW5mb01vZGVscy9PRExldmVsSW5mbyc7XHJcbmltcG9ydCBPRFJhZGFySW5mbyBmcm9tICcuLy4uL01vZGVscy9HYW1lSW5mb01vZGVscy9PRFJhZGFySW5mbyc7XHJcbmltcG9ydCBPRENhY2hlTWFuYWdlciBmcm9tICcuLy4uL01hbmFnZXJzL09EQ2FjaGVNYW5hZ2VyJztcclxuaW1wb3J0IFJhZGFyRXZlbnQgZnJvbSAnLi8uLi9Nb2RlbHMvUmFkYXJFdmVudCc7XHJcbmltcG9ydCBPRFdoaXRlRXhwbG9zaW9uIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RXaGl0ZUV4cGxvc2lvbic7XHJcbmNsYXNzIE9EQm9vdCBleHRlbmRzIFBoYXNlci5TY2VuZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcih7XHJcbiAgICAgICAgICAgIGtleTogJ0Jvb3RTY2VuZSdcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuZm9udHMubG9hZCgnMTBwdCBcImV0aG5vY2VudHJpY19yZ1wiJyk7XHJcbiAgICAgICAgdGhpcy5zeXMucGx1Z2lucy5yZWdpc3RlckdhbWVPYmplY3QoJ3JlZGV4cGxvc2lvbicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCByZWRFeHBsb3Npb24gPSBuZXcgT0RSZWRFeHBsb3Npb24odGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ3JlZGV4cGxvZGUwMDA0LnBuZycpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlMaXN0LmFkZChyZWRFeHBsb3Npb24pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKHJlZEV4cGxvc2lvbik7XHJcbiAgICAgICAgICAgIHJldHVybiByZWRFeHBsb3Npb247XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zeXMucGx1Z2lucy5yZWdpc3RlckdhbWVPYmplY3QoJ3doaXRlZXhwbG9zaW9uJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IHdoaXRlRXhwbG9zaW9uID0gbmV3IE9EV2hpdGVFeHBsb3Npb24odGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2V4cGxvZGV3aGl0ZTAwMDMucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHdoaXRlRXhwbG9zaW9uKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZCh3aGl0ZUV4cGxvc2lvbik7XHJcbiAgICAgICAgICAgIHJldHVybiB3aGl0ZUV4cGxvc2lvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnbWlzc2lsZScsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBtaXNzaWxlID0gbmV3IE9ETWlzc2lsZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnZ3JlZW5taXNzaWxlLnBuZycpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlMaXN0LmFkZChtaXNzaWxlKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChtaXNzaWxlKTtcclxuICAgICAgICAgICAgcmV0dXJuIG1pc3NpbGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zeXMucGx1Z2lucy5yZWdpc3RlckdhbWVPYmplY3QoJ29jZWFuJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IG9jZWFuID0gbmV3IE9ET2NlYW4odGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ29jZWFuLnBuZycpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlMaXN0LmFkZChvY2Vhbik7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQob2NlYW4pO1xyXG4gICAgICAgICAgICByZXR1cm4gb2NlYW47XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zeXMucGx1Z2lucy5yZWdpc3RlckdhbWVPYmplY3QoJ3NreScsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBza3kgPSBuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnb2NlYW5ib3JkZXIucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHNreSk7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQoc2t5KTtcclxuICAgICAgICAgICAgcmV0dXJuIHNreTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgneWVsbG93YmFzZScsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCB5YmFzZSA9IG5ldyBPRFllbGxvd0d1bkJhc2UodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2Jhc2V5ZWxsb3cucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHliYXNlKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZCh5YmFzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiB5YmFzZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnYmx1ZWJhc2UnLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgYmJhc2UgPSBuZXcgT0RCbHVlR3VuQmFzZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnYmFzZWJsdWUucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGJiYXNlKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChiYmFzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBiYmFzZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnZ3JleWJhc2UnLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgZ2Jhc2UgPSBuZXcgT0RHcmV5R3VuQmFzZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnYmFzZWdyZXkucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGdiYXNlKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChnYmFzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBnYmFzZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgneWVsbG93Z3VuJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IHlndW4gPSBuZXcgT0RZZWxsb3dHdW4odGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2d1bnllbGxvdy5wbmcnKTtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5TGlzdC5hZGQoeWd1bik7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQoeWd1bik7XHJcbiAgICAgICAgICAgIHJldHVybiB5Z3VuO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc3lzLnBsdWdpbnMucmVnaXN0ZXJHYW1lT2JqZWN0KCdibHVlZ3VuJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IGJndW4gPSBuZXcgT0RCbHVlR3VuKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdndW5ibHVlLnBuZycpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlMaXN0LmFkZChiZ3VuKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChiZ3VuKTtcclxuICAgICAgICAgICAgcmV0dXJuIGJndW47XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zeXMucGx1Z2lucy5yZWdpc3RlckdhbWVPYmplY3QoJ2dyZXlndW4nLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgZ2d1biA9IG5ldyBPREdyZXlHdW4odGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2d1bmdyZXkucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGdndW4pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKGdndW4pO1xyXG4gICAgICAgICAgICByZXR1cm4gZ2d1bjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgndHZtb25pdG9yZW1wdHknLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgdHYgPSBuZXcgT0RHcmV5R3VuQmFzZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAndHZtb25pdG9yZW1wdHlmcmFtZWQucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHR2KTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZCh0dik7XHJcbiAgICAgICAgICAgIHJldHVybiB0djtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnY2xvdWQxJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IGNsb3VkMSA9IG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdjbG91ZDEucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGNsb3VkMSk7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQoY2xvdWQxKTtcclxuICAgICAgICAgICAgcmV0dXJuIGNsb3VkMTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnY2xvdWQyJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IGNsb3VkMiA9IG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdjbG91ZDIucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGNsb3VkMik7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQoY2xvdWQyKTtcclxuICAgICAgICAgICAgcmV0dXJuIGNsb3VkMjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnc3RhdGljdHYnLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgc3RhdGljdHYgPSBuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnc3RhdGljbW9uaXRvci0wMDAucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHN0YXRpY3R2KTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChzdGF0aWN0dik7XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0aWN0djtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnYmx1ZWJ1dHRvbicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBibHVlYnV0dG9uID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2JsdWVidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGJsdWVidXR0b24pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKGJsdWVidXR0b24pO1xyXG4gICAgICAgICAgICByZXR1cm4gYmx1ZWJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnZ3JlZW5idXR0b24nLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgZ3JlZW5idXR0b24gPSBuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnZ3JlZW5idXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGdyZWVuYnV0dG9uKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChncmVlbmJ1dHRvbik7XHJcbiAgICAgICAgICAgIHJldHVybiBncmVlbmJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnbGV2ZWxsb2NrYnV0dG9uJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IGxldmVsbG9ja2J1dHRvbiA9IG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdsZXZlbGxvY2tidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGxldmVsbG9ja2J1dHRvbik7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQobGV2ZWxsb2NrYnV0dG9uKTtcclxuICAgICAgICAgICAgcmV0dXJuIGxldmVsbG9ja2J1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnYmFja2J1dHRvbicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBiYWNrYnV0dG9uID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2JhY2tidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGJhY2tidXR0b24pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKGJhY2tidXR0b24pO1xyXG4gICAgICAgICAgICByZXR1cm4gYmFja2J1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnc2hpZWxkJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IHNoaWVsZCA9IG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdzaGllbGQucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHNoaWVsZCk7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQoc2hpZWxkKTtcclxuICAgICAgICAgICAgcmV0dXJuIHNoaWVsZDtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgncGF1c2VidXR0b24nLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgcGF1c2VidXR0b24gPSBuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAncGF1c2VidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHBhdXNlYnV0dG9uKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChwYXVzZWJ1dHRvbik7XHJcbiAgICAgICAgICAgIHJldHVybiBwYXVzZWJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnb2NlYW5pbm5lcmJhcicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBvY2VhbmlubmVyYmFyID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ29jZWFuaW5uZXJiYXIucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKG9jZWFuaW5uZXJiYXIpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKG9jZWFuaW5uZXJiYXIpO1xyXG4gICAgICAgICAgICByZXR1cm4gb2NlYW5pbm5lcmJhcjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnb2NlYW5taWRkbGViYXInLCBmdW5jdGlvbiAoeCwgeSkge1xyXG4gICAgICAgICAgICBsZXQgb2NlYW5taWRkbGViYXIgPSBuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLnNjZW5lLCB4LCB5LCAnT0RJbWFnZXMnLCAnb2NlYW5taWRkbGViYXIucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKG9jZWFubWlkZGxlYmFyKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVMaXN0LmFkZChvY2Vhbm1pZGRsZWJhcik7XHJcbiAgICAgICAgICAgIHJldHVybiBvY2Vhbm1pZGRsZWJhcjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnb2NlYW5vdXRlcmJhcicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBvY2Vhbm91dGVyYmFyID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ29jZWFub3V0ZXJiYXIucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKG9jZWFub3V0ZXJiYXIpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKG9jZWFub3V0ZXJiYXIpO1xyXG4gICAgICAgICAgICByZXR1cm4gb2NlYW5vdXRlcmJhcjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgncGxheWJ1dHRvbicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBwbGF5YnV0dG9uID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ3BsYXlidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHBsYXlidXR0b24pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKHBsYXlidXR0b24pO1xyXG4gICAgICAgICAgICByZXR1cm4gcGxheWJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgncmVwbGF5YnV0dG9uJywgZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgbGV0IHJlcGxheWJ1dHRvbiA9IG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMuc2NlbmUsIHgsIHksICdPREltYWdlcycsICdyZXBsYXlidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHJlcGxheWJ1dHRvbik7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlTGlzdC5hZGQocmVwbGF5YnV0dG9uKTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcGxheWJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgnaG9tZWJ1dHRvbicsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBob21lYnV0dG9uID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ2hvbWVidXR0b24ucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKGhvbWVidXR0b24pO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKGhvbWVidXR0b24pO1xyXG4gICAgICAgICAgICByZXR1cm4gaG9tZWJ1dHRvbjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN5cy5wbHVnaW5zLnJlZ2lzdGVyR2FtZU9iamVjdCgncGF1c2VwYW5lbCcsIGZ1bmN0aW9uICh4LCB5KSB7XHJcbiAgICAgICAgICAgIGxldCBwYXVzZXBhbmVsID0gbmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcy5zY2VuZSwgeCwgeSwgJ09ESW1hZ2VzJywgJ3BhdXNlcGFuZWwucG5nJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUxpc3QuYWRkKHBhdXNlcGFuZWwpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUxpc3QuYWRkKHBhdXNlcGFuZWwpO1xyXG4gICAgICAgICAgICByZXR1cm4gcGF1c2VwYW5lbDtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIHByZWxvYWQoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkLm11bHRpYXRsYXMoJ09ESW1hZ2VzJywgJ2Fzc2V0cy9PRElJLmpzb24nLCAnYXNzZXRzJyk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1JhZGFyMScsIHsgeDogLTEwLCB5OiAtMTAgfSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1JhZGFyMicsIHsgeDogLTEwLCB5OiAtMTAgfSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1JhZGFyMycsIHsgeDogLTEwLCB5OiAtMTAgfSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ0FjdGl2ZVJhZGFyJywge30pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdTZXRNb25pdG9yRXhwbG9zaW9uJywge30pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdUcmFzaEF0V2F0ZXInLCAtMSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ01pc3NpbGVJbkZpbmFsUG9zaXRpb24nLCAtMSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1NldFJhZGFyQm9yZGVyJywge30pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdTdGFydENvb2xEb3duJywge30pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdFbmRDb29sRG93bicsIHt9KTtcclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnVHJhc2hEZXN0cm95ZWQnLCAtMSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1JhZGFyVXBkYXRlQ3ljbGUxJywgbmV3IFJhZGFyRXZlbnQoJ2luaXRpYWwxJykpO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdSYWRhclVwZGF0ZUN5Y2xlMicsIG5ldyBSYWRhckV2ZW50KCdpbml0aWFsMicpKTtcclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnUmFkYXJVcGRhdGVDeWNsZTMnLCBuZXcgUmFkYXJFdmVudCgnaW5pdGlhbDMnKSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ0dhbWVFdmVudFJhZGFyV29uJywge30pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdHYW1lRXZlbnRSYWRhckxvc3QnLCB7fSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ0dhbWVQYXVzZScsIHt9KTtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLmFuaW1zLmNyZWF0ZSh7XHJcbiAgICAgICAgICAgIGtleTogJ1JlZEV4cGxvZGUnLFxyXG4gICAgICAgICAgICBmcmFtZXM6IHRoaXMuYW5pbXMuZ2VuZXJhdGVGcmFtZU5hbWVzKCdPREltYWdlcycsIHsgc3RhcnQ6IDQsIGVuZDogMzUsIHplcm9QYWQ6IDQsIHByZWZpeDogJ3JlZGV4cGxvZGUnLCBzdWZmaXg6ICcucG5nJyB9KSxcclxuICAgICAgICAgICAgZnJhbWVSYXRlOiAxOCxcclxuICAgICAgICAgICAgcmVwZWF0OiAwLFxyXG4gICAgICAgICAgICBoaWRlT25Db21wbGV0ZTogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmFuaW1zLmNyZWF0ZSh7XHJcbiAgICAgICAgICAgIGtleTogJ1doaXRlRXhwbG9kZScsXHJcbiAgICAgICAgICAgIGZyYW1lczogdGhpcy5hbmltcy5nZW5lcmF0ZUZyYW1lTmFtZXMoJ09ESW1hZ2VzJywgeyBzdGFydDogMywgZW5kOiA0NiwgemVyb1BhZDogNCwgcHJlZml4OiAnZXhwbG9kZXdoaXRlJywgc3VmZml4OiAnLnBuZycgfSksXHJcbiAgICAgICAgICAgIGZyYW1lUmF0ZTogMjAsXHJcbiAgICAgICAgICAgIHJlcGVhdDogMCxcclxuICAgICAgICAgICAgaGlkZU9uQ29tcGxldGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hbmltcy5jcmVhdGUoe1xyXG4gICAgICAgICAgICBrZXk6ICdUcmFzaCcsXHJcbiAgICAgICAgICAgIGZyYW1lczogdGhpcy5hbmltcy5nZW5lcmF0ZUZyYW1lTmFtZXMoJ09ESW1hZ2VzJywgeyBzdGFydDogMCwgZW5kOiAxNCwgemVyb1BhZDogMywgcHJlZml4OiAndHJhc2hjb2xsZWN0aW9uLScsIHN1ZmZpeDogJy5wbmcnIH0pLFxyXG4gICAgICAgICAgICBmcmFtZVJhdGU6IDAsXHJcbiAgICAgICAgICAgIHJlcGVhdDogMCxcclxuICAgICAgICAgICAgaGlkZU9uQ29tcGxldGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hbmltcy5jcmVhdGUoe1xyXG4gICAgICAgICAgICBrZXk6ICdTdGF0aWNUdicsXHJcbiAgICAgICAgICAgIGZyYW1lczogdGhpcy5hbmltcy5nZW5lcmF0ZUZyYW1lTmFtZXMoJ09ESW1hZ2VzJywgeyBzdGFydDogMCwgZW5kOiA4LCB6ZXJvUGFkOiAzLCBwcmVmaXg6ICdzdGF0aWNtb25pdG9yLScsIHN1ZmZpeDogJy5wbmcnIH0pLFxyXG4gICAgICAgICAgICBmcmFtZVJhdGU6IDgsXHJcbiAgICAgICAgICAgIHJlcGVhdDogLTEsXHJcbiAgICAgICAgICAgIGhpZGVPbkNvbXBsZXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuYW5pbXMuY3JlYXRlKHtcclxuICAgICAgICAgICAga2V5OiAnQmFzZScsXHJcbiAgICAgICAgICAgIGZyYW1lczogW1xyXG4gICAgICAgICAgICAgICAgeyBrZXk6ICdPREltYWdlcycsIGZyYW1lOiAnYmFzZXllbGxvdy5wbmcnIH0sXHJcbiAgICAgICAgICAgICAgICB7IGtleTogJ09ESW1hZ2VzJywgZnJhbWU6ICdiYXNlYmx1ZS5wbmcnIH0sXHJcbiAgICAgICAgICAgICAgICB7IGtleTogJ09ESW1hZ2VzJywgZnJhbWU6ICdiYXNlZ3JleS5wbmcnIH1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgZnJhbWVSYXRlOiAwLFxyXG4gICAgICAgICAgICByZXBlYXQ6IDAsXHJcbiAgICAgICAgICAgIGhpZGVPbkNvbXBsZXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuYW5pbXMuY3JlYXRlKHtcclxuICAgICAgICAgICAga2V5OiAnR3VuJyxcclxuICAgICAgICAgICAgZnJhbWVzOiBbXHJcbiAgICAgICAgICAgICAgICB7IGtleTogJ09ESW1hZ2VzJywgZnJhbWU6ICdndW55ZWxsb3cucG5nJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBrZXk6ICdPREltYWdlcycsIGZyYW1lOiAnZ3VuYmx1ZS5wbmcnIH0sXHJcbiAgICAgICAgICAgICAgICB7IGtleTogJ09ESW1hZ2VzJywgZnJhbWU6ICdndW5ncmV5LnBuZycgfVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICBmcmFtZVJhdGU6IDAsXHJcbiAgICAgICAgICAgIHJlcGVhdDogMCxcclxuICAgICAgICAgICAgaGlkZU9uQ29tcGxldGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hbmltcy5jcmVhdGUoe1xyXG4gICAgICAgICAgICBrZXk6ICdFbXB0eVJhZGFycycsXHJcbiAgICAgICAgICAgIGZyYW1lczogW1xyXG4gICAgICAgICAgICAgICAgeyBrZXk6ICdPREltYWdlcycsIGZyYW1lOiAndHZtb25pdG9yZW1wdHlmcmFtZWQucG5nJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBrZXk6ICdPREltYWdlcycsIGZyYW1lOiAndHZtb25pdG9yZW1wdHlmcmFtZWRncmVlbi5wbmcnIH1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgZnJhbWVSYXRlOiAwLFxyXG4gICAgICAgICAgICByZXBlYXQ6IDAsXHJcbiAgICAgICAgICAgIGhpZGVPbkNvbXBsZXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuU2V0R2FtZUluZm8oKTtcclxuICAgIH1cclxuICAgIHRyYW5zaXRpb25Ub1NjZW5lKGtleSkge1xyXG4gICAgICAgIHRoaXMuc2NlbmUuc3RhcnQoa2V5KTtcclxuICAgIH1cclxuICAgIFNldEdhbWVJbmZvKCkge1xyXG4gICAgICAgIE9EQ2FjaGVNYW5hZ2VyLmdldEl0ZW0oJ09ER2FtZUluZm8nKS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlID0gZGF0YTtcclxuICAgICAgICAgICAgICAgIHRoaXMudHJhbnNpdGlvblRvU2NlbmUoJ1RpdGxlU2NyZWVuJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlNldERlZmF1bHRHYW1lSW5mbygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdHZXQgT0RHYW1lSW5mbyBmcm9tIGNhY2hlIHdhcyByZWplY3RlZDonKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICB0aGlzLlNldERlZmF1bHRHYW1lSW5mbygpO1xyXG4gICAgICAgIH0pLmNhdGNoKChyZWFzb24pID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0dldCBPREdhbWVJbmZvIGZyb20gY2FjaGUgZXhjZXB0aW9uOicpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZWFzb24pO1xyXG4gICAgICAgICAgICB0aGlzLlNldERlZmF1bHRHYW1lSW5mbygpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgU2V0RGVmYXVsdEdhbWVJbmZvKCkge1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuY3VyckxldmVsID0gMDtcclxuICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJQb2ludHMgPSAwO1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuaGlMZXZlbCA9IDA7XHJcbiAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5oaVBvaW50cyA9IDA7XHJcbiAgICAgICAgbGV0IGxldmVsMCA9IG5ldyBPRExldmVsSW5mbygpO1xyXG4gICAgICAgIGxldmVsMC5jbGVhcmVkID0gdHJ1ZTtcclxuICAgICAgICBsZXZlbDAubGV2ZWxOdW0gPSAwO1xyXG4gICAgICAgIGxldmVsMC5taXNzaWxlU3BlZWQgPSAzMDA7XHJcbiAgICAgICAgbGV2ZWwwLm5hbWUgPSAnVGl0bGUgU2NyZWVuJztcclxuICAgICAgICBsZXZlbDAucG9pbnRWYWx1ZSA9IDA7XHJcbiAgICAgICAgbGV2ZWwwLnVubG9ja2VkID0gdHJ1ZTtcclxuICAgICAgICBsZXZlbDAucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKHRydWUsIDE1MDAsIDgwLCAxNTAsIDUsIDUpKTtcclxuICAgICAgICBsZXZlbDAucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKHRydWUsIDIwMDAsIDgwLCAxNTAsIDUsIDUpKTtcclxuICAgICAgICBsZXZlbDAucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKGZhbHNlLCAxNTAwLCA4MCwgMTUwLCA1LCA1KSk7XHJcbiAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHMucHVzaChsZXZlbDApO1xyXG4gICAgICAgIGxldCBsZXZlbDEgPSBuZXcgT0RMZXZlbEluZm8oKTtcclxuICAgICAgICBsZXZlbDEuY2xlYXJlZCA9IGZhbHNlO1xyXG4gICAgICAgIGxldmVsMS5sZXZlbE51bSA9IDE7XHJcbiAgICAgICAgbGV2ZWwxLm1pc3NpbGVTcGVlZCA9IDMwMDtcclxuICAgICAgICBsZXZlbDEubmFtZSA9ICdMZXZlbCAxJztcclxuICAgICAgICBsZXZlbDEucG9pbnRWYWx1ZSA9IDE7XHJcbiAgICAgICAgbGV2ZWwxLnVubG9ja2VkID0gdHJ1ZTtcclxuICAgICAgICBsZXZlbDEucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKHRydWUsIDM1MDAsIDgwLCAxMjAsIDIsIDUpKTtcclxuICAgICAgICBsZXZlbDEucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKGZhbHNlLCAzNTAwLCA4MCwgMTIwLCAyLCA1KSk7XHJcbiAgICAgICAgbGV2ZWwxLnJhZGFycy5wdXNoKG5ldyBPRFJhZGFySW5mbyhmYWxzZSwgMzUwMCwgODAsIDEyMCwgMiwgNSkpO1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzLnB1c2gobGV2ZWwxKTtcclxuICAgICAgICBsZXQgbGV2ZWwyID0gbmV3IE9ETGV2ZWxJbmZvKCk7XHJcbiAgICAgICAgbGV2ZWwyLmNsZWFyZWQgPSBmYWxzZTtcclxuICAgICAgICBsZXZlbDIubGV2ZWxOdW0gPSAyO1xyXG4gICAgICAgIGxldmVsMi5taXNzaWxlU3BlZWQgPSAzMDA7XHJcbiAgICAgICAgbGV2ZWwyLm5hbWUgPSAnTGV2ZWwgMic7XHJcbiAgICAgICAgbGV2ZWwyLnBvaW50VmFsdWUgPSA1O1xyXG4gICAgICAgIGxldmVsMi51bmxvY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIGxldmVsMi5yYWRhcnMucHVzaChuZXcgT0RSYWRhckluZm8odHJ1ZSwgMzAwMCwgMTUwLCAyNTAsIDMsIDUpKTtcclxuICAgICAgICBsZXZlbDIucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKGZhbHNlLCAzNTAwLCAxMDAsIDIwMCwgMiwgNSkpO1xyXG4gICAgICAgIGxldmVsMi5yYWRhcnMucHVzaChuZXcgT0RSYWRhckluZm8oZmFsc2UsIDM1MDAsIDEwMCwgMjAwLCAyLCA1KSk7XHJcbiAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHMucHVzaChsZXZlbDIpO1xyXG4gICAgICAgIGxldCBsZXZlbDMgPSBuZXcgT0RMZXZlbEluZm8oKTtcclxuICAgICAgICBsZXZlbDMuY2xlYXJlZCA9IGZhbHNlO1xyXG4gICAgICAgIGxldmVsMy5sZXZlbE51bSA9IDM7XHJcbiAgICAgICAgbGV2ZWwzLm1pc3NpbGVTcGVlZCA9IDMwMDtcclxuICAgICAgICBsZXZlbDMubmFtZSA9ICdMZXZlbCAzJztcclxuICAgICAgICBsZXZlbDMucG9pbnRWYWx1ZSA9IDU7XHJcbiAgICAgICAgbGV2ZWwzLnVubG9ja2VkID0gZmFsc2U7XHJcbiAgICAgICAgbGV2ZWwzLnJhZGFycy5wdXNoKG5ldyBPRFJhZGFySW5mbyh0cnVlLCAzMDAwLCAxNTAsIDI1MCwgMywgNSkpO1xyXG4gICAgICAgIGxldmVsMy5yYWRhcnMucHVzaChuZXcgT0RSYWRhckluZm8oZmFsc2UsIDM1MDAsIDEwMCwgMjAwLCAyLCA1KSk7XHJcbiAgICAgICAgbGV2ZWwzLnJhZGFycy5wdXNoKG5ldyBPRFJhZGFySW5mbyhmYWxzZSwgMzUwMCwgMTAwLCAyMDAsIDIsIDUpKTtcclxuICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlLmxldmVscy5wdXNoKGxldmVsMyk7XHJcbiAgICAgICAgbGV0IGxldmVsNCA9IG5ldyBPRExldmVsSW5mbygpO1xyXG4gICAgICAgIGxldmVsNC5jbGVhcmVkID0gZmFsc2U7XHJcbiAgICAgICAgbGV2ZWw0LmxldmVsTnVtID0gNDtcclxuICAgICAgICBsZXZlbDQubWlzc2lsZVNwZWVkID0gMzAwO1xyXG4gICAgICAgIGxldmVsNC5uYW1lID0gJ0xldmVsIDQnO1xyXG4gICAgICAgIGxldmVsNC5wb2ludFZhbHVlID0gNTtcclxuICAgICAgICBsZXZlbDQudW5sb2NrZWQgPSBmYWxzZTtcclxuICAgICAgICBsZXZlbDQucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKHRydWUsIDMwMDAsIDE1MCwgMjUwLCAzLCA1KSk7XHJcbiAgICAgICAgbGV2ZWw0LnJhZGFycy5wdXNoKG5ldyBPRFJhZGFySW5mbyhmYWxzZSwgMzUwMCwgMTAwLCAyMDAsIDIsIDUpKTtcclxuICAgICAgICBsZXZlbDQucmFkYXJzLnB1c2gobmV3IE9EUmFkYXJJbmZvKGZhbHNlLCAzNTAwLCAxMDAsIDIwMCwgMiwgNSkpO1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzLnB1c2gobGV2ZWw0KTtcclxuICAgICAgICB0aGlzLnRyYW5zaXRpb25Ub1NjZW5lKCdUaXRsZVNjcmVlbicpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EQm9vdDtcclxuIiwiaW1wb3J0IE9ETW9uaXRvciBmcm9tIFwiLi9PRE1vbml0b3JcIjtcclxuaW1wb3J0IE9ER2FtZUluZm8gZnJvbSBcIi4vLi4vTW9kZWxzL09ER2FtZUluZm9cIjtcclxuaW1wb3J0IE9EUmFkYXIgZnJvbSBcIi4vT0RSYWRhclwiO1xyXG5pbXBvcnQgUmFkYXJFdmVudCBmcm9tIFwiLi8uLi9Nb2RlbHMvUmFkYXJFdmVudFwiO1xyXG5pbXBvcnQgTW9uaXRvckNsaWNrRXZlbnQgZnJvbSBcIi4vLi4vTW9kZWxzL01vbml0b3JDbGlja0V2ZW50XCI7XHJcbmltcG9ydCBPRFBhdXNlU2NlbmUgZnJvbSBcIi4vT0RQYXVzZVNjZW5lXCI7XHJcbmNsYXNzIE9ER2FtZVNjZW5lIGV4dGVuZHMgUGhhc2VyLlNjZW5lIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKHtcclxuICAgICAgICAgICAga2V5OiAnR2FtZVNjcmVlbidcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGluaXQoZGF0YSkge1xyXG4gICAgICAgIHRoaXMubW9uaXRvciA9IHVuZGVmaW5lZDtcclxuICAgICAgICB0aGlzLmZhZGVUaW1lID0gMTAwMDtcclxuICAgICAgICBpZiAoZGF0YS5yZXNldFNjb3JlID09IHRydWUpIHtcclxuICAgICAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyUG9pbnRzID0gMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yYWRhcnMgPSBuZXcgQXJyYXkoKTtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDM7IGkrKykge1xyXG4gICAgICAgICAgICBsZXQga2V5ID0gJ1JhZGFyJyArIChpICsgMSkudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgbGV0IGN1cnJMZXZlbCA9IE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuY3VyckxldmVsO1xyXG4gICAgICAgICAgICBsZXQgcmFkYXIgPSBuZXcgT0RSYWRhcihrZXksIDAgKyAoMTYwICogaSksIDYzMCwgMTYwLCAyMTIuOCwgKGkgPT0gMCkgPyB0cnVlIDogZmFsc2UsIHRoaXMuZmFkZVRpbWUpO1xyXG4gICAgICAgICAgICB0aGlzLnNjZW5lLmFkZChrZXksIHJhZGFyLCB0cnVlKTtcclxuICAgICAgICAgICAgcmFkYXIuU2V0UmFkYXJPbihPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1tjdXJyTGV2ZWxdLnJhZGFyc1tpXS5hY3RpdmUpO1xyXG4gICAgICAgICAgICB0aGlzLnJhZGFycy5wdXNoKHJhZGFyKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IG1vbml0b3JXaWR0aCA9IHRoaXMuZ2FtZS5jb25maWcud2lkdGggLSA0MDtcclxuICAgICAgICB0aGlzLm1vbml0b3IgPSBuZXcgT0RNb25pdG9yKCdNb25pdG9yMScsICh0aGlzLmdhbWUuY29uZmlnLndpZHRoIC8gMi4wKSAtIChtb25pdG9yV2lkdGggLyAyLjApLCAzMCwgbW9uaXRvcldpZHRoLCBtb25pdG9yV2lkdGggKiAxLjMzLCB0aGlzLmZhZGVUaW1lKTtcclxuICAgICAgICB0aGlzLnNjZW5lLmFkZCgnTW9uaXRvcjEnLCB0aGlzLm1vbml0b3IsIHRydWUpO1xyXG4gICAgICAgIHRoaXMucGF1c2VQYW5lbCA9IG5ldyBPRFBhdXNlU2NlbmUoJ1BhdXNlUGFuZWwnLCAodGhpcy5nYW1lLmNvbmZpZy53aWR0aCAvIDIuMCkgLSAoMzUwIC8gMi4wKSwgKHRoaXMubW9uaXRvci55KSArICg1MCksIDM1MCwgNTAwKTtcclxuICAgICAgICB0aGlzLnNjZW5lLmFkZCgnUGF1c2VQYW5lbCcsIHRoaXMucGF1c2VQYW5lbCwgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdBY3RpdmVSYWRhcicsIG5ldyBSYWRhckV2ZW50KCdSYWRhcjEnKSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5ldmVudHMub24oJ2NoYW5nZWRhdGEnLCAocGFyZW50LCBrZXksIGRhdGEpID0+IHtcclxuICAgICAgICAgICAgc3dpdGNoIChrZXkpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ0dhbWVFdmVudFJhZGFyV29uJzpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlByb2Nlc3NHYW1lRXZlbnRSYWRhcldvbihkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgJ0dhbWVFdmVudFJhZGFyTG9zdCc6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzR2FtZUV2ZW50UmFkYXJMb3N0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdHYW1lUGF1c2UnOlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuUHJvY2Vzc0dhbWVQYXVzZShkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY2FtZXJhcy5tYWluLmZhZGVJbih0aGlzLmZhZGVUaW1lLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5yZWQsIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuZmFkZUNvbG9yLmdyZWVuLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5ibHVlKTtcclxuICAgICAgICBjb25zb2xlLmxvZygnU2hvdyBMZXZlbCBJbmZvJyk7XHJcbiAgICAgICAgdGhpcy50aW1lLmFkZEV2ZW50KHtcclxuICAgICAgICAgICAgZGVsYXk6IDIwMDAsXHJcbiAgICAgICAgICAgIGNhbGxiYWNrOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnU3RhcnQgcmFkYXIgdGltZXJzJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJhZGFycy5mb3JFYWNoKChyYWRhcikgPT4geyByYWRhci5TdGFydFRpbWVyKCk7IH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzR2FtZUV2ZW50UmFkYXJMb3N0KCkge1xyXG4gICAgICAgIHRoaXMudHJhbnNpdGlvblRvU2NlbmUoJ1RpdGxlU2NyZWVuJyk7XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzR2FtZVBhdXNlKGRhdGEpIHtcclxuICAgICAgICBpZiAoZGF0YS5yZXN0YXJ0TGV2ZWwpIHtcclxuICAgICAgICAgICAgdGhpcy5yZXN0YXJ0U2NlbmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoZGF0YS5nb3RvVGl0bGVTY3JlZW4pIHtcclxuICAgICAgICAgICAgdGhpcy50cmFuc2l0aW9uVG9TY2VuZSgnVGl0bGVTY3JlZW4nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAoZGF0YS5pc1BhdXNlZCkge1xyXG4gICAgICAgICAgICB0aGlzLnNjZW5lLnJ1bignUGF1c2VQYW5lbCcpO1xyXG4gICAgICAgICAgICB0aGlzLnNjZW5lLnBhdXNlKHRoaXMubW9uaXRvci5rZXkpO1xyXG4gICAgICAgICAgICB0aGlzLnJhZGFycy5mb3JFYWNoKChyYWRhcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2VuZS5wYXVzZShyYWRhci5rZXkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NlbmUuc2xlZXAoJ1BhdXNlUGFuZWwnKTtcclxuICAgICAgICAgICAgdGhpcy5zY2VuZS5yZXN1bWUodGhpcy5tb25pdG9yLmtleSk7XHJcbiAgICAgICAgICAgIHRoaXMucmFkYXJzLmZvckVhY2goKHJhZGFyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjZW5lLnJlc3VtZShyYWRhci5rZXkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzR2FtZUV2ZW50UmFkYXJXb24oZGF0YSkge1xyXG4gICAgICAgIGxldCBmb3VuZEF2YWlsYWJsZVJhZGFyID0gdGhpcy5yYWRhcnMuZmluZCh4ID0+IHguR2V0UmFkYXJPbigpICYmIHgua2V5ICE9IGRhdGEua2V5KTtcclxuICAgICAgICBpZiAoZm91bmRBdmFpbGFibGVSYWRhcikge1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnQWN0aXZlUmFkYXInLCBuZXcgUmFkYXJFdmVudChmb3VuZEF2YWlsYWJsZVJhZGFyLmtleSkpO1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnU2V0UmFkYXJCb3JkZXInLCBuZXcgTW9uaXRvckNsaWNrRXZlbnQoZm91bmRBdmFpbGFibGVSYWRhci5rZXkpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVzdGFydFNjZW5lKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcHJlcGFyZUZvclNjZW5lVHJhbnNpdGlvbigpIHtcclxuICAgICAgICB0aGlzLnJhZGFycy5sZW5ndGggPSAwO1xyXG4gICAgICAgIHRoaXMuc2NlbmUucmVtb3ZlKCdQYXVzZVBhbmVsJyk7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5yZW1vdmUoJ1JhZGFyMScpO1xyXG4gICAgICAgIHRoaXMuc2NlbmUucmVtb3ZlKCdSYWRhcjInKTtcclxuICAgICAgICB0aGlzLnNjZW5lLnJlbW92ZSgnUmFkYXIzJyk7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5yZW1vdmUoJ01vbml0b3IxJyk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5ldmVudHMub2ZmKCdjaGFuZ2VkYXRhJywgdW5kZWZpbmVkLCB1bmRlZmluZWQsIGZhbHNlKTtcclxuICAgIH1cclxuICAgIHJlc3RhcnRTY2VuZSgpIHtcclxuICAgICAgICB0aGlzLnByZXBhcmVGb3JTY2VuZVRyYW5zaXRpb24oKTtcclxuICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1tPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbF0uY2xlYXJlZCA9IHRydWU7XHJcbiAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyTGV2ZWwgKz0gMTtcclxuICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1tPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbF0udW5sb2NrZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuc2NlbmUucmVzdGFydCh7IHJlc2V0U2NvcmU6IGZhbHNlIH0pO1xyXG4gICAgfVxyXG4gICAgdHJhbnNpdGlvblRvU2NlbmUoa2V5KSB7XHJcbiAgICAgICAgdGhpcy5wcmVwYXJlRm9yU2NlbmVUcmFuc2l0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5zdGFydChrZXksIHsgcmVzZXRTY29yZTogdHJ1ZSB9KTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPREdhbWVTY2VuZTtcclxuIiwiaW1wb3J0IE9ER2FtZUluZm8gZnJvbSBcIi4vLi4vTW9kZWxzL09ER2FtZUluZm9cIjtcclxuY2xhc3MgT0RMZXZlbFNlbGVjdGlvbiBleHRlbmRzIFBoYXNlci5TY2VuZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcih7IGtleTogJ0xldmVsU2VsZWN0aW9uJyB9KTtcclxuICAgICAgICB0aGlzLmZhZGVUaW1lID0gMTAwMDtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLmFkZC50ZXh0KDUsIDE1LCAnU2VsZWN0IEEgTGV2ZWwnLCB7XHJcbiAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdldGhub2NlbnRyaWNfcmcnLFxyXG4gICAgICAgICAgICBmb250U2l6ZTogMzUsXHJcbiAgICAgICAgICAgIGNvbG9yOiAnIzE2NTU5ZCdcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmFkZC5iYWNrYnV0dG9uKDQ0NywgMzgpLnNldFNjYWxlKDAuNSkuc2V0SW50ZXJhY3RpdmUoKS5vbigncG9pbnRlcmRvd24nLCAocG9pbnRlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNjZW5lLnN0YXJ0KCdUaXRsZVNjcmVlbicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxldCBzdGFydFJvdyA9IDIwMDtcclxuICAgICAgICBsZXQgc3RhcnRDb2wgPSA5MDtcclxuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8IE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBsZXZlbEluZm8gPSBPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1tpXTtcclxuICAgICAgICAgICAgbGV0IGJ1dHRvblRvUmVuZGVyO1xyXG4gICAgICAgICAgICBpZiAobGV2ZWxJbmZvLmNsZWFyZWQgJiYgbGV2ZWxJbmZvLnVubG9ja2VkKSB7XHJcbiAgICAgICAgICAgICAgICBidXR0b25Ub1JlbmRlciA9IHRoaXMuYWRkLmdyZWVuYnV0dG9uKHN0YXJ0Q29sLCBzdGFydFJvdykuc2V0RGlzcGxheVNpemUoMTIxLCAxMzApLnNldEludGVyYWN0aXZlKCkub24oJ3BvaW50ZXJkb3duJywgKCkgPT4geyB0aGlzLnByb2Nlc3NMZXZlbENsaWNrKGkpOyB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkLnRleHQoc3RhcnRDb2wsIHN0YXJ0Um93IC0gMTAsIGkudG9TdHJpbmcoKSwge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdldGhub2NlbnRyaWNfcmcnLFxyXG4gICAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiA2NCxcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyNmZmZmZmYnXHJcbiAgICAgICAgICAgICAgICB9KS5zZXRPcmlnaW4oMC41LCAwLjUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKGxldmVsSW5mby51bmxvY2tlZCkge1xyXG4gICAgICAgICAgICAgICAgYnV0dG9uVG9SZW5kZXIgPSB0aGlzLmFkZC5ibHVlYnV0dG9uKHN0YXJ0Q29sLCBzdGFydFJvdykuc2V0RGlzcGxheVNpemUoMTIxLCAxMzApLnNldEludGVyYWN0aXZlKCkub24oJ3BvaW50ZXJkb3duJywgKCkgPT4geyB0aGlzLnByb2Nlc3NMZXZlbENsaWNrKGkpOyB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkLnRleHQoc3RhcnRDb2wsIHN0YXJ0Um93IC0gMTAsIGkudG9TdHJpbmcoKSwge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvbnRGYW1pbHk6ICdldGhub2NlbnRyaWNfcmcnLFxyXG4gICAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiA2NCxcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyNmZmZmZmYnXHJcbiAgICAgICAgICAgICAgICB9KS5zZXRPcmlnaW4oMC41LCAwLjUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYnV0dG9uVG9SZW5kZXIgPSB0aGlzLmFkZC5sZXZlbGxvY2tidXR0b24oc3RhcnRDb2wsIHN0YXJ0Um93KS5zZXREaXNwbGF5U2l6ZSgxMjEsIDEzMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYnV0dG9uVG9SZW5kZXIuc2V0RGlzcGxheVNpemUoMTIxLCAxMzApO1xyXG4gICAgICAgICAgICBzdGFydENvbCArPSBidXR0b25Ub1JlbmRlci5kaXNwbGF5V2lkdGggKyAyNTtcclxuICAgICAgICAgICAgaWYgKHN0YXJ0Q29sID49IDQwMCkge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRDb2wgPSA5MDtcclxuICAgICAgICAgICAgICAgIHN0YXJ0Um93ICs9IGJ1dHRvblRvUmVuZGVyLmRpc3BsYXlIZWlnaHQgKyAyNTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNhbWVyYXMubWFpbi5mYWRlSW4odGhpcy5mYWRlVGltZSwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IucmVkLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5ncmVlbiwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IuYmx1ZSk7XHJcbiAgICB9XHJcbiAgICBwcm9jZXNzTGV2ZWxDbGljayhsZXZlbE51bSkge1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuY3VyckxldmVsID0gbGV2ZWxOdW07XHJcbiAgICAgICAgdGhpcy5zY2VuZS5zdGFydCgnR2FtZVNjcmVlbicsIHsgcmVzZXRTY29yZTogdHJ1ZSB9KTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPRExldmVsU2VsZWN0aW9uO1xyXG4iLCJpbXBvcnQgT0RUcmFzaCBmcm9tICcuLy4uL0dhbWVPYmplY3RzL09EVHJhc2gnO1xyXG5pbXBvcnQgT0RTdGF0ZSBmcm9tICcuLy4uL0VudW1zL09EU3RhdGUnO1xyXG5pbXBvcnQgT0RNaXNzaWxlIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RNaXNzaWxlJztcclxuaW1wb3J0IE9EQmFzZUVudW0gZnJvbSAnLi8uLi9FbnVtcy9PREJhc2VFbnVtJztcclxuaW1wb3J0IE1vbml0b3JDbGlja0V2ZW50IGZyb20gJy4vLi4vTW9kZWxzL01vbml0b3JDbGlja0V2ZW50JztcclxuaW1wb3J0IE9EUmVkRXhwbG9zaW9uIGZyb20gJy4uL0dhbWVPYmplY3RzL09EUmVkRXhwbG9zaW9uJztcclxuaW1wb3J0IE9ER2FtZUluZm8gZnJvbSAnLi8uLi9Nb2RlbHMvT0RHYW1lSW5mbyc7XHJcbmltcG9ydCBPRFdoaXRlRXhwbG9zaW9uIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RXaGl0ZUV4cGxvc2lvbic7XHJcbmltcG9ydCBHYW1lRXZlbnQgZnJvbSAnLi8uLi9Nb2RlbHMvR2FtZUV2ZW50JztcclxuY2xhc3MgT0RNb25pdG9yIGV4dGVuZHMgUGhhc2VyLlNjZW5lIHtcclxuICAgIGNvbnN0cnVjdG9yKGtleSwgeCwgeSwgZGlzcGxheVdpZHRoLCBkaXNwbGF5SGVpZ2h0LCBmYWRlVGltZSkge1xyXG4gICAgICAgIHN1cGVyKHtcclxuICAgICAgICAgICAga2V5XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5rZXkgPSBrZXk7XHJcbiAgICAgICAgdGhpcy54ID0geDtcclxuICAgICAgICB0aGlzLnkgPSB5O1xyXG4gICAgICAgIHRoaXMuZGlzcGxheVdpZHRoID0gZGlzcGxheVdpZHRoO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUhlaWdodCA9IGRpc3BsYXlIZWlnaHQ7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVSYWRhciA9ICcnO1xyXG4gICAgICAgIHRoaXMuaWdub3JlSW5wdXQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmZhZGVUaW1lID0gZmFkZVRpbWU7XHJcbiAgICAgICAgdGhpcy5pc1BhdXNlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcHJlbG9hZCgpIHtcclxuICAgICAgICB0aGlzLnRyYXNoUG9vbCA9IHRoaXMuYWRkLmdyb3VwKHtcclxuICAgICAgICAgICAgbWF4U2l6ZTogMjAsXHJcbiAgICAgICAgICAgIGRlZmF1bHRLZXk6ICdPREltYWdlcycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRGcmFtZTogJ3RyYXNoY29sbGVjdGlvbi0wMDAucG5nJyxcclxuICAgICAgICAgICAgY2xhc3NUeXBlOiBPRFRyYXNoLFxyXG4gICAgICAgICAgICBjcmVhdGVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJlbW92ZUNhbGxiYWNrOiAodHJhc2gpID0+IHsgfSxcclxuICAgICAgICAgICAgcnVuQ2hpbGRVcGRhdGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy50cmFzaFRyYWNraW5nUG9vbCA9IHRoaXMuYWRkLmdyb3VwKHtcclxuICAgICAgICAgICAgbWF4U2l6ZTogMTAsXHJcbiAgICAgICAgICAgIGRlZmF1bHRLZXk6ICdPREltYWdlcycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRGcmFtZTogJ3RyYXNoY29sbGVjdGlvbi0wMDAucG5nJyxcclxuICAgICAgICAgICAgY2xhc3NUeXBlOiBPRFRyYXNoLFxyXG4gICAgICAgICAgICBjcmVhdGVDYWxsYmFjazogKHRyYXNoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0cmFzaC5pZ25vcmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMubWlzc2lsZVBvb2wgPSB0aGlzLmFkZC5ncm91cCh7XHJcbiAgICAgICAgICAgIG1heFNpemU6IDEwLFxyXG4gICAgICAgICAgICBkZWZhdWx0S2V5OiAnT0RJbWFnZXMnLFxyXG4gICAgICAgICAgICBkZWZhdWx0RnJhbWU6ICdncmVlbm1pc3NpbGUucG5nJyxcclxuICAgICAgICAgICAgY2xhc3NUeXBlOiBPRE1pc3NpbGUsXHJcbiAgICAgICAgICAgIGNyZWF0ZUNhbGxiYWNrOiAodHJhc2gpID0+IHsgfSxcclxuICAgICAgICAgICAgcmVtb3ZlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICBydW5DaGlsZFVwZGF0ZTogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmV4cGxvc2lvblBvb2wgPSB0aGlzLmFkZC5ncm91cCh7XHJcbiAgICAgICAgICAgIG1heFNpemU6IDEwLFxyXG4gICAgICAgICAgICBkZWZhdWx0S2V5OiAnT0RJbWFnZXMnLFxyXG4gICAgICAgICAgICBkZWZhdWx0RnJhbWU6ICdyZWRleHBsb2RlMDAwNC5wbmcnLFxyXG4gICAgICAgICAgICBjbGFzc1R5cGU6IE9EUmVkRXhwbG9zaW9uLFxyXG4gICAgICAgICAgICBjcmVhdGVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJlbW92ZUNhbGxiYWNrOiAodHJhc2gpID0+IHsgfSxcclxuICAgICAgICAgICAgcnVuQ2hpbGRVcGRhdGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5leHBsb3Npb25UcmFja2luZ1Bvb2wgPSB0aGlzLmFkZC5ncm91cCh7XHJcbiAgICAgICAgICAgIG1heFNpemU6IDEwLFxyXG4gICAgICAgICAgICBkZWZhdWx0S2V5OiAnT0RJbWFnZXMnLFxyXG4gICAgICAgICAgICBkZWZhdWx0RnJhbWU6ICdleHBsb2Rld2hpdGUwMDAzLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogT0RXaGl0ZUV4cGxvc2lvbixcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY2xvdWRQb29sID0gdGhpcy5hZGQuZ3JvdXAoe1xyXG4gICAgICAgICAgICBtYXhTaXplOiAxMCxcclxuICAgICAgICAgICAgZGVmYXVsdEtleTogJ09ESW1hZ2VzJyxcclxuICAgICAgICAgICAgZGVmYXVsdEZyYW1lOiAnY2xvdWQxLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSxcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZmlzaFBvb2wgPSB0aGlzLmFkZC5ncm91cCh7XHJcbiAgICAgICAgICAgIG1heFNpemU6IDUsXHJcbiAgICAgICAgICAgIGRlZmF1bHRLZXk6ICdPREltYWdlcycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRGcmFtZTogJ2Zpc2gxLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSxcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgY3JlYXRlKCkge1xyXG4gICAgICAgIHRoaXMuZXZlbnRzLm9uKCdyZXN1bWUnLCAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyTGV2ZWwgPiAwICYmIHRoaXMucGF1c2VCdXR0b24gJiYgdGhpcy5wYXVzZUJ1dHRvbi5hY3RpdmUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGF1c2VCdXR0b24uc2V0VmlzaWJsZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNQYXVzZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY2FtZXJhcy5tYWluLnNldFZpZXdwb3J0KHRoaXMueCwgdGhpcy55LCB0aGlzLmRpc3BsYXlXaWR0aCwgdGhpcy5kaXNwbGF5SGVpZ2h0KTtcclxuICAgICAgICB0aGlzLmFkZC5za3kodGhpcy5kaXNwbGF5V2lkdGggLyAyLjAsIHRoaXMuZGlzcGxheUhlaWdodCAvIDIuMCkuc2V0RGlzcGxheVNpemUodGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5jbG91ZFBvb2wuYWRkTXVsdGlwbGUoW1xyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuNTc2OSAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuNTc4NCAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDEucG5nJykpLnNldFNjYWxlKDAuMjUsIDAuMjApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuMzA3NyAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuMTczNSAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDEucG5nJykpLnNldFNjYWxlKDAuMjAsIDAuMjUpLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuMTUzOCAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuMzQ3MCAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDEucG5nJykpLnNldFNjYWxlKDAuMTUsIDAuMTUpLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuNzY5MiAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuMTczNSAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDEucG5nJykpLnNldFNjYWxlKDAuMjAsIDAuMTUpLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuMzg0NiAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuMjg5MiAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMjUsIDAuMjApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuNzY5MiAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuMzQ3MCAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMzUsIDAuMjApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuMjY5MiAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuNDYyNyAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMTUsIDAuMjApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuNzY5MiAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuNDkxNiAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMTUsIDAuMTUpXHJcbiAgICAgICAgXSwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5iYXNlID0gdGhpcy5hZGQueWVsbG93YmFzZSh0aGlzLmRpc3BsYXlXaWR0aCAvIDIuMCwgdGhpcy5kaXNwbGF5SGVpZ2h0IC0gKDAuMTQyMCAqIHRoaXMuZGlzcGxheUhlaWdodCkpLnNldFNjYWxlKDAuNCkuc2V0RGVwdGgoMik7XHJcbiAgICAgICAgdGhpcy5ndW4gPSB0aGlzLmFkZC55ZWxsb3dndW4odGhpcy5iYXNlLngsIHRoaXMuYmFzZS55IC0gNykuc2V0U2NhbGUoMC4yNSkuc2V0RGVwdGgoMikuc2V0QW5nbGUoMCk7XHJcbiAgICAgICAgdGhpcy5TZXRCYXNlQW5kR3VuQ29sb3IoKTtcclxuICAgICAgICB0aGlzLmFkZC5vY2Vhbih0aGlzLmRpc3BsYXlXaWR0aCAvIDIuMCwgdGhpcy5kaXNwbGF5SGVpZ2h0IC0gKDAuMDgyNSAqIHRoaXMuZGlzcGxheUhlaWdodCkpLnNldERpc3BsYXlTaXplKHRoaXMuZGlzcGxheVdpZHRoIC0gMjAsICgwLjA5NjggKiB0aGlzLmRpc3BsYXlIZWlnaHQpKS5zZXREZXB0aCgxKTtcclxuICAgICAgICB0aGlzLmZpc2hQb29sLmFkZE11bHRpcGxlKFtcclxuICAgICAgICAgICAgKG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMsICgwLjMwNzcgKiB0aGlzLmRpc3BsYXlXaWR0aCksICgwLjg5NjUgKiB0aGlzLmRpc3BsYXlIZWlnaHQpLCAnT0RJbWFnZXMnLCAnZmlzaDEucG5nJykpLnNldFNjYWxlKDAuMzUpLnNldERlcHRoKDEpLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgKDAuNTAwMCAqIHRoaXMuZGlzcGxheVdpZHRoKSwgKDAuOTI1NCAqIHRoaXMuZGlzcGxheUhlaWdodCksICdPREltYWdlcycsICdmaXNoMi5wbmcnKSkuc2V0U2NhbGUoMC4zNSkuc2V0RGVwdGgoMSksXHJcbiAgICAgICAgICAgIChuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLCAoMC42NTM4ICogdGhpcy5kaXNwbGF5V2lkdGgpLCAoMC44OTY1ICogdGhpcy5kaXNwbGF5SGVpZ2h0KSwgJ09ESW1hZ2VzJywgJ2Zpc2gzLnBuZycpKS5zZXRTY2FsZSgwLjM1KS5zZXREZXB0aCgxKSxcclxuICAgICAgICAgICAgKG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMsICgwLjczMDggKiB0aGlzLmRpc3BsYXlXaWR0aCksICgwLjkyNTQgKiB0aGlzLmRpc3BsYXlIZWlnaHQpLCAnT0RJbWFnZXMnLCAnZmlzaDQucG5nJykpLnNldFNjYWxlKDAuMzUpLnNldERlcHRoKDEpXHJcbiAgICAgICAgXSwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy50d2VlbnMudGltZWxpbmUoe1xyXG4gICAgICAgICAgICB0YXJnZXRzOiBbdGhpcy5iYXNlXSxcclxuICAgICAgICAgICAgbG9vcDogLTEsXHJcbiAgICAgICAgICAgIHR3ZWVuczogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGFuZ2xlOiAtMjUsXHJcbiAgICAgICAgICAgICAgICAgICAgZWFzZTogJ1NpbmUuZWFzZUluT3V0JyxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwMFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBvZmZzZXQ6IDMwMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYW5nbGU6IDI1LFxyXG4gICAgICAgICAgICAgICAgICAgIGVhc2U6ICdTaW5lLmVhc2VJbk91dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDYwMDBcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiA5MDAwLFxyXG4gICAgICAgICAgICAgICAgICAgIGFuZ2xlOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgIGVhc2U6ICdTaW5lLmVhc2VJbk91dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMDBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuYWRkLnR2bW9uaXRvcmVtcHR5KDAsIDApLnNldERpc3BsYXlTaXplKHRoaXMuZGlzcGxheVdpZHRoLCB0aGlzLmRpc3BsYXlIZWlnaHQpLnNldERlcHRoKDIpLnNldEludGVyYWN0aXZlKCkub24oUGhhc2VyLklucHV0LkV2ZW50cy5HQU1FT0JKRUNUX1BPSU5URVJfRE9XTiwgKHBvaW50ZXIsIGV2ZW50LCBldmVudDIsIGV2ZW50MywgZXZlbnQ0KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pZ25vcmVJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzQ2xpY2socG9pbnRlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN0YXRpY3R2ID0gdGhpcy5hZGQuc3RhdGljdHYoMCwgMCkuc2V0RGlzcGxheVNpemUodGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCkuc2V0RGVwdGgoMykuc2V0QWN0aXZlKGZhbHNlKS5zZXRWaXNpYmxlKGZhbHNlKTtcclxuICAgICAgICB0aGlzLm9jZWFub3V0dGVyYmFyID0gdGhpcy5hZGQub2NlYW5vdXRlcmJhcigodGhpcy5kaXNwbGF5V2lkdGggLyAyLjApIC0gKCh0aGlzLmRpc3BsYXlXaWR0aCAvIDI2MC4wKSAqIDM1KSwgdGhpcy5kaXNwbGF5SGVpZ2h0IC0gKCh0aGlzLmRpc3BsYXlIZWlnaHQgLyAzNDUuOCkgKiAxOCkpLnNldERpc3BsYXlTaXplKCh0aGlzLmRpc3BsYXlXaWR0aCAvIDI2MC4wKSAqIDcwLjg0LCAodGhpcy5kaXNwbGF5SGVpZ2h0IC8gMzQ1LjgpICogMTAuMDgpLnNldERlcHRoKDEpO1xyXG4gICAgICAgICh0aGlzLmFkZCkub2NlYW5taWRkbGViYXIodGhpcy5vY2Vhbm91dHRlcmJhci54LCB0aGlzLm9jZWFub3V0dGVyYmFyLnkpLnNldERpc3BsYXlTaXplKHRoaXMub2NlYW5vdXR0ZXJiYXIuZGlzcGxheVdpZHRoLCB0aGlzLm9jZWFub3V0dGVyYmFyLmRpc3BsYXlIZWlnaHQpLnNldERlcHRoKDEpO1xyXG4gICAgICAgIHRoaXMub2NlYW5pbm5lcmJhciA9IHRoaXMuYWRkLm9jZWFuaW5uZXJiYXIodGhpcy5vY2Vhbm91dHRlcmJhci54ICsgMiwgdGhpcy5vY2Vhbm91dHRlcmJhci55ICsgMikuc2V0RGlzcGxheVNpemUodGhpcy5vY2Vhbm91dHRlcmJhci5kaXNwbGF5V2lkdGggLSA0LCB0aGlzLm9jZWFub3V0dGVyYmFyLmRpc3BsYXlIZWlnaHQgLSA0KS5zZXREZXB0aCgxKTtcclxuICAgICAgICB0aGlzLm9jZWFuaW5uZXJiYXJkZWZhdWx0ID0gdGhpcy5vY2VhbmlubmVyYmFyLmRpc3BsYXlXaWR0aDtcclxuICAgICAgICB0aGlzLmFkZC5zaGllbGQoMTAwICogKHRoaXMuZGlzcGxheVdpZHRoIC8gMjYwLjApLCAzMCAqICh0aGlzLmRpc3BsYXlIZWlnaHQgLyAzNDUuOCkpLnNldFNjYWxlKDAuMzUgKiAodGhpcy5kaXNwbGF5V2lkdGggLyAyNjAuMCkpLnNldERlcHRoKDIpO1xyXG4gICAgICAgIHRoaXMuc2NvcmVUZXh0ID0gdGhpcy5hZGQudGV4dCgxMjUgKiAodGhpcy5kaXNwbGF5V2lkdGggLyAyNjAuMCksIDE1ICogKHRoaXMuZGlzcGxheUhlaWdodCAvIDM0NS44KSwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyUG9pbnRzLnRvU3RyaW5nKCksIHtcclxuICAgICAgICAgICAgZm9udEZhbWlseTogJ2V0aG5vY2VudHJpY19yZycsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAyNCAqICh0aGlzLmRpc3BsYXlXaWR0aCAvIDI2MC4wKSxcclxuICAgICAgICAgICAgY29sb3I6ICcjRkZDQzAyJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5wYXVzZUJ1dHRvbiA9IHRoaXMuYWRkLnBhdXNlYnV0dG9uKHRoaXMuZGlzcGxheVdpZHRoIC0gMjUsIDI1KS5zZXRTY2FsZSgwLjMwKS5zZXREZXB0aCgyKS5zZXRJbnRlcmFjdGl2ZSgpO1xyXG4gICAgICAgICAgICB0aGlzLnBhdXNlQnV0dG9uLm9uKCdwb2ludGVyZG93bicsIChwb2ludGVyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlBhdXNlQnV0dG9uQ2xpY2socG9pbnRlcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLlNldFVwUGFydGljYWxTeXN0ZW0oKTtcclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LmV2ZW50cy5vbignY2hhbmdlZGF0YScsIChwYXJlbnQsIGtleSwgZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGtleSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnUmFkYXJVcGRhdGVDeWNsZTEnOlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuUHJvY2Vzc1JhZGFyVXBkYXRlQ3ljbGUoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdSYWRhclVwZGF0ZUN5Y2xlMic6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzUmFkYXJVcGRhdGVDeWNsZShkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgJ1JhZGFyVXBkYXRlQ3ljbGUzJzpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlByb2Nlc3NSYWRhclVwZGF0ZUN5Y2xlKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnQWN0aXZlUmFkYXInOlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuUHJvY2Vzc0FjdGl2ZVJhZGFyKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnVHJhc2hBdFdhdGVyJzpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlByb2Nlc3NUcmFzaEF0V2F0ZXIoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdUcmFzaERlc3Ryb3llZCc6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzVHJhc2hEZXN0cm95ZWQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdNaXNzaWxlSW5GaW5hbFBvc2l0aW9uJzpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlByb2Nlc3NNaXNzaWxlSW5GaW5hbFBvc2l0aW9uKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnU2V0TW9uaXRvckV4cGxvc2lvbic6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzU2V0TW9uaXRvckV4cGxvc2lvbihkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgJ1N0YXJ0Q29vbERvd24nOlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuUHJvY2Vzc1N0YXJ0Q29vbERvd24oZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdFbmRDb29sRG93bic6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzRW5kQ29vbERvd24oZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmNhbWVyYXMubWFpbi5mYWRlSW4odGhpcy5mYWRlVGltZSwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IucmVkLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5ncmVlbiwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IuYmx1ZSk7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUodGltZSwgZHQpIHtcclxuICAgICAgICBQaGFzZXIuQWN0aW9ucy5DYWxsKHRoaXMudHJhc2hQb29sLmdldENoaWxkcmVuKCkuZmlsdGVyKCh4KSA9PiB4LmFjdGl2ZSksICh0cmFzaCkgPT4ge1xyXG4gICAgICAgICAgICB0cmFzaC5zZXRSb3RhdGlvbih0cmFzaC5yb3RhdGlvbiArICh0cmFzaC5yb3RhdGlvbkRlbHRhICogKGR0IC8gMTAwMC4wKSkpO1xyXG4gICAgICAgIH0sIHRoaXMpO1xyXG4gICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICBQaGFzZXIuQWN0aW9ucy5DYWxsKHRoaXMuY2xvdWRQb29sLmdldENoaWxkcmVuKCksIChjbG91ZCkgPT4ge1xyXG4gICAgICAgICAgICBjbG91ZC54ICs9ICgoKGR0IC8gMTAwMCkgKiA4KSArICgwLjA2ICogY291bnRlcikpO1xyXG4gICAgICAgICAgICBpZiAoY2xvdWQueCAtIChjbG91ZC5kaXNwbGF5V2lkdGggLyAyLjApID4gdGhpcy5kaXNwbGF5V2lkdGgpIHtcclxuICAgICAgICAgICAgICAgIGNsb3VkLnggPSAtY2xvdWQuZGlzcGxheVdpZHRoIC8gMi4wO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvdW50ZXIgKz0gMTtcclxuICAgICAgICB9LCB0aGlzKTtcclxuICAgICAgICBQaGFzZXIuQWN0aW9ucy5DYWxsKHRoaXMuZmlzaFBvb2wuZ2V0Q2hpbGRyZW4oKSwgKGZpc2gpID0+IHtcclxuICAgICAgICAgICAgZmlzaC54ICs9ICgoZHQgLyAxMDAwKSAqIDEwKTtcclxuICAgICAgICAgICAgaWYgKGZpc2gueCAtIChmaXNoLmRpc3BsYXlXaWR0aCAvIDIuMCkgPiB0aGlzLmRpc3BsYXlXaWR0aCkge1xyXG4gICAgICAgICAgICAgICAgZmlzaC54ID0gLWZpc2guZGlzcGxheVdpZHRoIC8gMi4wO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdGhpcyk7XHJcbiAgICB9XHJcbiAgICBTZXRJZ25vcmVJbnB1dE9uKGlnbm9yZSkge1xyXG4gICAgICAgIHRoaXMuaWdub3JlSW5wdXQgPSBpZ25vcmU7XHJcbiAgICB9XHJcbiAgICBEZW1vQ3JlYXRlTWlzc2lsZSgpIHtcclxuICAgICAgICB0aGlzLlByb2Nlc3NDbGljayh7IHdvcmxkWDogUGhhc2VyLk1hdGguUk5ELnJlYWxJblJhbmdlKDMwLCB0aGlzLmRpc3BsYXlXaWR0aCAtIDMwKSwgd29ybGRZOiBQaGFzZXIuTWF0aC5STkQucmVhbEluUmFuZ2UoNTAsIHRoaXMuZ3VuLnkgLSB0aGlzLmd1bi5oZWlnaHQgLSAyMCkgfSk7XHJcbiAgICB9XHJcbiAgICBQYXVzZUJ1dHRvbkNsaWNrKHBvaW50ZXIpIHtcclxuICAgICAgICB0aGlzLmlzUGF1c2VkID0gIXRoaXMuaXNQYXVzZWQ7XHJcbiAgICAgICAgbGV0IGdlID0gbmV3IEdhbWVFdmVudCgnR2FtZVBhdXNlJywgdGhpcy5pc1BhdXNlZCk7XHJcbiAgICAgICAgdGhpcy5wYXVzZUJ1dHRvbi5zZXRWaXNpYmxlKCF0aGlzLmlzUGF1c2VkKTtcclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnR2FtZVBhdXNlJywgZ2UpO1xyXG4gICAgfVxyXG4gICAgUHJvY2Vzc0NsaWNrKHBvaW50ZXIpIHtcclxuICAgICAgICBpZiAocG9pbnRlci53b3JsZFggPCAoMC4wOTYyICogdGhpcy5kaXNwbGF5V2lkdGgpIHx8IHBvaW50ZXIud29ybGRYID4gKDAuOTAzOCAqIHRoaXMuZGlzcGxheVdpZHRoKVxyXG4gICAgICAgICAgICB8fCBwb2ludGVyLndvcmxkWSA8ICgwLjEzMDEgKiB0aGlzLmRpc3BsYXlIZWlnaHQpIHx8IHBvaW50ZXIud29ybGRZID4gKDAuNzk1MyAqIHRoaXMuZGlzcGxheUhlaWdodCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgYW5nUmFkID0gUGhhc2VyLk1hdGguQW5nbGUuQmV0d2Vlbihwb2ludGVyLndvcmxkWCwgcG9pbnRlci53b3JsZFksIHRoaXMuZ3VuLngsIHRoaXMuZ3VuLnkpO1xyXG4gICAgICAgIGxldCBndW5BbmdsZSA9IFBoYXNlci5NYXRoLlJhZFRvRGVnKGFuZ1JhZCkgLSA5MDtcclxuICAgICAgICB0aGlzLmd1bi5zZXRBbmdsZShndW5BbmdsZSk7XHJcbiAgICAgICAgbGV0IGNsaWNrRXZlbnQgPSBuZXcgTW9uaXRvckNsaWNrRXZlbnQodGhpcy5hY3RpdmVSYWRhcik7XHJcbiAgICAgICAgY2xpY2tFdmVudC5pbml0aWFsWCA9IHRoaXMuZ3VuLnggLSAodGhpcy5ndW4uZGlzcGxheUhlaWdodCAqIE1hdGguY29zKGFuZ1JhZCkpO1xyXG4gICAgICAgIGNsaWNrRXZlbnQuaW5pdGlhbFkgPSB0aGlzLmd1bi55IC0gKHRoaXMuZ3VuLmRpc3BsYXlIZWlnaHQgKiBNYXRoLnNpbihhbmdSYWQpKTtcclxuICAgICAgICBjbGlja0V2ZW50LmZpbmFsWCA9IHBvaW50ZXIud29ybGRYO1xyXG4gICAgICAgIGNsaWNrRXZlbnQuZmluYWxZID0gcG9pbnRlci53b3JsZFk7XHJcbiAgICAgICAgY2xpY2tFdmVudC5tb25pdG9ySGVpZ2h0ID0gdGhpcy5kaXNwbGF5SGVpZ2h0O1xyXG4gICAgICAgIGNsaWNrRXZlbnQubW9uaXRvcldpZHRoID0gdGhpcy5kaXNwbGF5V2lkdGg7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQodGhpcy5hY3RpdmVSYWRhciwgY2xpY2tFdmVudCk7XHJcbiAgICB9XHJcbiAgICBTZXRCYXNlQW5kR3VuQ29sb3IoKSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IE9EQmFzZUVudW0uWWVsbG93O1xyXG4gICAgICAgIGlmICh0aGlzLmFjdGl2ZVJhZGFyID09ICdSYWRhcjInKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IE9EQmFzZUVudW0uQmx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAodGhpcy5hY3RpdmVSYWRhciA9PSAnUmFkYXIzJykge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBPREJhc2VFbnVtLkdyZXk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuYmFzZS5wbGF5KCdCYXNlJywgZmFsc2UsIHJlc3VsdCk7XHJcbiAgICAgICAgdGhpcy5ndW4ucGxheSgnR3VuJywgZmFsc2UsIHJlc3VsdCk7XHJcbiAgICB9XHJcbiAgICBTZXRVcFBhcnRpY2FsU3lzdGVtKCkge1xyXG4gICAgICAgIHRoaXMucGFydGljbGVNYW5hZ2VyID0gdGhpcy5hZGQucGFydGljbGVzKCdPREltYWdlcycsICd3YXRlcnNwbGFzaC5wbmcnKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSAzOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJ0aWNsZU1hbmFnZXIuY3JlYXRlRW1pdHRlcih7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnUmFkYXInICsgaS50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgeDogdGhpcy5kaXNwbGF5V2lkdGggLyAyLjAsXHJcbiAgICAgICAgICAgICAgICB5OiB0aGlzLmRpc3BsYXlIZWlnaHQgLyAyLjAsXHJcbiAgICAgICAgICAgICAgICBhbmdsZTogeyBtaW46IDI2MCwgbWF4OiAyODAgfSxcclxuICAgICAgICAgICAgICAgIHNwZWVkOiB7IG1pbjogMTAwLCBtYXg6IDE4MCB9LFxyXG4gICAgICAgICAgICAgICAgZ3Jhdml0eVk6IDg1MCxcclxuICAgICAgICAgICAgICAgIGFjY2VsZXJhdGlvblk6IC0zNTAsXHJcbiAgICAgICAgICAgICAgICBzY2FsZTogMC41LFxyXG4gICAgICAgICAgICAgICAgYmxlbmRNb2RlOiBQaGFzZXIuQmxlbmRNb2Rlcy5DT0xPUixcclxuICAgICAgICAgICAgICAgIG1heFBhcnRpY2xlczogMTAwMCxcclxuICAgICAgICAgICAgICAgIGxpZmVzcGFuOiA2MDAsXHJcbiAgICAgICAgICAgICAgICByYWRpYWw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBxdWFudGl0eTogNTAsXHJcbiAgICAgICAgICAgICAgICBvbjogZmFsc2VcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUHJvY2Vzc1JhZGFyVXBkYXRlQ3ljbGUoZGF0YSkge1xyXG4gICAgICAgIGlmIChkYXRhLmtleSA9PSB0aGlzLmFjdGl2ZVJhZGFyKSB7XHJcbiAgICAgICAgICAgIGRhdGEubWlzc2lsZXMuZm9yRWFjaCgocmVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZm91bmRNaXNzaWxlID0gdGhpcy5taXNzaWxlUG9vbC5nZXRDaGlsZHJlbigpLmZpbmQoeCA9PiB4LmlkID09IHJlZC5pZCAmJiB4LmFjdGl2ZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWZvdW5kTWlzc2lsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kTWlzc2lsZSA9IHRoaXMubWlzc2lsZVBvb2wuZ2V0KDAsIDApO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kTWlzc2lsZS5zZXRTY2FsZSgwLjA1KTtcclxuICAgICAgICAgICAgICAgICAgICBmb3VuZE1pc3NpbGUuaWQgPSByZWQuaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgZm91bmRNaXNzaWxlLnNldE9EU3RhdGUoT0RTdGF0ZS5WaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgICAgICAgICBmb3VuZE1pc3NpbGUudGFnID0gZGF0YS5rZXk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmb3VuZE1pc3NpbGUueCA9IHJlZC54ICogKHRoaXMuZGlzcGxheVdpZHRoIC8gcmVkLmRpc3BsYXlXaWR0aCk7XHJcbiAgICAgICAgICAgICAgICBmb3VuZE1pc3NpbGUueSA9IHJlZC55ICogKHRoaXMuZGlzcGxheUhlaWdodCAvIHJlZC5kaXNwbGF5SGVpZ2h0KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRhdGEudHJhc2guZm9yRWFjaCgocmVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZm91bmRUcmFzaCA9IHRoaXMudHJhc2hQb29sLmdldENoaWxkcmVuKCkuZmluZCh4ID0+IHguaWQgPT0gcmVkLmlkICYmIHguYWN0aXZlKTtcclxuICAgICAgICAgICAgICAgIGlmICghZm91bmRUcmFzaCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kVHJhc2ggPSB0aGlzLnRyYXNoUG9vbC5nZXQoMCwgMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZm91bmRUcmFzaC5wbGF5KCdUcmFzaCcsIGZhbHNlLCBQaGFzZXIuTWF0aC5STkQuYmV0d2VlbigwLCAxNCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kVHJhc2gucm90YXRpb25EZWx0YSA9IFBoYXNlci5NYXRoLlJORC5yZWFsSW5SYW5nZSgtNS4wLCA1LjApO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvdW5kVHJhc2guc2V0U2NhbGUoMC4yNSk7XHJcbiAgICAgICAgICAgICAgICAgICAgZm91bmRUcmFzaC5pZCA9IHJlZC5pZDtcclxuICAgICAgICAgICAgICAgICAgICBmb3VuZFRyYXNoLnNldE9EU3RhdGUoT0RTdGF0ZS5WaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgICAgICAgICBmb3VuZFRyYXNoLnRhZyA9IGRhdGEua2V5O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZm91bmRUcmFzaC54ID0gcmVkLnggKiAodGhpcy5kaXNwbGF5V2lkdGggLyByZWQuZGlzcGxheVdpZHRoKTtcclxuICAgICAgICAgICAgICAgIGZvdW5kVHJhc2gueSA9IHJlZC55ICogKHRoaXMuZGlzcGxheUhlaWdodCAvIHJlZC5kaXNwbGF5SGVpZ2h0KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGlmIChPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGxldCB0cmFzaFRhY2tlckV4cGxvc2lvbk5lZWRlZCA9IGRhdGEudHJhc2hUb0NsZWFyIDwgdGhpcy50cmFzaFRyYWNraW5nUG9vbC5nZXRUb3RhbFVzZWQoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudHJhc2hUcmFja2luZ1Bvb2wuZ2V0Q2hpbGRyZW4oKS5mb3JFYWNoKCh0cmFzaCkgPT4geyB0cmFzaC5zZXRPRFN0YXRlKE9EU3RhdGUuTm90VmlzaWJsZUFjdGl2ZSk7IH0pO1xyXG4gICAgICAgICAgICAgICAgbGV0IGluaXRYID0gNDA7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW5pdFkgPSA2MDtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YS50cmFzaFRvQ2xlYXI7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB0cmFzaCA9IHRoaXMudHJhc2hUcmFja2luZ1Bvb2wuZ2V0KGluaXRYLCBpbml0WSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhc2guc2V0U2NhbGUoMC4yMCkuc2V0RGVwdGgoMikuc2V0T0RTdGF0ZShPRFN0YXRlLlZpc2libGVBY3RpdmUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGluaXRYICs9IDIwO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpbml0WCA+IDE1MCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbml0WCA9IDQwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbml0WSArPSAyMztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodHJhc2hUYWNrZXJFeHBsb3Npb25OZWVkZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdHJhY2tFeHBsb3Npb24gPSB0aGlzLmV4cGxvc2lvblRyYWNraW5nUG9vbC5nZXQoaW5pdFgsIGluaXRZKS5zZXRTY2FsZSgwLjIwKS5zZXREZXB0aCgyKTtcclxuICAgICAgICAgICAgICAgICAgICB0cmFja0V4cGxvc2lvbi5zZXRPRFN0YXRlKE9EU3RhdGUuVmlzaWJsZUFjdGl2ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhY2tFeHBsb3Npb24ucGxheSgnV2hpdGVFeHBsb2RlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhY2tFeHBsb3Npb24ub24oJ2FuaW1hdGlvbmNvbXBsZXRlJywgKGFuaW1hdGlvbiwgZnJhbWUsIGdhbWVPYmplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2FtZU9iamVjdC5zZXRPRFN0YXRlKE9EU3RhdGUuTm90VmlzaWJsZUFjdGl2ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5vY2VhbmlubmVyYmFyLmRpc3BsYXlXaWR0aCA9IHRoaXMub2NlYW5pbm5lcmJhcmRlZmF1bHQgKiBkYXRhLm9jZWFuSGVhbHRoO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFByb2Nlc3NTdGFydENvb2xEb3duKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmJhc2Uuc2V0QWxwaGEoMC41KTtcclxuICAgICAgICB0aGlzLmd1bi5zZXRBbHBoYSgwLjUpO1xyXG4gICAgfVxyXG4gICAgUHJvY2Vzc0VuZENvb2xEb3duKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmJhc2Uuc2V0QWxwaGEoMS4wKTtcclxuICAgICAgICB0aGlzLmd1bi5zZXRBbHBoYSgxLjApO1xyXG4gICAgfVxyXG4gICAgUHJvY2Vzc1NldE1vbml0b3JFeHBsb3Npb24oZGF0YSkge1xyXG4gICAgICAgIGRhdGEubWlzc2lsZXMuZm9yRWFjaCgoZGV0YWlsKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBleHBsb3Npb24gPSB0aGlzLmV4cGxvc2lvblBvb2wuZ2V0KGRldGFpbC54ICogKHRoaXMuZGlzcGxheVdpZHRoIC8gZGV0YWlsLmRpc3BsYXlXaWR0aCksIGRldGFpbC55ICogKHRoaXMuZGlzcGxheUhlaWdodCAvIGRldGFpbC5kaXNwbGF5SGVpZ2h0KSkuc2V0RGVwdGgoMikuc2V0U2NhbGUoMC4zNSkucGxheSgnUmVkRXhwbG9kZScpO1xyXG4gICAgICAgICAgICBleHBsb3Npb24udGFnID0gZGF0YS5rZXk7XHJcbiAgICAgICAgICAgIGV4cGxvc2lvbi5pZCA9IGRldGFpbC5pZDtcclxuICAgICAgICAgICAgZXhwbG9zaW9uLnNldE9EU3RhdGUoT0RTdGF0ZS5WaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgZXhwbG9zaW9uLm9uKCdhbmltYXRpb25jb21wbGV0ZScsIChhbmltYXRpb24sIGZyYW1lLCBnYW1lT2JqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBnYW1lT2JqZWN0LnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzQWN0aXZlUmFkYXIoZGF0YSkge1xyXG4gICAgICAgIGlmIChkYXRhLmtleSAhPSB0aGlzLmFjdGl2ZVJhZGFyKSB7XHJcbiAgICAgICAgICAgIHRoaXMudHJhc2hQb29sLmdldENoaWxkcmVuKCkuZmlsdGVyKCh4KSA9PiB4LmFjdGl2ZSAmJiB4LnRhZyA9PSB0aGlzLmFjdGl2ZVJhZGFyKS5mb3JFYWNoKCh4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB4LnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMubWlzc2lsZVBvb2wuZ2V0Q2hpbGRyZW4oKS5maWx0ZXIoKHgpID0+IHguYWN0aXZlICYmIHgudGFnID09IHRoaXMuYWN0aXZlUmFkYXIpLmZvckVhY2goKHgpID0+IHtcclxuICAgICAgICAgICAgICAgIHguc2V0T0RTdGF0ZShPRFN0YXRlLk5vdFZpc2libGVBY3RpdmUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5leHBsb3Npb25Qb29sLmdldENoaWxkcmVuKCkuZmlsdGVyKCh4KSA9PiB4LmFjdGl2ZSAmJiB4LnRhZyA9PSB0aGlzLmFjdGl2ZVJhZGFyKS5mb3JFYWNoKCh4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB4LnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucGFydGljbGVNYW5hZ2VyLmVtaXR0ZXJzLmdldEFsbCgnbmFtZScsIHRoaXMuYWN0aXZlUmFkYXIpLmZvckVhY2goKGVtaXQpID0+IHtcclxuICAgICAgICAgICAgICAgIGVtaXQudmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5hY3RpdmVSYWRhciA9IGRhdGEua2V5O1xyXG4gICAgICAgICAgICB0aGlzLlNldEJhc2VBbmRHdW5Db2xvcigpO1xyXG4gICAgICAgICAgICB0aGlzLnN0YXRpY3R2LnNldEFjdGl2ZSh0cnVlKS5zZXRWaXNpYmxlKHRydWUpO1xyXG4gICAgICAgICAgICB0aGlzLnRpbWUuYWRkRXZlbnQoe1xyXG4gICAgICAgICAgICAgICAgZGVsYXk6IDEwMCxcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0aWN0di5zZXRBY3RpdmUoZmFsc2UpLnNldFZpc2libGUoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzTWlzc2lsZUluRmluYWxQb3NpdGlvbihkYXRhKSB7XHJcbiAgICAgICAgZGF0YS5taXNzaWxlcy5mb3JFYWNoKChkZXRhaWwpID0+IHtcclxuICAgICAgICAgICAgbGV0IGZvdW5kTWlzc2lsZSA9ICh0aGlzLm1pc3NpbGVQb29sLmdldENoaWxkcmVuKCkuZmluZCgoeCkgPT4geC5pZCA9PSBkZXRhaWwuaWQgJiYgeC50YWcgPT0gZGF0YS5rZXkpKTtcclxuICAgICAgICAgICAgaWYgKGZvdW5kTWlzc2lsZSkge1xyXG4gICAgICAgICAgICAgICAgZm91bmRNaXNzaWxlLnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgRGVzdHJveVRyYXNoKGRhdGEpIHtcclxuICAgICAgICBsZXQgZm91bmRUcmFzaDtcclxuICAgICAgICBkYXRhLnRyYXNoLmZvckVhY2goKGRldGFpbCkgPT4ge1xyXG4gICAgICAgICAgICBmb3VuZFRyYXNoID0gKHRoaXMudHJhc2hQb29sLmdldENoaWxkcmVuKCkuZmluZCgoeCkgPT4geC5pZCA9PSBkZXRhaWwuaWQgJiYgeC50YWcgPT0gZGF0YS5rZXkpKTtcclxuICAgICAgICAgICAgaWYgKGZvdW5kVHJhc2gpIHtcclxuICAgICAgICAgICAgICAgIGZvdW5kVHJhc2guc2V0T0RTdGF0ZShPRFN0YXRlLk5vdFZpc2libGVBY3RpdmUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGZvdW5kVHJhc2g7XHJcbiAgICB9XHJcbiAgICBQcm9jZXNzVHJhc2hEZXN0cm95ZWQoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuRGVzdHJveVRyYXNoKGRhdGEpO1xyXG4gICAgICAgIGlmIChPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbCA+IDApIHtcclxuICAgICAgICAgICAgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyUG9pbnRzICs9IDE7XHJcbiAgICAgICAgICAgIHRoaXMuc2NvcmVUZXh0LnNldFRleHQoT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5jdXJyUG9pbnRzLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFByb2Nlc3NUcmFzaEF0V2F0ZXIoZGF0YSkge1xyXG4gICAgICAgIGxldCBmb3VuZFRyYXNoID0gdGhpcy5EZXN0cm95VHJhc2goZGF0YSk7XHJcbiAgICAgICAgaWYgKGZvdW5kVHJhc2gpIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJ0aWNsZU1hbmFnZXIuZW1pdHRlcnMuZ2V0QWxsKCduYW1lJywgdGhpcy5hY3RpdmVSYWRhcikuZm9yRWFjaCgoZW1pdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgZW1pdC52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGVtaXQuZW1pdFBhcnRpY2xlQXQoZm91bmRUcmFzaC54LCB0aGlzLmRpc3BsYXlIZWlnaHQgLSAodGhpcy5kaXNwbGF5SGVpZ2h0ICogMC4wOTY1KSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5leHBvcnQgZGVmYXVsdCBPRE1vbml0b3I7XHJcbiIsImltcG9ydCBHYW1lRXZlbnQgZnJvbSBcIi4vLi4vTW9kZWxzL0dhbWVFdmVudFwiO1xyXG5jbGFzcyBPRFBhdXNlU2NlbmUgZXh0ZW5kcyBQaGFzZXIuU2NlbmUge1xyXG4gICAgY29uc3RydWN0b3Ioa2V5LCB4LCB5LCBkaXNwbGF5V2lkdGgsIGRpc3BsYXlIZWlnaHQpIHtcclxuICAgICAgICBzdXBlcih7XHJcbiAgICAgICAgICAgIGtleVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMua2V5ID0ga2V5O1xyXG4gICAgICAgIHRoaXMueCA9IHg7XHJcbiAgICAgICAgdGhpcy55ID0geTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlXaWR0aCA9IGRpc3BsYXlXaWR0aDtcclxuICAgICAgICB0aGlzLmRpc3BsYXlIZWlnaHQgPSBkaXNwbGF5SGVpZ2h0O1xyXG4gICAgfVxyXG4gICAgcHJlbG9hZCgpIHtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLmNhbWVyYXMubWFpbi5zZXRWaWV3cG9ydCh0aGlzLngsIHRoaXMueSwgdGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5hZGQucGF1c2VwYW5lbCh0aGlzLmRpc3BsYXlXaWR0aCAvIDIuMCwgdGhpcy5kaXNwbGF5SGVpZ2h0IC8gMi4wKS5zZXREaXNwbGF5U2l6ZSh0aGlzLmRpc3BsYXlXaWR0aCwgdGhpcy5kaXNwbGF5SGVpZ2h0KTtcclxuICAgICAgICBsZXQgcGxheUJ1dHRvbiA9IHRoaXMuYWRkLnBsYXlidXR0b24odGhpcy5kaXNwbGF5V2lkdGggLyAyLjAsICh0aGlzLmRpc3BsYXlIZWlnaHQgLyA0LjApIC0gMjApLnNldEludGVyYWN0aXZlKCkuc2V0RGlzcGxheVNpemUoMTE1LCAxMjQpO1xyXG4gICAgICAgIHBsYXlCdXR0b24ub24oJ3BvaW50ZXJkb3duJywgKHBvaW50ZXIpID0+IHtcclxuICAgICAgICAgICAgbGV0IGdlID0gbmV3IEdhbWVFdmVudCgnR2FtZVBhdXNlJywgZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnR2FtZVBhdXNlJywgZ2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxldCByZXBsYXlCdXR0b24gPSB0aGlzLmFkZC5yZXBsYXlidXR0b24odGhpcy5kaXNwbGF5V2lkdGggLyAyLjAsIHRoaXMuZGlzcGxheUhlaWdodCAvIDIuMCkuc2V0SW50ZXJhY3RpdmUoKS5zZXREaXNwbGF5U2l6ZSgxMTUsIDEyNCk7XHJcbiAgICAgICAgcmVwbGF5QnV0dG9uLm9uKCdwb2ludGVyZG93bicsIChwb2ludGVyKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBnZSA9IG5ldyBHYW1lRXZlbnQoJ0dhbWVQYXVzZScsIGZhbHNlLCB0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ0dhbWVQYXVzZScsIGdlKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmFkZC5ob21lYnV0dG9uKHRoaXMuZGlzcGxheVdpZHRoIC8gMi4wLCByZXBsYXlCdXR0b24ueSArIChyZXBsYXlCdXR0b24ueSAtIHBsYXlCdXR0b24ueSkpLnNldEludGVyYWN0aXZlKCkuc2V0RGlzcGxheVNpemUoMTE1LCAxMjQpLm9uKCdwb2ludGVyZG93bicsIChwb2ludGVyKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBnZSA9IG5ldyBHYW1lRXZlbnQoJ0dhbWVQYXVzZScsIGZhbHNlLCBmYWxzZSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdHYW1lUGF1c2UnLCBnZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgT0RQYXVzZVNjZW5lO1xyXG4iLCJpbXBvcnQgT0RNaXNzaWxlTWFrZXIgZnJvbSAnLi8uLi9HYW1lT2JqZWN0cy9PRE1pc3NpbGVNYWtlcic7XHJcbmltcG9ydCBPRFN0YXRlIGZyb20gJy4vLi4vRW51bXMvT0RTdGF0ZSc7XHJcbmltcG9ydCBPRFRyYXNoTWFya2VyIGZyb20gJy4vLi4vR2FtZU9iamVjdHMvT0RUcmFzaE1hcmtlcic7XHJcbmltcG9ydCBPREV4cGxvc2lvbk1hcmtlciBmcm9tICcuLy4uL0dhbWVPYmplY3RzL09ERXhwbG9zaW9uTWFya2VyJztcclxuaW1wb3J0IE1vbml0b3JDbGlja0V2ZW50IGZyb20gJy4vLi4vTW9kZWxzL01vbml0b3JDbGlja0V2ZW50JztcclxuaW1wb3J0IE9ER2FtZUluZm8gZnJvbSAnLi8uLi9Nb2RlbHMvT0RHYW1lSW5mbyc7XHJcbmltcG9ydCBSYWRhckV2ZW50IGZyb20gJy4vLi4vTW9kZWxzL1JhZGFyRXZlbnQnO1xyXG5pbXBvcnQgUmFkYXJEZXRhaWxFdmVudCBmcm9tICcuLy4uL01vZGVscy9SYWRhckRldGFpbEV2ZW50JztcclxuaW1wb3J0IEdhbWVFdmVudCBmcm9tICcuLy4uL01vZGVscy9HYW1lRXZlbnQnO1xyXG5jbGFzcyBPRFJhZGFyIGV4dGVuZHMgUGhhc2VyLlNjZW5lIHtcclxuICAgIGNvbnN0cnVjdG9yKGtleSwgeFBvcywgeVBvcywgZGlzcGxheVdpZHRoLCBkaXNwbGF5SGVpZ2h0LCBpc0hpZ2hsaWdodGVkLCBmYWRlVGltZSkge1xyXG4gICAgICAgIHN1cGVyKHtcclxuICAgICAgICAgICAga2V5XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5rZXkgPSBrZXk7XHJcbiAgICAgICAgdGhpcy54UG9zID0geFBvcztcclxuICAgICAgICB0aGlzLnlQb3MgPSB5UG9zO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheVdpZHRoID0gZGlzcGxheVdpZHRoO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUhlaWdodCA9IGRpc3BsYXlIZWlnaHQ7XHJcbiAgICAgICAgdGhpcy5pc0hpZ2hsaWdodGVkID0gaXNIaWdobGlnaHRlZDtcclxuICAgICAgICB0aGlzLnJhZGFyT24gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZmFkZVRpbWUgPSBmYWRlVGltZTtcclxuICAgIH1cclxuICAgIGluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyTGV2ZWwgPSBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbDtcclxuICAgICAgICB0aGlzLnJhZGFySW5kZXggPSBwYXJzZUludCh0aGlzLmtleVt0aGlzLmtleS5sZW5ndGggLSAxXSkgLSAxO1xyXG4gICAgICAgIHRoaXMuY29vbERvd25UaW1lID0gT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHNbdGhpcy5jdXJyTGV2ZWxdLmNvb2xEb3duVGltZXI7XHJcbiAgICAgICAgdGhpcy5vY2VhbkhlYWx0aCA9IE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzW3RoaXMuY3VyckxldmVsXS5yYWRhcnNbdGhpcy5yYWRhckluZGV4XS5vY2Vhbk1heERhbWFnZTtcclxuICAgICAgICB0aGlzLm9jZWFuSGVhbHRoTWF4ID0gT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHNbdGhpcy5jdXJyTGV2ZWxdLnJhZGFyc1t0aGlzLnJhZGFySW5kZXhdLm9jZWFuTWF4RGFtYWdlO1xyXG4gICAgICAgIHRoaXMudHJhc2hUaW1lciA9IE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzW3RoaXMuY3VyckxldmVsXS5yYWRhcnNbdGhpcy5yYWRhckluZGV4XS50cmFzaFNwYXduVGltZTtcclxuICAgICAgICB0aGlzLnRyYXNoVG9DbGVhciA9IE9ER2FtZUluZm8uaW5zdGFuY2UubGV2ZWxzW3RoaXMuY3VyckxldmVsXS5yYWRhcnNbdGhpcy5yYWRhckluZGV4XS50cmFzaFRvQ2xlYXI7XHJcbiAgICAgICAgdGhpcy50cmFzaE1pblNwZWVkID0gT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHNbdGhpcy5jdXJyTGV2ZWxdLnJhZGFyc1t0aGlzLnJhZGFySW5kZXhdLnRyYXNoTWluU3BlZWQ7XHJcbiAgICAgICAgdGhpcy50cmFzaE1heFNwZWVkID0gT0RHYW1lSW5mby5pbnN0YW5jZS5sZXZlbHNbdGhpcy5jdXJyTGV2ZWxdLnJhZGFyc1t0aGlzLnJhZGFySW5kZXhdLnRyYXNoTWF4U3BlZWQ7XHJcbiAgICAgICAgdGhpcy5taXNzaWxlU3BlZWQgPSBPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1t0aGlzLmN1cnJMZXZlbF0ubWlzc2lsZVNwZWVkO1xyXG4gICAgICAgIHRoaXMuZmxhc2hUaW1lciA9IHVuZGVmaW5lZDtcclxuICAgICAgICB0aGlzLmNvb2xEb3duVGltZXIgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgdGhpcy5jYW5GaXJlID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIHByZWxvYWQoKSB7XHJcbiAgICAgICAgdGhpcy5leHBsb3Npb25NYXJrZXJQb29sID0gdGhpcy5hZGQuZ3JvdXAoe1xyXG4gICAgICAgICAgICBtYXhTaXplOiAxMCxcclxuICAgICAgICAgICAgZGVmYXVsdEtleTogJ09ESW1hZ2VzJyxcclxuICAgICAgICAgICAgZGVmYXVsdEZyYW1lOiAnZXhwbG9zaW9ubWFya2VyLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogT0RFeHBsb3Npb25NYXJrZXIsXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiB0cnVlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5taXNzaWxlTWFya2VyUG9vbCA9IHRoaXMuYWRkLmdyb3VwKHtcclxuICAgICAgICAgICAgbWF4U2l6ZTogMTAsXHJcbiAgICAgICAgICAgIGRlZmF1bHRLZXk6ICdPREltYWdlcycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRGcmFtZTogJ21pc3NpbGVtYXJrZXIucG5nJyxcclxuICAgICAgICAgICAgY2xhc3NUeXBlOiBPRE1pc3NpbGVNYWtlcixcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6IChtaXNzaWxlKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJlbW92ZUNhbGxiYWNrOiAobWlzc2lsZSkgPT4geyB9LFxyXG4gICAgICAgICAgICBydW5DaGlsZFVwZGF0ZTogZmFsc2VcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnRyYXNoTWFya2VyUG9vbCA9IHRoaXMuYWRkLmdyb3VwKHtcclxuICAgICAgICAgICAgbWF4U2l6ZTogMjAsXHJcbiAgICAgICAgICAgIGRlZmF1bHRLZXk6ICdPREltYWdlcycsXHJcbiAgICAgICAgICAgIGRlZmF1bHRGcmFtZTogJ3RyYXNobWFya2VyLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogT0RUcmFzaE1hcmtlcixcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY2xvdWRQb29sID0gdGhpcy5hZGQuZ3JvdXAoe1xyXG4gICAgICAgICAgICBtYXhTaXplOiAxMCxcclxuICAgICAgICAgICAgZGVmYXVsdEtleTogJ09ESW1hZ2VzJyxcclxuICAgICAgICAgICAgZGVmYXVsdEZyYW1lOiAnY2xvdWQxLnBuZycsXHJcbiAgICAgICAgICAgIGNsYXNzVHlwZTogUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSxcclxuICAgICAgICAgICAgY3JlYXRlQ2FsbGJhY2s6ICh0cmFzaCkgPT4geyB9LFxyXG4gICAgICAgICAgICByZW1vdmVDYWxsYmFjazogKHRyYXNoKSA9PiB7IH0sXHJcbiAgICAgICAgICAgIHJ1bkNoaWxkVXBkYXRlOiBmYWxzZVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY2FtZXJhcy5tYWluLmZhZGVJbih0aGlzLmZhZGVUaW1lLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5yZWQsIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuZmFkZUNvbG9yLmdyZWVuLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5ibHVlKTtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLmNhbWVyYXMubWFpbi5zZXRWaWV3cG9ydCh0aGlzLnhQb3MsIHRoaXMueVBvcywgdGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5hZGQuc2t5KCh0aGlzLmRpc3BsYXlXaWR0aCAvIDIuMCksICh0aGlzLmRpc3BsYXlIZWlnaHQgLyAyLjApKS5zZXREaXNwbGF5U2l6ZSh0aGlzLmRpc3BsYXlXaWR0aCwgdGhpcy5kaXNwbGF5SGVpZ2h0KTtcclxuICAgICAgICB0aGlzLmNsb3VkUG9vbC5hZGRNdWx0aXBsZShbXHJcbiAgICAgICAgICAgIChuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLCA5Mi4zMSwgMTIzLjA4LCAnT0RJbWFnZXMnLCAnY2xvdWQxLnBuZycpKS5zZXRTY2FsZSgwLjE1LCAwLjEwKSxcclxuICAgICAgICAgICAgKG5ldyBQaGFzZXIuR2FtZU9iamVjdHMuU3ByaXRlKHRoaXMsIDQ5LjIzLCAzNi45MiwgJ09ESW1hZ2VzJywgJ2Nsb3VkMS5wbmcnKSkuc2V0U2NhbGUoMC4xMCwgMC4xNSksXHJcbiAgICAgICAgICAgIChuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLCAyNC42MiwgNzMuODUsICdPREltYWdlcycsICdjbG91ZDEucG5nJykpLnNldFNjYWxlKDAuMDUsIDAuMDUpLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgMTIzLjA4LCAzNi45MiwgJ09ESW1hZ2VzJywgJ2Nsb3VkMS5wbmcnKSkuc2V0U2NhbGUoMC4xMCwgMC4wNSksXHJcbiAgICAgICAgICAgIChuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLCA2MS41NCwgNjEuNTQsICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMTUsIDAuMTApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgMTIzLjA4LCA3My44NSwgJ09ESW1hZ2VzJywgJ2Nsb3VkMi5wbmcnKSkuc2V0U2NhbGUoMC4yNSwgMC4xMCksXHJcbiAgICAgICAgICAgIChuZXcgUGhhc2VyLkdhbWVPYmplY3RzLlNwcml0ZSh0aGlzLCA0My4wOCwgOTguNDYsICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMDUsIDAuMTApLFxyXG4gICAgICAgICAgICAobmV3IFBoYXNlci5HYW1lT2JqZWN0cy5TcHJpdGUodGhpcywgMTIzLjA4LCAxMDQuNjIsICdPREltYWdlcycsICdjbG91ZDIucG5nJykpLnNldFNjYWxlKDAuMDUsIDAuMDUpXHJcbiAgICAgICAgXSwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5hZGQub2NlYW4oKHRoaXMuZGlzcGxheVdpZHRoIC8gMi4wKSwgdGhpcy5kaXNwbGF5SGVpZ2h0IC0gMjApLnNldERpc3BsYXlTaXplKHRoaXMuZGlzcGxheVdpZHRoIC0gMjAsIDI1KS5zZXREZXB0aCgxKTtcclxuICAgICAgICB0aGlzLm1vbml0b3IgPSB0aGlzLmFkZC50dm1vbml0b3JlbXB0eSgwLCAwKTtcclxuICAgICAgICB0aGlzLm1vbml0b3Iuc2V0RGlzcGxheVNpemUodGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCkuc2V0RGVwdGgoMikuc2V0SW50ZXJhY3RpdmUoKS5wbGF5KCdFbXB0eVJhZGFycycsIGZhbHNlLCAodGhpcy5pc0hpZ2hsaWdodGVkKSA/IDEgOiAwKS5vbigncG9pbnRlcmRvd24nLCAocG9pbnRlcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yYWRhck9uKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnQWN0aXZlUmFkYXInLCBuZXcgUmFkYXJFdmVudCh0aGlzLmtleSkpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ1NldFJhZGFyQm9yZGVyJywgbmV3IE1vbml0b3JDbGlja0V2ZW50KHRoaXMua2V5KSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN0YXRpY3R2ID0gdGhpcy5hZGQuc3RhdGljdHYoMCwgMCkuc2V0RGlzcGxheVNpemUodGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCkuc2V0RGVwdGgoMykuc2V0QWN0aXZlKGZhbHNlKS5zZXRWaXNpYmxlKGZhbHNlKTtcclxuICAgICAgICB0aGlzLm9jZWFub3V0dGVyYmFyID0gdGhpcy5hZGQub2NlYW5vdXRlcmJhcigodGhpcy5kaXNwbGF5V2lkdGggLyAyLjApIC0gMzUsIHRoaXMuZGlzcGxheUhlaWdodCAtIDE4KS5zZXREaXNwbGF5U2l6ZSg3MC44NCwgMTAuMDgpLnNldERlcHRoKDEpO1xyXG4gICAgICAgICh0aGlzLmFkZCkub2NlYW5taWRkbGViYXIodGhpcy5vY2Vhbm91dHRlcmJhci54LCB0aGlzLm9jZWFub3V0dGVyYmFyLnkpLnNldERpc3BsYXlTaXplKHRoaXMub2NlYW5vdXR0ZXJiYXIuZGlzcGxheVdpZHRoLCB0aGlzLm9jZWFub3V0dGVyYmFyLmRpc3BsYXlIZWlnaHQpLnNldERlcHRoKDEpO1xyXG4gICAgICAgIHRoaXMub2NlYW5pbm5lcmJhciA9IHRoaXMuYWRkLm9jZWFuaW5uZXJiYXIodGhpcy5vY2Vhbm91dHRlcmJhci54ICsgMiwgdGhpcy5vY2Vhbm91dHRlcmJhci55ICsgMikuc2V0RGlzcGxheVNpemUodGhpcy5vY2Vhbm91dHRlcmJhci5kaXNwbGF5V2lkdGggLSA0LCB0aGlzLm9jZWFub3V0dGVyYmFyLmRpc3BsYXlIZWlnaHQgLSA0KS5zZXREZXB0aCgxKTtcclxuICAgICAgICB0aGlzLmhlYWx0aFJhdGlvID0gdGhpcy5vY2VhbkhlYWx0aCAvIHRoaXMub2NlYW5IZWFsdGhNYXg7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5ldmVudHMub24oJ2NoYW5nZWRhdGEnLCAocGFyZW50LCBrZXksIGRhdGEpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucmFkYXJPbikge1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIHRoaXMua2V5OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZU1pc3NpbGUoa2V5LCBkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnU2V0UmFkYXJCb3JkZXInOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vbml0b3IucGxheSgnRW1wdHlSYWRhcnMnLCBmYWxzZSwgKGRhdGEua2V5ID09IHRoaXMua2V5KSA/IDEgOiAwKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnVHJhc2hEZXN0cm95ZWQnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyTGV2ZWwgPiAwICYmIGRhdGEua2V5ID09IHRoaXMua2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRyYXNoVG9DbGVhciAtPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudHJhc2hUb0NsZWFyID09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLlNldFJhZGFyT24oZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdHYW1lRXZlbnRSYWRhcldvbicsIG5ldyBHYW1lRXZlbnQodGhpcy5rZXkpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgU3RhcnRUaW1lcigpIHtcclxuICAgICAgICB0aGlzLnRpbWUuYWRkRXZlbnQoe1xyXG4gICAgICAgICAgICBkZWxheTogdGhpcy50cmFzaFRpbWVyLFxyXG4gICAgICAgICAgICBsb29wOiB0cnVlLFxyXG4gICAgICAgICAgICBjYWxsYmFjazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucmFkYXJPbikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlVHJhc2goKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgU2V0UmFkYXJPbihpc09uKSB7XHJcbiAgICAgICAgdGhpcy5yYWRhck9uID0gaXNPbjtcclxuICAgICAgICBpZiAodGhpcy5yYWRhck9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGljdHYuc2V0QWN0aXZlKGZhbHNlKS5zZXRWaXNpYmxlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGljdHYuc2V0QWN0aXZlKHRydWUpLnNldFZpc2libGUodHJ1ZSkucGxheSgnU3RhdGljVHYnLCBmYWxzZSwgUGhhc2VyLk1hdGguUk5ELmludGVnZXJJblJhbmdlKDAsIDYpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBHZXRSYWRhck9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJhZGFyT247XHJcbiAgICB9XHJcbiAgICBHZXRUcmFzaE1hcmtlclBvb2woKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhc2hNYXJrZXJQb29sO1xyXG4gICAgfVxyXG4gICAgY3JlYXRlVHJhc2goKSB7XHJcbiAgICAgICAgbGV0IG1hcmtlciA9IHRoaXMudHJhc2hNYXJrZXJQb29sLmdldCgwLCAwKS5zZXRTY2FsZSgwLjM1KTtcclxuICAgICAgICBtYXJrZXIuU2V0SWQoKTtcclxuICAgICAgICBtYXJrZXIudGFnID0gdGhpcy5rZXk7XHJcbiAgICAgICAgbWFya2VyLnNwZWVkID0gUGhhc2VyLk1hdGguUk5ELnJlYWxJblJhbmdlKHRoaXMudHJhc2hNaW5TcGVlZCwgdGhpcy50cmFzaE1heFNwZWVkKTtcclxuICAgICAgICBtYXJrZXIuc2V0WFkoUGhhc2VyLk1hdGguUk5ELnJlYWxJblJhbmdlKDE1LCB0aGlzLmRpc3BsYXlXaWR0aCAtIDE1KSwgMTAsIFBoYXNlci5NYXRoLlJORC5yZWFsSW5SYW5nZSgxNSwgdGhpcy5kaXNwbGF5V2lkdGggLSAxNSksIHRoaXMuZGlzcGxheUhlaWdodCAtIDMwKTtcclxuICAgICAgICBtYXJrZXIuc2V0T0RTdGF0ZShPRFN0YXRlLlZpc2libGVBY3RpdmUpO1xyXG4gICAgfVxyXG4gICAgUnVuQ29vbERvd25UaW1lcigpIHtcclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnU3RhcnRDb29sRG93bicsIG5ldyBSYWRhckV2ZW50KHRoaXMua2V5KSk7XHJcbiAgICAgICAgdGhpcy5jb29sRG93blRpbWVyID0gdGhpcy50aW1lLmFkZEV2ZW50KHtcclxuICAgICAgICAgICAgZGVsYXk6IHRoaXMuY29vbERvd25UaW1lLFxyXG4gICAgICAgICAgICBjYWxsYmFjazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ0VuZENvb2xEb3duJywgbmV3IFJhZGFyRXZlbnQodGhpcy5rZXkpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FuRmlyZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvb2xEb3duVGltZXIuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBjcmVhdGVNaXNzaWxlKHJhZGFyTmFtZSwgZXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5jYW5GaXJlIHx8IHRoaXMuY3VyckxldmVsID09IDApIHtcclxuICAgICAgICAgICAgbGV0IG1hcmtlciA9IHRoaXMubWlzc2lsZU1hcmtlclBvb2wuZ2V0KDAsIDApLnNldFNjYWxlKDAuMzUpO1xyXG4gICAgICAgICAgICBtYXJrZXIuU2V0SWQoKTtcclxuICAgICAgICAgICAgbWFya2VyLnRhZyA9IHRoaXMua2V5O1xyXG4gICAgICAgICAgICBtYXJrZXIuc3BlZWQgPSB0aGlzLm1pc3NpbGVTcGVlZDtcclxuICAgICAgICAgICAgbWFya2VyLnNldFhZKGV2ZW50LmluaXRpYWxYICogKHRoaXMuZGlzcGxheVdpZHRoIC8gZXZlbnQubW9uaXRvcldpZHRoKSwgZXZlbnQuaW5pdGlhbFkgKiAodGhpcy5kaXNwbGF5SGVpZ2h0IC8gZXZlbnQubW9uaXRvckhlaWdodCksIGV2ZW50LmZpbmFsWCAqICh0aGlzLmRpc3BsYXlXaWR0aCAvIGV2ZW50Lm1vbml0b3JXaWR0aCksIGV2ZW50LmZpbmFsWSAqICh0aGlzLmRpc3BsYXlIZWlnaHQgLyBldmVudC5tb25pdG9ySGVpZ2h0KSk7XHJcbiAgICAgICAgICAgIG1hcmtlci5zZXRPRFN0YXRlKE9EU3RhdGUuVmlzaWJsZUFjdGl2ZSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJMZXZlbCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FuRmlyZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5SdW5Db29sRG93blRpbWVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBDcmVhdGVFeHBsb3Npb24oeCwgeSkge1xyXG4gICAgICAgIGxldCBtYXJrZXIgPSB0aGlzLmV4cGxvc2lvbk1hcmtlclBvb2wuZ2V0KHgsIHkpLnNldFNjYWxlKDAuMjUpO1xyXG4gICAgICAgIG1hcmtlci5TZXRJZCgpO1xyXG4gICAgICAgIG1hcmtlci50YWcgPSB0aGlzLmtleTtcclxuICAgICAgICBtYXJrZXIuc2V0T0RTdGF0ZShPRFN0YXRlLlZpc2libGVBY3RpdmUpO1xyXG4gICAgICAgIGxldCBldmVudCA9IG5ldyBSYWRhckV2ZW50KHRoaXMua2V5LCB0aGlzLnRyYXNoVG9DbGVhciwgdGhpcy5oZWFsdGhSYXRpbyk7XHJcbiAgICAgICAgZXZlbnQubWlzc2lsZXMucHVzaChuZXcgUmFkYXJEZXRhaWxFdmVudChtYXJrZXIuaWQsIG1hcmtlci54LCBtYXJrZXIueSwgdGhpcy5kaXNwbGF5V2lkdGgsIHRoaXMuZGlzcGxheUhlaWdodCkpO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdTZXRNb25pdG9yRXhwbG9zaW9uJywgZXZlbnQpO1xyXG4gICAgICAgIHRoaXMudGltZS5hZGRFdmVudCh7XHJcbiAgICAgICAgICAgIGRlbGF5OiAxOTAwLFxyXG4gICAgICAgICAgICBsb29wOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVwZWF0OiAwLFxyXG4gICAgICAgICAgICBjYWxsYmFjazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbWFya2VyLnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdXBkYXRlKHRpbWUsIGR0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMucmFkYXJPbikge1xyXG4gICAgICAgICAgICBsZXQgcmFkYXJVcGRhdGVJbmZvID0gbmV3IFJhZGFyRXZlbnQodGhpcy5rZXksIHRoaXMudHJhc2hUb0NsZWFyLCB0aGlzLmhlYWx0aFJhdGlvKTtcclxuICAgICAgICAgICAgUGhhc2VyLkFjdGlvbnMuQ2FsbCh0aGlzLnRyYXNoTWFya2VyUG9vbC5nZXRDaGlsZHJlbigpLmZpbHRlcigoeCkgPT4geC5hY3RpdmUgPT0gdHJ1ZSksIChtYXJrZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIG1hcmtlci54ICs9IChtYXJrZXIuZGlyZWN0aW9uLnggKiBtYXJrZXIuc3BlZWQgKiAoZHQgLyAxMDAwKSk7XHJcbiAgICAgICAgICAgICAgICBtYXJrZXIueSArPSAobWFya2VyLmRpcmVjdGlvbi55ICogbWFya2VyLnNwZWVkICogKGR0IC8gMTAwMCkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1hcmtlci55ID49IG1hcmtlci5maW5hbFkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlByb2Nlc3NUcmFzaEF0V2F0ZXIobWFya2VyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJhZGFyVXBkYXRlSW5mby50cmFzaC5wdXNoKG5ldyBSYWRhckRldGFpbEV2ZW50KG1hcmtlci5pZCwgbWFya2VyLngsIG1hcmtlci55LCB0aGlzLmRpc3BsYXlXaWR0aCwgdGhpcy5kaXNwbGF5SGVpZ2h0KSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIHRoaXMpO1xyXG4gICAgICAgICAgICBQaGFzZXIuQWN0aW9ucy5DYWxsKHRoaXMubWlzc2lsZU1hcmtlclBvb2wuZ2V0Q2hpbGRyZW4oKS5maWx0ZXIoKHgpID0+IHguYWN0aXZlID09IHRydWUpLCAobWFya2VyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBtYXJrZXIueCArPSAobWFya2VyLmRpcmVjdGlvbi54ICogbWFya2VyLnNwZWVkICogKGR0IC8gMTAwMCkpO1xyXG4gICAgICAgICAgICAgICAgbWFya2VyLnkgKz0gKG1hcmtlci5kaXJlY3Rpb24ueSAqIG1hcmtlci5zcGVlZCAqIChkdCAvIDEwMDApKTtcclxuICAgICAgICAgICAgICAgIGlmIChtYXJrZXIueSA8PSBtYXJrZXIuZmluYWxZKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Qcm9jZXNzTWlzc2lsZUluRmluYWxQb3NpdGlvbihtYXJrZXIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmFkYXJVcGRhdGVJbmZvLm1pc3NpbGVzLnB1c2gobmV3IFJhZGFyRGV0YWlsRXZlbnQobWFya2VyLmlkLCBtYXJrZXIueCwgbWFya2VyLnksIHRoaXMuZGlzcGxheVdpZHRoLCB0aGlzLmRpc3BsYXlIZWlnaHQpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgdGhpcyk7XHJcbiAgICAgICAgICAgIGxldCBjb3VudGVyID0gMDtcclxuICAgICAgICAgICAgUGhhc2VyLkFjdGlvbnMuQ2FsbCh0aGlzLmNsb3VkUG9vbC5nZXRDaGlsZHJlbigpLCAoY2xvdWQpID0+IHtcclxuICAgICAgICAgICAgICAgIGNsb3VkLnggKz0gKCgoZHQgLyAxMDAwKSAqIDQuOTIpICsgKDAuMDQgKiBjb3VudGVyKSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2xvdWQueCAtIChjbG91ZC5kaXNwbGF5V2lkdGggLyAyLjApID4gdGhpcy5kaXNwbGF5V2lkdGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBjbG91ZC54ID0gLWNsb3VkLmRpc3BsYXlXaWR0aCAvIDIuMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGNvdW50ZXIgKz0gMTtcclxuICAgICAgICAgICAgfSwgdGhpcyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdSYWRhclVwZGF0ZUN5Y2xlJyArIHRoaXMua2V5W3RoaXMua2V5Lmxlbmd0aCAtIDFdLCByYWRhclVwZGF0ZUluZm8pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFByb2Nlc3NUcmFzaEF0V2F0ZXIobWFya2VyKSB7XHJcbiAgICAgICAgbGV0IGV2ZW50ID0gbmV3IFJhZGFyRXZlbnQodGhpcy5rZXksIHRoaXMudHJhc2hUb0NsZWFyLCB0aGlzLmhlYWx0aFJhdGlvKTtcclxuICAgICAgICBldmVudC50cmFzaC5wdXNoKG5ldyBSYWRhckRldGFpbEV2ZW50KG1hcmtlci5pZCkpO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdUcmFzaEF0V2F0ZXInLCBldmVudCk7XHJcbiAgICAgICAgbWFya2VyLnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICBpZiAodGhpcy5jdXJyTGV2ZWwgPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMub2NlYW5IZWFsdGggLT0gMTtcclxuICAgICAgICAgICAgdGhpcy5oZWFsdGhSYXRpbyA9IHRoaXMub2NlYW5IZWFsdGggLyB0aGlzLm9jZWFuSGVhbHRoTWF4O1xyXG4gICAgICAgICAgICB0aGlzLm9jZWFuaW5uZXJiYXIuZGlzcGxheVdpZHRoID0gKHRoaXMub2NlYW5vdXR0ZXJiYXIuZGlzcGxheVdpZHRoIC0gNCkgKiB0aGlzLmhlYWx0aFJhdGlvO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5vY2VhbkhlYWx0aCA9PSAyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZsYXNoVGltZXIgPSB0aGlzLnRpbWUuYWRkRXZlbnQoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRlbGF5OiAxMDAwLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlcGVhdDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW1lcmFzLm1haW4uZmxhc2goMjAwLCAyNTUsIDAsIDAsIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLm9jZWFuSGVhbHRoID09IDApIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmZsYXNoVGltZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZsYXNoVGltZXIuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmxhc2hUaW1lciA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuU2V0UmFkYXJPbihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldCgnR2FtZUV2ZW50UmFkYXJMb3N0JywgbmV3IEdhbWVFdmVudCh0aGlzLmtleSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgUHJvY2Vzc01pc3NpbGVJbkZpbmFsUG9zaXRpb24obWFya2VyKSB7XHJcbiAgICAgICAgbGV0IGV2ZW50ID0gbmV3IFJhZGFyRXZlbnQodGhpcy5rZXkpO1xyXG4gICAgICAgIGV2ZW50Lm1pc3NpbGVzLnB1c2gobmV3IFJhZGFyRGV0YWlsRXZlbnQobWFya2VyLmlkKSk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5zZXQoJ01pc3NpbGVJbkZpbmFsUG9zaXRpb24nLCBldmVudCk7XHJcbiAgICAgICAgbWFya2VyLnNldE9EU3RhdGUoT0RTdGF0ZS5Ob3RWaXNpYmxlQWN0aXZlKTtcclxuICAgICAgICB0aGlzLkNyZWF0ZUV4cGxvc2lvbihtYXJrZXIueCwgbWFya2VyLnkpO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EUmFkYXI7XHJcbiIsImltcG9ydCBPRE1vbml0b3IgZnJvbSAnLi9PRE1vbml0b3InO1xyXG5pbXBvcnQgT0RSYWRhciBmcm9tICcuL09EUmFkYXInO1xyXG5pbXBvcnQgT0RHYW1lSW5mbyBmcm9tICcuLy4uL01vZGVscy9PREdhbWVJbmZvJztcclxuaW1wb3J0IFJhZGFyRXZlbnQgZnJvbSAnLi8uLi9Nb2RlbHMvUmFkYXJFdmVudCc7XHJcbmNsYXNzIE9EVGl0bGVTY2VuZSBleHRlbmRzIFBoYXNlci5TY2VuZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcih7XHJcbiAgICAgICAgICAgIGtleTogJ1RpdGxlU2NyZWVuJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaW5pdCgpIHtcclxuICAgICAgICB0aGlzLnBsYXlCdXR0b24gPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgdGhpcy5tb25pdG9yID0gdW5kZWZpbmVkO1xyXG4gICAgICAgIHRoaXMuZmFkZVRpbWUgPSAxMDAwO1xyXG4gICAgICAgIE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuY3VyckxldmVsID0gMDtcclxuICAgIH1cclxuICAgIGNyZWF0ZSgpIHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDM7IGkrKykge1xyXG4gICAgICAgICAgICBsZXQga2V5ID0gJ1JhZGFyJyArIChpICsgMSkudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgbGV0IGN1cnJMZXZlbCA9IE9ER2FtZUluZm8uaW5zdGFuY2Uud29ybGQuY3VyckxldmVsO1xyXG4gICAgICAgICAgICBsZXQgcmFkYXIgPSBuZXcgT0RSYWRhcihrZXksIDAgKyAoMTYwICogaSksIDUwMCwgMTYwLCAyMTIuOCwgKGkgPT0gMCkgPyB0cnVlIDogZmFsc2UsIHRoaXMuZmFkZVRpbWUpO1xyXG4gICAgICAgICAgICB0aGlzLnNjZW5lLmFkZChrZXksIHJhZGFyLCB0cnVlKTtcclxuICAgICAgICAgICAgcmFkYXIuU2V0UmFkYXJPbihPREdhbWVJbmZvLmluc3RhbmNlLmxldmVsc1swXS5yYWRhcnNbaV0uYWN0aXZlKTtcclxuICAgICAgICAgICAgcmFkYXIuU3RhcnRUaW1lcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm1vbml0b3IgPSBuZXcgT0RNb25pdG9yKCdNb25pdG9yMScsIDIwLCA4MCwgMjYwLCAzNDUuOCwgdGhpcy5mYWRlVGltZSk7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5hZGQoJ01vbml0b3IxJywgdGhpcy5tb25pdG9yLCB0cnVlKTtcclxuICAgICAgICB0aGlzLm1vbml0b3IuU2V0SWdub3JlSW5wdXRPbih0cnVlKTtcclxuICAgICAgICB0aGlzLnBsYXlCdXR0b24gPSB0aGlzLmFkZC5wbGF5YnV0dG9uKDc4LCA3ODcpO1xyXG4gICAgICAgIGxldCBsZXZlbEJ1dHRvbiA9IHRoaXMuYWRkLnNwcml0ZSh0aGlzLmdhbWUuY29uZmlnLndpZHRoIC8gMi4wLCA3ODcsICdPREltYWdlcycsICdsZXZlbHNidXR0b24ucG5nJyk7XHJcbiAgICAgICAgbGV0IHNldHRpbmdzQnV0dG9uID0gdGhpcy5hZGQuc3ByaXRlKDQwMCwgNzg3LCAnT0RJbWFnZXMnLCAnc2V0dGluZ3NidXR0b24ucG5nJyk7XHJcbiAgICAgICAgbGV0IG1vbml0b3JTd2l0Y2hlcyA9IHRoaXMuYWRkLnNwcml0ZSgxMDAsIDQ0MCwgJ09ESW1hZ2VzJywgJ21vbml0b3Jzd2l0Y2hlcy5wbmcnKTtcclxuICAgICAgICBsZXQgbW9uaXRvckxpZ2h0cyA9IHRoaXMuYWRkLnNwcml0ZSgyMjAsIDQ0MCwgJ09ESW1hZ2VzJywgJ21pbnRvcmxpZ2h0cy5wbmcnKS5zZXRBbmdsZSg5MCk7XHJcbiAgICAgICAgbGV0IG1vbml0b3JCdXR0b25zID0gdGhpcy5hZGQuc3ByaXRlKDE1MCwgNDcwLCAnT0RJbWFnZXMnLCAnbW9uaXRvcmJ1dHRvbnMucG5nJykuc2V0U2NhbGUoMC43NSk7XHJcbiAgICAgICAgbGV0IG1vbml0b3JNZXRlcnMgPSB0aGlzLmFkZC5zcHJpdGUoMzUwLCAxOTAsICdPREltYWdlcycsICdtb25pdG9ybWV0ZXJzLnBuZycpO1xyXG4gICAgICAgIGxldCBtb25pdG9yUGxhaW5EaWFsID0gdGhpcy5hZGQuc3ByaXRlKDQzMCwgMTkwLCAnT0RJbWFnZXMnLCAnbW9uaXRvcnBsYWluZGlhbC5wbmcnKTtcclxuICAgICAgICBsZXQgbW9uaXRvckJpZ0J1dHRvbiA9IHRoaXMuYWRkLnNwcml0ZSgzODAsIDMyMCwgJ09ESW1hZ2VzJywgJ21vbml0b3JiaWdidXR0b24ucG5nJyk7XHJcbiAgICAgICAgbGV2ZWxCdXR0b24uc2V0SW50ZXJhY3RpdmUoKTtcclxuICAgICAgICB0aGlzLnBsYXlCdXR0b24uc2V0SW50ZXJhY3RpdmUoKTtcclxuICAgICAgICBsZXZlbEJ1dHRvbi5vbigncG9pbnRlcmRvd24nLCAocG9pbnRlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxldmVsQnV0dG9uUHJlc3NlZCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucGxheUJ1dHRvbi5vbigncG9pbnRlcmRvd24nLCAocG9pbnRlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnBsYXlCdXR0b25QcmVzc2VkKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hZGQudGV4dCg4LCAxNSwgJ09jZWFuIERlZmVuZGVyIElJJywge1xyXG4gICAgICAgICAgICBmb250RmFtaWx5OiAnZXRobm9jZW50cmljX3JnJyxcclxuICAgICAgICAgICAgZm9udFNpemU6IDM0LFxyXG4gICAgICAgICAgICBjb2xvcjogJyMxNjU1OWQnXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hZGQudGV4dCgyODUsIDQwMCwgJ1JHQlogU1RVRElPUycsIHtcclxuICAgICAgICAgICAgZm9udEZhbWlseTogJ2V0aG5vY2VudHJpY19yZycsXHJcbiAgICAgICAgICAgIGZvbnRTaXplOiAxOCxcclxuICAgICAgICAgICAgY29sb3I6ICcjYmY2NjJmJ1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucmVnaXN0cnkuc2V0KCdBY3RpdmVSYWRhcicsIG5ldyBSYWRhckV2ZW50KCdSYWRhcjEnKSk7XHJcbiAgICAgICAgdGhpcy50d2VlbnMudGltZWxpbmUoe1xyXG4gICAgICAgICAgICB0YXJnZXRzOiBbdGhpcy5wbGF5QnV0dG9uXSxcclxuICAgICAgICAgICAgbG9vcDogLTEsXHJcbiAgICAgICAgICAgIGxvb3BEZWxheTogMjAwMCxcclxuICAgICAgICAgICAgdHdlZW5zOiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgYW5nbGU6IC0yNSxcclxuICAgICAgICAgICAgICAgICAgICBlYXNlOiAnU2luZS5lYXNlSW5PdXQnLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAyMDBcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYW5nbGU6IDI1LFxyXG4gICAgICAgICAgICAgICAgICAgIGVhc2U6ICdTaW5lLmVhc2VJbk91dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDQwMFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBvZmZzZXQ6IDYwMCxcclxuICAgICAgICAgICAgICAgICAgICBhbmdsZTogMCxcclxuICAgICAgICAgICAgICAgICAgICBlYXNlOiAnU2luZS5lYXNlSW5PdXQnLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAyMDBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudGltZS5hZGRFdmVudCh7XHJcbiAgICAgICAgICAgIGRlbGF5OiAxMDAwLFxyXG4gICAgICAgICAgICBsb29wOiB0cnVlLFxyXG4gICAgICAgICAgICBjYWxsYmFjazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tb25pdG9yLkRlbW9DcmVhdGVNaXNzaWxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmNhbWVyYXMubWFpbi5mYWRlSW4odGhpcy5mYWRlVGltZSwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IucmVkLCBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmZhZGVDb2xvci5ncmVlbiwgT0RHYW1lSW5mby5pbnN0YW5jZS53b3JsZC5mYWRlQ29sb3IuYmx1ZSk7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUodGltZSwgZHQpIHtcclxuICAgIH1cclxuICAgIGxldmVsQnV0dG9uUHJlc3NlZCgpIHtcclxuICAgICAgICB0aGlzLnRyYW5zaXRpb25Ub1NjZW5lKCdMZXZlbFNlbGVjdGlvbicpO1xyXG4gICAgfVxyXG4gICAgcGxheUJ1dHRvblByZXNzZWQoKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0Z1cnRoZXN0IGxldmVsIHVubG9ja2VkOiAnICsgT0RHYW1lSW5mby5HZXRGdXJ0aGVzdExldmVsVW5sb2NrZWQoKS50b1N0cmluZygpKTtcclxuICAgICAgICBPREdhbWVJbmZvLmluc3RhbmNlLndvcmxkLmN1cnJMZXZlbCA9IE9ER2FtZUluZm8uR2V0RnVydGhlc3RMZXZlbFVubG9ja2VkKCk7XHJcbiAgICAgICAgdGhpcy50cmFuc2l0aW9uVG9TY2VuZSgnR2FtZVNjcmVlbicpO1xyXG4gICAgfVxyXG4gICAgdHJhbnNpdGlvblRvU2NlbmUoa2V5KSB7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5yZW1vdmUoJ1JhZGFyMScpO1xyXG4gICAgICAgIHRoaXMuc2NlbmUucmVtb3ZlKCdSYWRhcjInKTtcclxuICAgICAgICB0aGlzLnNjZW5lLnJlbW92ZSgnUmFkYXIzJyk7XHJcbiAgICAgICAgdGhpcy5zY2VuZS5yZW1vdmUoJ01vbml0b3IxJyk7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RyeS5ldmVudHMub2ZmKCdjaGFuZ2VkYXRhJywgdW5kZWZpbmVkLCB1bmRlZmluZWQsIGZhbHNlKTtcclxuICAgICAgICB0aGlzLnNjZW5lLnN0YXJ0KGtleSwgeyByZXNldFNjb3JlOiB0cnVlIH0pO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE9EVGl0bGVTY2VuZTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBsb2NhbGZvcmFnZTsiLCJtb2R1bGUuZXhwb3J0cyA9IFBoYXNlcjsiXSwic291cmNlUm9vdCI6IiJ9