let rgbzEvent = function (name, message) {
  this.key = name;
  this.message = message;
  this.handledBy = [];
  this.lastModified = (new Date()).getTime();
};
