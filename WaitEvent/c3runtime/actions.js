"use strict";

{
	self.C3.Plugins.Rex_WaitEvent.Acts =
	{
		WaitEvent(event_name, tag)
		{
			if (!this.events.hasOwnProperty(tag))
				this.events[tag] = {};
			
			if (this.events[tag].hasOwnProperty(event_name))
				return;
			
			this.events[tag][event_name] = true;
			var cnds = C3.Plugins.Rex_WaitEvent.Cnds;
			this.runTrigEvent(cnds.OnAnyEventStart, tag, event_name);
		},
		EventFinished(event_name, tag)
		{
			if (!this.eventExist(event_name, tag))
				return;
			
			var cnds = C3.Plugins.Rex_WaitEvent.Cnds;
			this.exp_EventName = event_name;
			
			delete this.events[tag][event_name];
			this.runTrigEvent(cnds.OnAnyEventFinished, tag, event_name);
			
			if (isEmpty(this.events[tag]))
			{
				delete this.events[tag];
				this.runTrigEvent(cnds.OnAllEventsFinished, tag, event_name);
			}
		},
		CancelEvents(tag)
		{
			if (!this.events.hasOwnProperty(tag))
				return;
			
			delete this.events[tag];
		}
	};
} 