"use strict";

{
	C3.Plugins.RGBZ_EventTrigger.Exps =
	{
		LastEventMessage()
		{
			return (this.lastMessage && this.lastMessage.length>0)?this.lastMessage[0]:"";
		},
		GetEventMessageAt(index)
		{
			let result = '';

			if (this.lastMessage && index>=0 && Array.isArray(this.lastMessage) && index<this.lastMessage.length) {
				result = this.lastMessage[index];
			}

			return result;
		},
		GetEventMessageItemCount()
		{
			let result = 0;

			if (this.lastMessage && Array.isArray(this.lastMessage)) {
				result = this.lastMessage.length;
			}

			return result;
		}
	};
}