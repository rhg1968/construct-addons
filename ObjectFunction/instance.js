"use strict";

{
	const BEHAVIOR_CLASS = SDK.Behaviors.RGBZ_ObjectFunction;
	
	BEHAVIOR_CLASS.Instance = class RGBZ_ObjectFunctionInstance extends SDK.IBehaviorInstanceBase
	{
		constructor(sdkBehType, behInst)
		{
			super(sdkBehType, behInst);
		}
		
		Release()
		{
		}
		
		OnCreate()
		{
		}
		
		OnPropertyChanged(id, value)
		{
		}
		
		LoadC2Property(name, valueString)
		{
			return false;		// not handled
		}
	};
}