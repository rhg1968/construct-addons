"use strict";

{
	let processJsonFileLoad = (self, files, slots, numOfFiles)=>{
		for (let i=0;i<files.length;i++) {
			let index = i;
			let xhttp = new XMLHttpRequest();
			let slotName = slots[index].trim().toLowerCase();

			rgbzGetFromDb(slotName).then((cacheCheck)=>{
				if (!cacheCheck) {
					xhttp.onreadystatechange = function () {
						if (this.readyState == 4 && this.status == 200) {
							numOfFiles--;
	
							self.jsonSlots[slotName] = JSON.parse(xhttp.responseText);
							rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});
	
							if (numOfFiles<=0){
								//All files have been loaded, trigger complete event
								self.Trigger(C3.Plugins.RGBZ_JsonManager.Cnds.JsonManagerLoaded);
							}
						}
						else if (this.readyState == 4) {
							//An error has been returned from the server
							logMessage('Json ajax file retrieve falied - ' + this.status.toString() + ' - ' + this.statusText);
						}
					};
	
					if (files[i].trim().toLowerCase().startsWith('http')) {
						let url = files[i].trim().toLowerCase();
						xhttp.open("GET", url, true);
						xhttp.send();
					}
					else {
						let assetManager = self.GetRuntime().GetAssetManager();
						assetManager.LoadProjectFileUrl(files[i].trim().toLowerCase()).then((url)=>{
							xhttp.open("GET", url, true);
							xhttp.send();
						});
					}
				}
				else if (cacheCheck && cacheCheck.isError==false) {
					self.jsonSlots[slotName] = cacheCheck.items;
					numOfFiles--;

					if (numOfFiles<=0){
						//All files have been loaded, trigger complete event
						self.Trigger(C3.Plugins.RGBZ_JsonManager.Cnds.JsonManagerLoaded);
					}
				}
				else {
					logMessage('Unexpected error getting ' + slotName);
				}
			}, (error)=>{
				logMessage('Unexpected error getting ' + slotName + ' from indexDb');
			});
		}
	};

	C3.Plugins.RGBZ_JsonManager.Acts =
	{
		LoadJsonManager (enableCaching)
		{
			let self = this;

			let files;
			let slots;
			let numOfFiles;
			
			if (self.rgbzJsonFiles.trim()=='' || self.rgbzJsonSlots.trim()==''){
				logMessage("Files and Slots not specified.");
				return;
			}
			else {
				files = self.rgbzJsonFiles.split(',');
				slots = self.rgbzJsonSlots.split(',');
	
				if (files.length != slots.length){
					logMessage('Uneven number of files and slots.');
					return;
				}
				else {
					numOfFiles = files.length;
				}
			}
	
			try {
				if (enableCaching.toLowerCase()=='false') {
					clearStore().then((success)=>{
						processJsonFileLoad(self, files, slots, numOfFiles);
					}, (error)=>{
						logMessage('Error clearing object store.');
					});
				}
				else {
					processJsonFileLoad(self, files, slots, numOfFiles);
				}
			}
			catch(err){
				logMessage(err.message);
			}
		},
		LoadJsonManagerFromString (slotName, jsonString, enableCaching)
		{
			let self = this;

			try {
				if (enableCaching.toLowerCase()=='false') {
					clearStore().then((success)=>{
						self.jsonSlots[slotName] = JSON.parse(jsonString);
						rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});
			
						//Theoretically a trigger isn't required since this a synchronous operation, but I
						//will fire it anyway in case a user has logic down the line that relies on a loaded trigger occurring.
			
						self.Trigger(C3.Plugins.RGBZ_JsonManager.Cnds.JsonManagerLoaded);
					}, (error)=>{
						logMessage('Error: failure to clear object store - LoadJsonManagerFromString');
					});
				}
				else {
					self.jsonSlots[slotName] = JSON.parse(jsonString);
					rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});
			
						//Theoretically a trigger isn't required since this a synchronous operation, but I
						//will fire it anyway in case a user has logic down the line that relies on a loaded trigger occurring.
			
						self.Trigger(C3.Plugins.RGBZ_JsonManager.Cnds.JsonManagerLoaded);
				}
			}
			catch(err){
				logMessage(err.message);
			}
		},
		SetJsonValue (slot, query, property, val, withinExistingLoop)
		{
			let foundObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

			if (foundObj && property && typeof(foundObj[property])!=='undefined'){
				foundObj[property] = val;
	
				if (!withinExistingLoop) {
					rgbzAddToDb(slot, this.jsonSlots[slot]).then((data)=>{
						//Do nothing, success
					}, (error)=>{
						logMessage('Error adding to json manager.');
						console.log(error);
					});
				}
			}
			else {
				logMessage('Query could not locate object to perform a set on.');
			}
		},
		SetJsonSubToNewSlot (oldSlot, query, newSlot, withinExistingLoop)
		{
			let foundObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, oldSlot, query);

			if (foundObj) {
				this.jsonSlots[newSlot] = JSON.parse(JSON.stringify(foundObj));
	
				rgbzAddToDb(newSlot, this.jsonSlots[newSlot]).then((data)=>{
					//Do nothing, success
				}, (error)=>{
					logMessage('Error adding to json to new slot.');
					console.log(error);
				});
			}
			else {
				logMessage("Query to locate Json to put into a new slot can't be found.");
			}
		},
		ResetJsonSlotFromBackup (slot)
		{
			let self = this;

			resetKeyFromBak(slot).then((restoredItem)=>{
				if (restoredItem) {
					self.jsonSlots[slot] = restoredItem;
				}
			}, (error)=>{
				logMessage(error);
			});
		},
		RemoveJsonSlot (slot)
		{
			let self = this;

			rgbzDeleteFromDb(slot).then((success)=>{
				delete self.jsonSlots[slot];
			});
		}
	};
}