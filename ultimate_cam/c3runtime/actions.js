"use strict";
{
    self.C3.Plugins.EMI_INDO_Ultimate_Camera.Acts = {
        Optimis(x, y, width, height)
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            const ops = {
                x: x,
                y: y,
                width: width,
                height: height,
                camera: CameraPreview.CAMERA_DIRECTION.BACK,
                tapPhoto: self.tapphoto,
                previewDrag: self.previewdrag,
                toBack: self.toback,
                tapFocus: self.tapfocus,
                storeToFile: self.storetofile,
                disableExifHeaderStripping: self.disableexifheader

            };

            this.cd.startCamera(ops, success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Camera);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Error);
            }


        },

        Putar()
        {

            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.switchCamera(success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Putar);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Error);

            }

        },

        Stopcamera()
        {

            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.stopCamera(success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Stop);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Error);
            }


        },

        Show()
        {

            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.show(success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Show);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Error);
            }


        },

        Hide()
        {

            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.hide(success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Hide);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Error);
            }


        },

        Takepicture(a, b, c, d)
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            const ops = {

                width: a,
                height: b,
                quality: c,
                filepath: d

            }

            this.cd.takePicture(ops, d, success, fail)


            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Takepicture);

            }

            function fail(result)
            {
                self.errorResponse = (result);
                self.Trigger(self.ins.Error);

            }

        },

        Takesnapshot(quality)
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            this.cd.takeSnapshot(
            {
                quality: quality
            }, success, fail);

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Takesnapshot);
            }

            function fail(result)
            {
                self.errorResponse = (result);
                self.Trigger(self.ins.Error);
            }

        },

        Setcoloreffect(eff)
        {

            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Response);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Response);
            }

            this.cd.setColorEffect(CameraPreview.COLOR_EFFECT = eff, success, fail);
        },

        Setflashmode(rh)
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            function success(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Response);
            }

            function fail(result)
            {
                self.eventResponse = (result);
                self.Trigger(self.ins.Response);
            }
            this.cd.setFlashMode(CameraPreview.FLASH_MODE = rh, success, fail);

        },

        Startrecordvideo(p, w, x, y)
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            var opt = {
                cameraDirection: CameraPreview.CAMERA_DIRECTION.BACK,
                width: p,
                height: w,
                quality: x,
                withFlash: y
            }

            this.cd.startRecordVideo(opt, function(filePath)
            {
                self.eventResponse = (filePath);
                self.Trigger(self.ins.Startrecordvideo);
            });

        },

        Stoprecordvideo()
        {
            const self = this;
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};

            this.cd.stopRecordVideo(function(filePath)
            {
                //alert(filePath);
                self.eventResponse = (filePath);
                self.Trigger(self.ins.Stoprecordvideo);
            });

        },

        Setpreviewsize(w, h)
        {
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.setPreviewSize(
            {
                width: w,
                height: h
            });
        },

        Setzoom(zom)
        {
            if (typeof window['CameraPreview'] == 'undefined')
            {
                return;
            }
            else
            {};
            this.cd.setZoom(zom);
        }
    };
}