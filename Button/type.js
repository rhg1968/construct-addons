"use strict";

{
	const BEHAVIOR_CLASS = SDK.Behaviors.wackytoasterButtons;
	
	BEHAVIOR_CLASS.Type = class wackytoasterButtonType extends SDK.IBehaviorTypeBase
	{
		constructor(sdkPlugin, iBehaviorType)
		{
			super(sdkPlugin, iBehaviorType);
		}
	};
}