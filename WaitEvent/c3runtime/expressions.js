"use strict";

{
	self.C3.Plugins.Rex_WaitEvent.Exps =
	{
		CurEventName()
		{
			return this.exp_EventName;
		},
		CurTag()
		{
			return this.exp_Tag || "";
		}
	};
}