class ODWorldInfo
{
    public currPoints:number;
    public hiPoints:number;
    public currLevel:number;
    public hiLevel:number;
    public fadeColor:any;

    constructor(){
        this.fadeColor = {
            red: 204,
            green: 204,
            blue:204
        };
    }
}

export default ODWorldInfo;
