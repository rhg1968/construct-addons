enum ODState {
    Visible,
    Active,
    VisibleActive,
    NotVisible,
    NotActive,
    NotVisibleActive
}

export default ODState;
