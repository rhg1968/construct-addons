class GameEvent
{
    constructor(public key:string, public isPaused:boolean=false, public restartLevel:boolean=false, public gotoTitleScreen:boolean=false)
    {

    }
}

export default GameEvent;
