////////////////////////////////////////////////////////////////////////////////////////////////////
//  Object Not Defined
///////////////////////////////////////////////////////////////////////////////////////////////////
let objectIsNotDefined = (objectToCheck)=>{
    let result = false;

    if (typeof(objectToCheck)==='undefined') {
        result = true;
    }

    return result;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
//  rgbzStack
///////////////////////////////////////////////////////////////////////////////////////////////////
let rgbzStack = function(){
    this._stack = [];
    this.peek = function(){
        if (this._stack.length==0) return undefined;

        return this._stack[this._stack.length-1];
    };
    this.push = function(item){
        if (typeof(item)!=='undefined') {
            this._stack.push(item);
        }
    };
    this.pop = function(){
        if (this._stack.length>0) {
            return this._stack.pop();
        }
    };
    this.clear = function(){
        this._stack.length = 0;
    };
};

////////////////////////////////////////////////////////////////////////////////////////////////////
//  Array Demension Functions
//  Returns false if any item is not an array and if the array items aren't balanced
///////////////////////////////////////////////////////////////////////////////////////////////////
var getArrayDimemsions = (mat)=> {

    var d = dimZeroBaseHelper(mat);

    let valid = validator(d)(mat) && d.length<=3;

    //Construct 3 requires all arrays to specify 3 demensions
    while (d.length<3) {
        d.push(1);
    }

    //Validate that the entire array matches what was found at the zero indecies of this array.
    //Construct only suports 3 demensional arrays, so check for that also.
    return valid ? d : [];
};

var validator = (d)=> {
    return (mat)=> {
        if (Array.isArray(mat)) {
            return d.length > 0 &&
                d[0] === mat.length &&
                mat.every(arrItem => { return validator(d.slice(1))(arrItem); });
        }
        else {
            return d.length === 0;
        }
    };
};

var dimZeroBaseHelper = (mat)=> {
    if (Array.isArray(mat)) {
        return [mat.length].concat(dimZeroBaseHelper(mat[0]));
    } else {
        return [];
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////
//  Array Populate Functions
///////////////////////////////////////////////////////////////////////////////////////////////////
var populateC3Array = (sourceData, arrayDims)=>{
    if (arrayDims[2] > 1) {
        return sourceData;
    }
    else {
        var result = [];

        for (let i=0; i<arrayDims[0]; i++) {
            result.push([]);

            for (let j=0; j<arrayDims[1]; j++) {
                result[i][j]= [(arrayDims[1]>1)?sourceData[i][j]:sourceData[i]];
            }
        }
        return result;
    }
};
