class RadarDetailEvent
{
    constructor(public id:number, public x:number=0, public y:number=0, public displayWidth:number=0, public displayHeight:number=0)
    {

    }
}

export default RadarDetailEvent;
