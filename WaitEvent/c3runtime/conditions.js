"use strict";

{
	self.C3.Plugins.Rex_WaitEvent.Cnds =
	{
		OnAllEventsFinished(tag)
		{
			return (this.exp_Tag === tag);
		},
		OnAnyEventFinished(tag)
		{
			return (this.exp_Tag === tag);
		},
		NoWaitEvent(tag)
		{
			var e = this.events[tag];
			if (e == null)
				return true;
			var k;
			for (k in e)
			{
				return false;
			}
			return true;
		},
		OnAnyEventStart()
		{
			return true;
		},
		IsWaiting(event_name, tag)
		{
			return (this.events[tag] && this.events[tag][event_name]);
		}
	};
}