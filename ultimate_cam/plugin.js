"use strict";
{

    const PLUGIN_ID = "EMI_INDO_Ultimate_Camera";
    const PLUGIN_VERSION = "1.0.0.8";
    const PLUGIN_CATEGORY = "media";

    const PLUGIN_CLASS = SDK.Plugins.EMI_INDO_Ultimate_Camera = class Ultimate_CameraPlugin extends SDK.IPluginBase
    {
        constructor()
        {
            super(PLUGIN_ID);

            SDK.Lang.PushContext("plugins." + PLUGIN_ID.toLowerCase());

            this._info.SetName(lang(".name"));
            this._info.SetDescription(lang(".description"));
            this._info.SetVersion(PLUGIN_VERSION);
            this._info.SetCategory(PLUGIN_CATEGORY);
            this._info.SetAuthor("EMI_INDO");
            this._info.SetHelpUrl(lang(".help-url"));
            this._info.SetIsSingleGlobal(true);

            this._info.SetSupportedRuntimes(["c3"]);

            this._info.AddCordovaPluginReference(
            {
                id: "cordova-plugin-camera-preview",
                plugin: this,
                version: "^0.12.1",
            });

            this._info.AddCordovaPluginReference(
            {
                id: "cordova-plugin-androidx",
                plugin: this,
                version: "^3.0.0",
            });

            SDK.Lang.PushContext(".properties");

            this._info.SetProperties([
            new SDK.PluginProperty("group", "group-start-camera-defaults"),
                new SDK.PluginProperty("check", "to-back", true),
                new SDK.PluginProperty("check", "tap-photo", true),
                new SDK.PluginProperty("check", "tap-focus", true),
                new SDK.PluginProperty("check", "preview-drag", true),
                new SDK.PluginProperty("check", "store-to-file", true),
                new SDK.PluginProperty("check", "disable-exif-header", true)

            ]);

            SDK.Lang.PopContext(); //.properties
            SDK.Lang.PopContext();
        }
    };

    PLUGIN_CLASS.Register(PLUGIN_ID, PLUGIN_CLASS);
}