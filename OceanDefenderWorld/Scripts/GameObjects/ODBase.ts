import ODState from './../Enums/ODState';

abstract class ODBase extends Phaser.GameObjects.Sprite
{
    public finalX:number;
    public finalY:number;
    public direction:Phaser.Math.Vector2;
    public speed:number;
    public id:number;
    public rotationDelta:number;
    public tag:string;

    constructor(scene:Phaser.Scene, x:number, y:number, texture:string, frame?:string|number)
    {
        super(scene, x, y, texture, frame);

        this.direction = new Phaser.Math.Vector2();
    }

    setXY(x:number, y:number, finalX:number, finalY:number):void
    {
        this.x = x;
        this.y = y;
        this.finalX=finalX;
        this.finalY=finalY;

        this.direction.set(this.finalX-this.x, this.finalY-this.y).normalize();
    }

    public setODState(state:ODState):void {
        switch(state) {
            case ODState.Active:
                this.setActive(true);
                break;
            case ODState.NotActive:
                this.setActive(false);
                break;
            case ODState.NotVisible:
                this.setVisible(false);
                break;
            case ODState.NotVisibleActive:
                this.setActive(false);
                this.setVisible(false);
                break;
            case ODState.Visible:
                this.setVisible(true);
                break;
            case ODState.VisibleActive:
                this.setActive(true);
                this.setVisible(true);
            break;
        }
    }
}

export default ODBase;
