import ODBase from './ODBase';

class ODTrashMarker extends ODBase
{
    constructor(scene:Phaser.Scene, x:number, y:number, texture:string, frame?:string|number) {
        super(scene, x, y, texture, frame);

        this.id = (new Date()).getTime();
    }

    SetId():void
    {
        this.id = (new Date()).getTime();
    }
}

export default ODTrashMarker;
