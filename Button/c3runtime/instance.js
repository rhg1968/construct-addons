"use strict";

{
	C3.Behaviors.wackytoasterButtons.Instance = class wackytoasterButtonInstance extends C3.SDKBehaviorInstanceBase
	{
		constructor(behInst, properties)
		{
			super(behInst);
			
			this._buttonFunction = "";
			this._functionParams = "[]";
			this._buttonAnimation = "";
			this._animationParams = "[]";
			this._enabled = true;
			
			if (properties)
			{
				this._buttonFunction = properties[0];
				this._functionParams = properties[1];
				this._buttonAnimation = properties[2];
				this._animationParams = properties[3];
				this._enabled = properties[4];
			}
			
			// Opt-in to getting calls to Tick()
			//this._StartTicking();
		}

		Release()
		{
			super.Release();
		}
		
		SaveToJson()
		{
			return {
				// data to store for savegames
			};
		}

		LoadFromJson(o)
		{
			// load state for savegames
		}
		
		/*
		Tick()
		{
			const dt = this._runtime.GetDt(this._inst);
			const wi = this._inst.GetWorldInfo();
			
			// ... code to run every tick for this behavior ...
		}
		*/
	};
}