"use strict";

{
	C3.Plugins.RGBZ_JsonManager.Cnds =
	{
		JsonManagerLoaded (data)
		{
			return true;
		},
		JsonArrayLoop (slot, query, withinExistingLoop)
		{
			let positionObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(), query):getJsonObject(this.jsonSlots, slot, query);
	
			if (!positionObj || !Array.isArray(positionObj)){
				logMessage('Json object undefined or not an array.');
			}
			else {
				let runtime = this._runtime;
				let eventSheetManager = runtime.GetEventSheetManager();
				let currentEvent = runtime.GetCurrentEvent();
				let solModifiers = currentEvent.GetSolModifiers();
				let eventStack = runtime.GetEventStack();

				//Get current stack frame and push a new one
				let oldFrame = eventStack.GetCurrentStackFrame();
				let newFrame = eventStack.Push(currentEvent);
	
				let index = 0;

				for (let item of positionObj){
					this.loopIndexStack.push(index);
	
					this.lastJsonObjectSelectedStack.push(item);

					// Push a copy of the current SOL
					eventSheetManager.PushCopySol(solModifiers);

					// Retrigger the current event, running a single loop iteration
					currentEvent.Retrigger(oldFrame, newFrame);
	
					this.loopIndexStack.pop();
					this.lastJsonObjectSelectedStack.pop();

					// Pop the current SOL
					eventSheetManager.PopSol(solModifiers);

                    index++;
				}
	
				//Update after the loop just in case values were set into the object so they can be persisted to local storage.
				if (slot.trim()!='') {
					rgbzAddToDb(slot, this.jsonSlots[slot]).then((success)=>{}, (error)=>{});
				}

				// Pop the event stack frame
				eventStack.Pop();
			}
	
			return false;
		}
	};
}