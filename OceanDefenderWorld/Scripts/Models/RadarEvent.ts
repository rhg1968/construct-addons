import RadarDetailEvent from './RadarDetailEvent';

class RadarEvent
{
    public trash:Array<RadarDetailEvent>;
    public missiles:Array<RadarDetailEvent>;

    constructor(public key:string, public trashToClear:number=0, public oceanHealth:number=0)
    {
        this.trash = new Array<RadarDetailEvent>();
        this.missiles = new Array<RadarDetailEvent>();
    }
}

export default RadarEvent;
