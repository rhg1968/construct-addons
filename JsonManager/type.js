"use strict";

{
	const PLUGIN_CLASS = SDK.Plugins.RGBZ_JsonManager;
	
	PLUGIN_CLASS.Type = class RGBZ_JsonManagerType extends SDK.ITypeBase
	{
		constructor(sdkPlugin, iObjectType)
		{
			super(sdkPlugin, iObjectType);
		}
	};
}