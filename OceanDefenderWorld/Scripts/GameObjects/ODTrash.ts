import ODState from './../Enums/ODState';
import ODBase from './ODBase';

class ODTrash extends ODBase
{
    public ignore:boolean;

    constructor(scene: Phaser.Scene, x: number, y: number, texture: string, frame?: string | integer)
    {
        super(scene, x, y, texture, frame);

        this.ignore = false;
    }
}

export default ODTrash;
