import ODRadarInfo from './ODRadarInfo';

class ODLevelInfo
{
    public levelNum:number;
    public name:string;
    public cleared:boolean;
    public unlocked:boolean;
    public pointValue:number;
    public missileSpeed:number;
    public radars:Array<ODRadarInfo>;
    public coolDownTimer:number;

    constructor() 
    {
        this.cleared=false;
        this.unlocked=false;
        this.radars=new Array<ODRadarInfo>();
        this.coolDownTimer = 400;
    }
}

export default ODLevelInfo;
