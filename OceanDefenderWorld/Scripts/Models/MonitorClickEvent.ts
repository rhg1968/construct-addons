class MonitorClickEvent
{
    constructor(public key:string, public initialX:number=0, public initialY:number=0, public finalX:number=0,
        public finalY:number=0, public monitorWidth:number=0,
        public monitorHeight:number=0)
    {

    }
}

export default MonitorClickEvent;
