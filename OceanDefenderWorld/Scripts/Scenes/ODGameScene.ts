import ODMonitor from "./ODMonitor";
import ODGameInfo from "./../Models/ODGameInfo";
import ODRadar from "./ODRadar";
import RadarEvent from "./../Models/RadarEvent";
import GameEvent from "./../Models/GameEvent";
import MonitorClickEvent from "./../Models/MonitorClickEvent";
import { Scene } from "phaser";
import ODPauseScene from "./ODPauseScene";

class ODGameScene extends Phaser.Scene
{
    private monitor:ODMonitor;
    private fadeTime:number;
    private radars:Array<ODRadar>;
    private pausePanel:ODPauseScene;

    constructor()
    {
        super({
            key: 'GameScreen'
        });
    }

    init(data:any):void
    {
        this.monitor = undefined;
        this.fadeTime = 1000;
        
        if(data.resetScore==true) {
            ODGameInfo.instance.world.currPoints=0;
        }

        this.radars = new Array<ODRadar>();
    }

    create():void
    {
        for(let i=0; i<3; i++) {
            let key:string = 'Radar'+(i+1).toString();
            let currLevel:number = ODGameInfo.instance.world.currLevel;

            let radar:ODRadar = new ODRadar(key, 0+(160*i), 630, 160, 212.8, (i==0)?true:false, this.fadeTime);

            this.scene.add(key, radar, true);
            
            radar.SetRadarOn(ODGameInfo.instance.levels[currLevel].radars[i].active);

            this.radars.push(radar);
        }

        let monitorWidth:number = (this.game.config.width as number)-40;
        this.monitor = new ODMonitor('Monitor1', (this.game.config.width as number/2.0) - (monitorWidth/2.0), 30, monitorWidth/*260*/, /*345.8*/monitorWidth*1.33, this.fadeTime);
        this.scene.add('Monitor1', this.monitor, true);

        this.pausePanel = new ODPauseScene('PausePanel', (this.game.config.width as number/2.0)-(350/2.0), (this.monitor.y)+(50), 350, 500);
        this.scene.add('PausePanel', this.pausePanel , false);

        this.registry.set('ActiveRadar', new RadarEvent('Radar1'));

        this.registry.events.on('changedata', (parent:any, key:string, data:GameEvent)=>{
            switch(key)
            {
                case 'GameEventRadarWon':
                    this.ProcessGameEventRadarWon(data);
                    break;
                case 'GameEventRadarLost':
                    this.ProcessGameEventRadarLost();
                    break;
                case 'GamePause':
                    this.ProcessGamePause(data);
                    break;
            }
        });

        this.cameras.main.fadeIn(this.fadeTime, ODGameInfo.instance.world.fadeColor.red, ODGameInfo.instance.world.fadeColor.green, ODGameInfo.instance.world.fadeColor.blue);

        console.log('Show Level Info');

        this.time.addEvent({
            delay: 2000,
            callback: ()=>{
                console.log('Start radar timers');

                this.radars.forEach((radar:ODRadar)=>{radar.StartTimer();});
            }
        });
    }

    private ProcessGameEventRadarLost():void
    {
        this.transitionToScene('TitleScreen');
    }

    private ProcessGamePause(data:GameEvent):void
    {
        if (data.restartLevel) {
            this.restartScene();
        }
        else if (data.gotoTitleScreen)
        {
            this.transitionToScene('TitleScreen');
        }
        else if (data.isPaused as boolean) {
            this.scene.run('PausePanel');

            this.scene.pause(this.monitor.key);

            this.radars.forEach((radar:ODRadar)=>{
                this.scene.pause(radar.key);
            });
        }
        else {
            this.scene.sleep('PausePanel');

            this.scene.resume(this.monitor.key);

            this.radars.forEach((radar:ODRadar)=>{
                this.scene.resume(radar.key);
            });
        }
    }

    private ProcessGameEventRadarWon(data:GameEvent):void
    {
        //Determine if there is another radar to switch to
        let foundAvailableRadar:ODRadar = this.radars.find(x=>x.GetRadarOn() && x.key!=data.key);

        if (foundAvailableRadar)
        {
            //Switch over to available radar
            this.registry.set('ActiveRadar', new RadarEvent(foundAvailableRadar.key));
            this.registry.set('SetRadarBorder', new MonitorClickEvent(foundAvailableRadar.key));
        }
        else
        {
            //no available radars, so the level is complete
            this.restartScene();
        }
    }

    private prepareForSceneTransition():void
    {
        this.radars.length = 0;
        this.scene.remove('PausePanel');
        this.scene.remove('Radar1');
        this.scene.remove('Radar2');
        this.scene.remove('Radar3');
        this.scene.remove('Monitor1');
        this.registry.events.off('changedata', undefined, undefined, false);
    }

    private restartScene():void
    {
        this.prepareForSceneTransition();

        ODGameInfo.instance.levels[ODGameInfo.instance.world.currLevel].cleared = true;
        ODGameInfo.instance.world.currLevel+=1;
        ODGameInfo.instance.levels[ODGameInfo.instance.world.currLevel].unlocked = true;

        this.scene.restart({resetScore:false});
    }

    private transitionToScene(key:string):void
    {
        this.prepareForSceneTransition();

        this.scene.start(key, {resetScore:true});
    }
}

export default ODGameScene;
