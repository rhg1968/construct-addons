﻿// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.RGBZ_JsonManager = function(runtime)
{
	this.runtime = runtime;
};

(function ()
{
	var pluginProto = cr.plugins_.RGBZ_JsonManager.prototype;
		
	/////////////////////////////////////
	// Object type class
	pluginProto.Type = function(plugin)
	{
		this.plugin = plugin;
		this.runtime = plugin.runtime;
	};

	var typeProto = pluginProto.Type.prototype;

	typeProto.onCreate = function()
	{
	};

	/////////////////////////////////////
	// Instance class
	pluginProto.Instance = function(type)
	{
		this.type = type;
		this.runtime = type.runtime;
	};
	
	var instanceProto = pluginProto.Instance.prototype;
	
	instanceProto.onCreate = function()
	{
		// Read properties set in C3
		this.testProperty = this.properties[0];

		//Set up storage of json slots
		this.jsonSlots = {};
		this.lastJsonObjectSelectedStack = new rgbzStack();
		this.loopIndexStack = new rgbzStack();

		initializeJSMDB(this.runtime.name);
	};
	
	instanceProto.saveToJSON = function ()
	{
		return {};
	};
	
	instanceProto.loadFromJSON = function (o)
	{
	};
	
	/**BEGIN-PREVIEWONLY**/
	instanceProto.getDebuggerValues = function (propsections)
	{
	};
	/**END-PREVIEWONLY**/

	//////////////////////////////////////
	// Conditions
	function Cnds() {};
	
	Cnds.prototype.JsonManagerLoaded = function(data){
		return true;
	};

	Cnds.prototype.JsonArrayLoop = function(slot, query, withinExistingLoop) {
		let current_frame = this.runtime.getCurrentEventStack();
		let current_event = current_frame.current_event;
		let colModifiedAfterCnds = current_frame.isModifierAfterCnds();

		let positionObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(), query):getJsonObject(this.jsonSlots, slot, query);

		if (!positionObj || !Array.isArray(positionObj)){
			logMessage('Json object undefined or not an array.');
		}
		else {
			let index = 0;

			if (colModifiedAfterCnds){
				for (let item of positionObj){
					this.runtime.pushCopySol(current_event.solModifiers);

                    this.loopIndexStack.push(index);

					this.lastJsonObjectSelectedStack.push(item);

					current_event.retrigger();

					this.loopIndexStack.pop();
					this.lastJsonObjectSelectedStack.pop();

					this.runtime.popSol(current_event.solModifiers);

					index++;
				}
			}
			else {
				for (let item of positionObj){
                    this.loopIndexStack.push(index);

                    this.lastJsonObjectSelectedStack.push(item);

					current_event.retrigger();

                    this.loopIndexStack.pop();
					this.lastJsonObjectSelectedStack.pop();

                    index++;
				}
			}

			//Update after the loop just in case values were set into the object so they can be persisted to local storage.
			if (slot.trim()!='') {
				rgbzAddToDb(slot, this.jsonSlots[slot]).then((success)=>{}, (error)=>{});
			}
		}

		return false;
	};
	
	pluginProto.cnds = new Cnds();

	//////////////////////////////////////
	// Actions
	function Acts() {};

	Acts.prototype.LoadJsonManagerFromString = function(slotName, jsonString, enableCaching){
		let self = this;

		try {
			if (enableCaching.toLowerCase()=='false') {
				clearStore().then((success)=>{
					self.jsonSlots[slotName] = JSON.parse(jsonString);
					rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});
		
					//Theoretically a trigger isn't required since this a synchronous operation, but I
					//will fire it anyway in case a user has logic down the line that relies on a loaded trigger occurring.
		
					self.runtime.trigger(cr.plugins_.RGBZ_JsonManager.prototype.cnds.JsonManagerLoaded, self);
				}, (error)=>{
					logMessage('Error: failure to clear object store - LoadJsonManagerFromString');
				});
			}
			else {
				self.jsonSlots[slotName] = JSON.parse(jsonString);
				rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});
	
				//Theoretically a trigger isn't required since this a synchronous operation, but I
				//will fire it anyway in case a user has logic down the line that relies on a loaded trigger occurring.
	
				self.runtime.trigger(cr.plugins_.RGBZ_JsonManager.prototype.cnds.JsonManagerLoaded, self);
			}
		}
		catch(err){
			console.log(err.message);
		}
	};

	let processJsonFileLoad = (self, files, slots, numOfFiles)=>{
		for (let i=0;i<files.length;i++) {
			let index = i;
			let xhttp = new XMLHttpRequest();
			let slotName = slots[index].trim().toLowerCase();

			rgbzGetFromDb(slotName).then((cacheCheck)=>{
				if (!cacheCheck) {
					xhttp.onreadystatechange = function () {
						if (this.readyState == 4 && this.status == 200) {
							numOfFiles--;

							self.jsonSlots[slotName] = JSON.parse(xhttp.responseText);
							rgbzAddToDb(slotName, self.jsonSlots[slotName]).then((success)=>{}, (error)=>{});

							if (numOfFiles<=0){
								//All files have been loaded, trigger complete event
								self.runtime.trigger(cr.plugins_.RGBZ_JsonManager.prototype.cnds.JsonManagerLoaded, self);
							}
						}
						else if (this.readyState == 4) {
							//An error has been returned from the server
							logMessage('Json ajax file retrieve falied - ' + this.status.toString() + ' - ' + this.statusText);
						}
					};

					let pathToFile = (!self.runtime.isPreview || files[i].trim().toLowerCase().startsWith('http'))?files[i].trim().toLowerCase():self.runtime.getProjectFileUrl(files[i].trim().toLowerCase());
					xhttp.open("GET", pathToFile, true);
					xhttp.send();
				}
				else if(cacheCheck && cacheCheck.isError==false) {
					self.jsonSlots[slotName] = cacheCheck.items;
					numOfFiles--;

					if (numOfFiles<=0){
						//All files have been loaded, trigger complete event
						self.runtime.trigger(cr.plugins_.RGBZ_JsonManager.prototype.cnds.JsonManagerLoaded, self);
					}
				}
				else {
					logMessage('Unexpected error getting ' + slotName);
				}
			}, (error)=>{
				logMessage('Unexpected error getting ' + slotName + ' from indexDb');
			});
		}
	};

    Acts.prototype.LoadJsonManager = function(enableCaching) {
    	let self = this;

    	let files;
    	let slots;
		let numOfFiles;
		
    	if (self.properties.length<2 || self.properties[0].trim()=='' || self.properties[1].trim()==''){
    	    logMessage("Files and Slots not specified.");
    	    return;
        }
        else {
    	    files = self.properties[0].split(',');
            slots = self.properties[1].split(',');

            if (files.length != slots.length){
                logMessage('Uneven number of files and slots.');
                return;
            }
            else {
                numOfFiles = files.length;
            }
        }

		try {
			if (enableCaching.toLowerCase()=='false') {
				clearStore().then((success)=>{
					processJsonFileLoad(self, files, slots, numOfFiles);
				}, (error)=>{
					logMessage('Error clearing object store.');
				});
			}
			else {
				processJsonFileLoad(self, files, slots, numOfFiles);
			}
		}
		catch(err){
			logMessage(err.message);
		}
	};

	Acts.prototype.SetJsonValue = function(slot, query, property, val, withinExistingLoop){
		let foundObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

		if (foundObj && property && typeof(foundObj[property])!=='undefined'){
			foundObj[property] = val;

			if (!withinExistingLoop) {
				rgbzAddToDb(slot, this.jsonSlots[slot]).then((data)=>{
					//Do nothing, success
				}, (error)=>{
					logMessage('Error adding to json manager.');
					console.log(error);
				});
			}
		}
		else {
			logMessage('Query could not locate object to perform a set on.');
		}
	};

	Acts.prototype.SetJsonSubToNewSlot = function(oldSlot, query, newSlot, withinExistingLoop) {
		let foundObj = (withinExistingLoop)?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, oldSlot, query);

		if (foundObj) {
			this.jsonSlots[newSlot] = JSON.parse(JSON.stringify(foundObj));

			rgbzAddToDb(newSlot, this.jsonSlots[newSlot]).then((data)=>{
				//Do nothing, success
			}, (error)=>{
				logMessage('Error adding to json to new slot.');
				console.log(error);
			});
		}
		else {
			logMessage("Query to locate Json to put into a new slot can't be found.");
		}
	};

	Acts.prototype.ResetJsonSlotFromBackup = function(slot) {
		var self = this;

		resetKeyFromBak(slot).then((restoredItem)=>{
			if (restoredItem) {
				self.jsonSlots[slot] = restoredItem;
			}
		}, (error)=>{
			logMessage(error);
		});
	};

	Acts.prototype.RemoveJsonSlot = function(slot) {
		let self = this;

		rgbzDeleteFromDb(slot).then((success)=>{
			delete self.jsonSlots[slot];
		});
	};
	
	pluginProto.acts = new Acts();

	//////////////////////////////////////
	// Expressions
	function Exps() {};

	Exps.prototype.JsonLoopIndex = function(ret){
		let result = this.loopIndexStack.peek();

		ret.set_int(result);
	};

	Exps.prototype.GetJsonArrayLength = function(ret, slot, query, withinExistingLoop) {
		let result = 0;

		let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

		if (currentPosition && Array.isArray(currentPosition)) {
			result = currentPosition.length;
		}

		ret.set_int(result);
	};

	Exps.prototype.GetJsonValue = function(ret, slot, query, withinExistingLoop) {
		let result = '';

		let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

		if (typeof(currentPosition)!=='undefined') {
			result = currentPosition.toString();
		}

		ret.set_string(result);
	};

	Exps.prototype.GetArrayFromJsonPath = function(ret, slot, query, withinExistingLoop){
		let result = '';

		let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

		if (currentPosition) {
			let arrayDims = getArrayDimemsions(currentPosition);

			if (arrayDims && arrayDims.length>0) {
				let resultObj = {
					"c2array": true,
					"size": arrayDims,
					"data": populateC3Array(currentPosition, arrayDims)
				};

				result = JSON.stringify(resultObj);
			}
		}

		ret.set_string(result);
	};

    Exps.prototype.GetDictionaryFromJsonPath = function(ret, slot, query, withinExistingLoop) {
        let result = '';

        let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);

        if (currentPosition) {
            let resultObj = {
                "c2dictionary": true,
                "data": {}
            };

            for (let key of Object.keys(currentPosition)) {
                let testType = typeof(currentPosition[key]);

                if (testType==='number' || testType==='string') {
                    resultObj.data[key] = currentPosition[key];
                }
                else {
                    break;
                }
            }

            result = JSON.stringify(resultObj);
        }

        ret.set_string(result);
    };

	pluginProto.exps = new Exps();
}());