import ODGameInfo from "./../Models/ODGameInfo";
import ODLevelInfo from "./../Models/GameInfoModels/ODLevelInfo";

class ODLevelSelection extends Phaser.Scene
{
    private fadeTime:number;

    constructor(){
        super({key: 'LevelSelection'});

        this.fadeTime = 1000;
    }

    create():void
    {
        this.add.text(5, 15, 'Select A Level', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 35,
            color: '#16559d'
        });

        ((this.add as any).backbutton(447, 38) as Phaser.GameObjects.Sprite).setScale(0.5).setInteractive().on('pointerdown', (pointer:any)=>{
            this.scene.start('TitleScreen');
        });

        let startRow:number=200;
        let startCol:number=90;

        for (let i:number=1; i<ODGameInfo.instance.levels.length; i++)
        {
            let levelInfo:ODLevelInfo = ODGameInfo.instance.levels[i] as ODLevelInfo;
            let buttonToRender:Phaser.GameObjects.Sprite;

            if (levelInfo.cleared && levelInfo.unlocked) {
                buttonToRender = (this.add as any).greenbutton(startCol, startRow).setDisplaySize(121, 130).setInteractive().on('pointerdown', ()=>{this.processLevelClick(i)});

                this.add.text(startCol, startRow-10, i.toString(), {
                    fontFamily: 'ethnocentric_rg',
                    fontSize: 64,
                    color: '#ffffff'
                }).setOrigin(0.5, 0.5);
            }
            else if (levelInfo.unlocked) {
                buttonToRender = (this.add as any).bluebutton(startCol, startRow).setDisplaySize(121, 130).setInteractive().on('pointerdown', ()=>{this.processLevelClick(i)});

                this.add.text(startCol, startRow-10, i.toString(), {
                    fontFamily: 'ethnocentric_rg',
                    fontSize: 64,
                    color: '#ffffff'
                }).setOrigin(0.5, 0.5);
            }
            else {
                buttonToRender = (this.add as any).levellockbutton(startCol, startRow).setDisplaySize(121, 130);
            }

            buttonToRender.setDisplaySize(121, 130);

            startCol += buttonToRender.displayWidth+25;

            if (startCol>=400) {
                startCol = 90;
                startRow += buttonToRender.displayHeight + 25;
            }
        }

        this.cameras.main.fadeIn(this.fadeTime, ODGameInfo.instance.world.fadeColor.red, ODGameInfo.instance.world.fadeColor.green, ODGameInfo.instance.world.fadeColor.blue);
    }

    private processLevelClick(levelNum:number)
    {
        ODGameInfo.instance.world.currLevel=levelNum;
        this.scene.start('GameScreen', {resetScore:true});
    }
}

export default ODLevelSelection;
