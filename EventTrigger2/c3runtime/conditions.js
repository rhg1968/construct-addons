"use strict";

const C3 = globalThis.C3;

C3.Plugins.RGBZ_PubSub.Cnds =
{
	SubscribeToEvent(name)
	{
		if (this.ConsoleLogging) console.info(`Subscribe called ${name}`);

		return (this.EventPublished && this.EventPublished.Name.toLowerCase()==name.toLowerCase());
	}
};
