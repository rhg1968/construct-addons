import ODBase from './ODBase';
import ODState from './../Enums/ODState';
import ODRadar from './../Scenes/ODRadar';
import ODTrashMarker from './ODTrashMarker';
import RadarEvent from './../Models/RadarEvent';
import RadarDetailEvent from './../Models/RadarDetailEvent';

class ODExplosionMarker extends ODBase
{
    private radar:ODRadar;

    constructor(scene:Phaser.Scene, x:number, y:number, texture:string, frame?:string|number)
    {
        super(scene, x, y, texture, frame);

        this.id = (new Date()).getTime();
        this.radar = scene as ODRadar;
    }

    SetId():void
    {
        this.id = (new Date()).getTime();
    }

    update(time: number, dt: number) {
        if (this.active)
        {
            this.radar.GetTrashMarkerPool().getChildren().filter((mm: ODTrashMarker)=>mm.active).forEach((marker:ODTrashMarker)=>{
                if(Math.abs(Phaser.Math.Distance.Between(this.x, this.y, marker.x, marker.y))<10) {
                    marker.setODState(ODState.NotVisibleActive);

                    let event:RadarEvent = new RadarEvent(marker.tag);
                    event.trash.push(new RadarDetailEvent(marker.id));

                    this.scene.registry.set('TrashDestroyed', event);
                }

                // if (Phaser.Geom.Intersects.RectangleToRectangle(this.getBounds(), marker.getBounds())) {
                //     marker.setState(ODState.NotVisibleActive);

                //     let event:RadarEvent = new RadarEvent(marker.tag);
                //     event.trash.push(new RadarDetailEvent(marker.id));

                //     this.scene.registry.set('TrashDestroyed', event);
                // }
            });
        }
    }
}

export default ODExplosionMarker;
