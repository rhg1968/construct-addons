"use strict";

{
	C3.Plugins.RGBZ_EventTrigger.Cnds =
	{
		EventTriggered(data)
		{
			let current_frame = this._runtime.GetCurrentEventStackFrame();
			let current_event = this._runtime.GetCurrentEvent();
	
			let eventId = current_event._sid.toString() + '-' + current_frame._cndIndex.toString();
	
			//Find the first event of this (data) type in the collection That hasn't been processed for this eventId Key.
			//This allows multiple triggers to be fired for the same event.
			let eventFound = this.rgbzEventBuffer.find(x=>x.key==data && x.handledBy.indexOf(eventId)<0);
	
			if (eventFound) {
				eventFound.handledBy.push(eventId);
				eventFound.lastModified = (new Date()).getTime();
	
				this.lastMessage = eventFound.message;
	
				return true;
			}
			else {
				this.lastMessage = [];
				return false;
			}
		}
	};
}