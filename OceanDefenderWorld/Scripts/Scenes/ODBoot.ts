import ODMissile from './../GameObjects/ODMissile';
import ODRedExplosion from './../GameObjects/ODRedExplosion';
import ODOcean from './../GameObjects/ODOcean';
import ODYellowGunBase from './../GameObjects/ODYellowGunBase';
import ODBlueGunBase from './../GameObjects/ODBlueGunBase';
import ODGreyGunBase from './../GameObjects/ODGreyGunBase';
import ODYellowGun from './../GameObjects/ODYellowGun';
import ODBlueGun from './../GameObjects/ODBlueGun';
import ODGreyGun from './../GameObjects/ODGreyGun';
import ODGameInfo from './../Models/ODGameInfo';
import ODLevelInfo from './../Models/GameInfoModels/ODLevelInfo';
import ODRadarInfo from './../Models/GameInfoModels/ODRadarInfo';
import ODCacheManager from './../Managers/ODCacheManager';
import RadarEvent from './../Models/RadarEvent';
import ODWhiteExplosion from './../GameObjects/ODWhiteExplosion';

class ODBoot extends Phaser.Scene
{
    constructor() {
        super({
            key: 'BootScene'
        });
    }

    init():void{
        (document as any).fonts.load('10pt "ethnocentric_rg"');
        //explodewhite0003

        this.sys.plugins.registerGameObject('redexplosion', function(x:number, y:number){
            let redExplosion:ODRedExplosion = new ODRedExplosion(this.scene, x, y, 'ODImages', 'redexplode0004.png');

            this.displayList.add(redExplosion);
            this.updateList.add(redExplosion);

            return redExplosion;
        });

        this.sys.plugins.registerGameObject('whiteexplosion', function(x:number, y:number){
            let whiteExplosion:ODWhiteExplosion = new ODWhiteExplosion(this.scene, x, y, 'ODImages', 'explodewhite0003.png');

            this.displayList.add(whiteExplosion);
            this.updateList.add(whiteExplosion);

            return whiteExplosion;
        });

        this.sys.plugins.registerGameObject('missile', function(x:number, y:number){
            let missile:ODMissile = new ODMissile(this.scene, x, y, 'ODImages','greenmissile.png');

            this.displayList.add(missile);
            this.updateList.add(missile);

            return missile;
        });

        this.sys.plugins.registerGameObject('ocean', function(x:number, y:number){
            let ocean:ODOcean = new ODOcean(this.scene, x, y, 'ODImages', 'ocean.png');

            this.displayList.add(ocean);
            this.updateList.add(ocean);

            return ocean;
        });

        this.sys.plugins.registerGameObject('sky', function(x:number, y:number){
            let sky:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanborder.png');

            this.displayList.add(sky);
            this.updateList.add(sky);

            return sky;
        });

        this.sys.plugins.registerGameObject('yellowbase', function(x:number, y:number){
            let ybase:ODYellowGunBase = new ODYellowGunBase(this.scene, x, y, 'ODImages', 'baseyellow.png');

            this.displayList.add(ybase);
            this.updateList.add(ybase);

            return ybase;
        });

        this.sys.plugins.registerGameObject('bluebase', function(x:number, y:number){
            let bbase:ODBlueGunBase = new ODBlueGunBase(this.scene, x, y, 'ODImages', 'baseblue.png');

            this.displayList.add(bbase);
            this.updateList.add(bbase);

            return bbase;
        });

        this.sys.plugins.registerGameObject('greybase', function(x:number, y:number){
            let gbase:ODGreyGunBase = new ODGreyGunBase(this.scene, x, y, 'ODImages', 'basegrey.png');

            this.displayList.add(gbase);
            this.updateList.add(gbase);

            return gbase;
        });

        this.sys.plugins.registerGameObject('yellowgun', function(x:number, y:number){
            let ygun:ODYellowGun = new ODYellowGun(this.scene, x, y, 'ODImages', 'gunyellow.png');

            this.displayList.add(ygun);
            this.updateList.add(ygun);

            return ygun;
        });

        this.sys.plugins.registerGameObject('bluegun', function(x:number, y:number){
            let bgun:ODBlueGun = new ODBlueGun(this.scene, x, y, 'ODImages', 'gunblue.png');

            this.displayList.add(bgun);
            this.updateList.add(bgun);

            return bgun;
        });

        this.sys.plugins.registerGameObject('greygun', function(x:number, y:number){
            let ggun:ODGreyGun = new ODGreyGun(this.scene, x, y, 'ODImages', 'gungrey.png');

            this.displayList.add(ggun);
            this.updateList.add(ggun);

            return ggun;
        });

        this.sys.plugins.registerGameObject('tvmonitorempty', function(x:number, y:number){
            let tv:ODGreyGunBase = new ODGreyGunBase(this.scene, x, y, 'ODImages', 'tvmonitoremptyframed.png');

            this.displayList.add(tv);
            this.updateList.add(tv);

            return tv;
        });

        this.sys.plugins.registerGameObject('cloud1', function(x:number, y:number){
            let cloud1:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'cloud1.png');

            this.displayList.add(cloud1);
            this.updateList.add(cloud1);

            return cloud1;
        });

        this.sys.plugins.registerGameObject('cloud2', function(x:number, y:number){
            let cloud2:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'cloud2.png');

            this.displayList.add(cloud2);
            this.updateList.add(cloud2);

            return cloud2;
        });

        this.sys.plugins.registerGameObject('statictv', function(x:number, y:number){
            let statictv:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'staticmonitor-000.png');

            this.displayList.add(statictv);
            this.updateList.add(statictv);

            return statictv;
        });

        this.sys.plugins.registerGameObject('bluebutton', function(x:number, y:number){
            let bluebutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'bluebutton.png');

            this.displayList.add(bluebutton);
            this.updateList.add(bluebutton);

            return bluebutton;
        });

        this.sys.plugins.registerGameObject('greenbutton', function(x:number, y:number){
            let greenbutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'greenbutton.png');

            this.displayList.add(greenbutton);
            this.updateList.add(greenbutton);

            return greenbutton;
        });

        this.sys.plugins.registerGameObject('levellockbutton', function(x:number, y:number){
            let levellockbutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'levellockbutton.png');

            this.displayList.add(levellockbutton);
            this.updateList.add(levellockbutton);

            return levellockbutton;
        });

        this.sys.plugins.registerGameObject('backbutton', function(x:number, y:number){
            let backbutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'backbutton.png');

            this.displayList.add(backbutton);
            this.updateList.add(backbutton);

            return backbutton;
        });

        this.sys.plugins.registerGameObject('shield', function(x:number, y:number){
            let shield:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'shield.png');

            this.displayList.add(shield);
            this.updateList.add(shield);

            return shield;
        });

        this.sys.plugins.registerGameObject('pausebutton', function(x:number, y:number){
            let pausebutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'pausebutton.png');

            this.displayList.add(pausebutton);
            this.updateList.add(pausebutton);

            return pausebutton;
        });

        this.sys.plugins.registerGameObject('oceaninnerbar', function(x:number, y:number){
            let oceaninnerbar:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceaninnerbar.png');

            this.displayList.add(oceaninnerbar);
            this.updateList.add(oceaninnerbar);

            return oceaninnerbar;
        });

        this.sys.plugins.registerGameObject('oceanmiddlebar', function(x:number, y:number){
            let oceanmiddlebar:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanmiddlebar.png');

            this.displayList.add(oceanmiddlebar);
            this.updateList.add(oceanmiddlebar);

            return oceanmiddlebar;
        });

        this.sys.plugins.registerGameObject('oceanouterbar', function(x:number, y:number){
            let oceanouterbar:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'oceanouterbar.png');

            this.displayList.add(oceanouterbar);
            this.updateList.add(oceanouterbar);

            return oceanouterbar;
        });

        this.sys.plugins.registerGameObject('playbutton', function(x:number, y:number){
            let playbutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'playbutton.png');

            this.displayList.add(playbutton);
            this.updateList.add(playbutton);

            return playbutton;
        });

        this.sys.plugins.registerGameObject('replaybutton', function(x:number, y:number){
            let replaybutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'replaybutton.png');

            this.displayList.add(replaybutton);
            this.updateList.add(replaybutton);

            return replaybutton;
        });

        this.sys.plugins.registerGameObject('homebutton', function(x:number, y:number){
            let homebutton:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'homebutton.png');

            this.displayList.add(homebutton);
            this.updateList.add(homebutton);

            return homebutton;
        });

        this.sys.plugins.registerGameObject('pausepanel', function(x:number, y:number){
            let pausepanel:Phaser.GameObjects.Sprite = new Phaser.GameObjects.Sprite(this.scene, x, y, 'ODImages', 'pausepanel.png');

            this.displayList.add(pausepanel);
            this.updateList.add(pausepanel);

            return pausepanel;
        });
    }

    preload():void
    {
        this.load.multiatlas('ODImages', 'assets/ODII.json', 'assets');

        //Initial values must be set for a change event to register
        this.registry.set('Radar1', {x:-10, y:-10});
        this.registry.set('Radar2', {x:-10, y:-10});
        this.registry.set('Radar3', {x:-10, y:-10});
        this.registry.set('ActiveRadar', {});
        this.registry.set('SetMonitorExplosion', {});
        this.registry.set('TrashAtWater', -1);
        this.registry.set('MissileInFinalPosition', -1);
        this.registry.set('SetRadarBorder', {});
        this.registry.set('StartCoolDown', {});
        this.registry.set('EndCoolDown', {});
        this.registry.set('TrashDestroyed', -1);
        this.registry.set('RadarUpdateCycle1', new RadarEvent('initial1'));
        this.registry.set('RadarUpdateCycle2', new RadarEvent('initial2'));
        this.registry.set('RadarUpdateCycle3', new RadarEvent('initial3'));
        this.registry.set('GameEventRadarWon', {});
        this.registry.set('GameEventRadarLost', {});
        this.registry.set('GamePause', {});
    }

    create():void
    {
        this.anims.create({
            key: 'RedExplode',
            frames: this.anims.generateFrameNames('ODImages', {start: 4,end: 35,zeroPad: 4,prefix: 'redexplode',suffix: '.png'}),
            frameRate: 18,
            repeat: 0,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'WhiteExplode',
            frames: this.anims.generateFrameNames('ODImages', {start: 3,end: 46,zeroPad: 4,prefix: 'explodewhite',suffix: '.png'}),
            frameRate: 20,
            repeat: 0,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'Trash',
            frames: this.anims.generateFrameNames('ODImages', {start: 0,end: 14,zeroPad: 3,prefix: 'trashcollection-',suffix: '.png'}),
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'StaticTv',
            frames: this.anims.generateFrameNames('ODImages', {start:0, end: 8, zeroPad: 3, prefix: 'staticmonitor-', suffix: '.png'}),
            frameRate: 8,
            repeat: -1,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'Base',
            frames: [
                {key:'ODImages', frame:'baseyellow.png'},
                {key:'ODImages', frame:'baseblue.png'},
                {key:'ODImages', frame:'basegrey.png'}
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'Gun',
            frames: [
                {key:'ODImages', frame:'gunyellow.png'},
                {key:'ODImages', frame:'gunblue.png'},
                {key:'ODImages', frame:'gungrey.png'}
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });

        this.anims.create({
            key: 'EmptyRadars',
            frames: [
                {key:'ODImages', frame:'tvmonitoremptyframed.png'},
                {key:'ODImages', frame:'tvmonitoremptyframedgreen.png'}
            ],
            frameRate: 0,
            repeat: 0,
            hideOnComplete: false
        });

        this.SetGameInfo();
    }

    private transitionToScene(key:string):void
    {
        this.scene.start(key);
    }

    private SetGameInfo():void
    {
        //Determine if already stored in index db, if not create default
        ODCacheManager.getItem('ODGameInfo').then((data:ODGameInfo)=>{
            if (data) {
                ODGameInfo.instance = data;

                this.transitionToScene('TitleScreen');
            }
            else {
                this.SetDefaultGameInfo();
            }
        }, (error:any)=>{
            console.log('Get ODGameInfo from cache was rejected:');
            console.log(error);

            this.SetDefaultGameInfo();
        }).catch((reason:any)=>{
            console.log('Get ODGameInfo from cache exception:');
            console.log(reason);

            this.SetDefaultGameInfo();
        });
    }

    private SetDefaultGameInfo():void
    {
        ODGameInfo.instance.world.currLevel=0;
        ODGameInfo.instance.world.currPoints=0;
        ODGameInfo.instance.world.hiLevel=0;
        ODGameInfo.instance.world.hiPoints=0;
        
        let level0:ODLevelInfo = new ODLevelInfo();
        level0.cleared=true;
        level0.levelNum=0;
        level0.missileSpeed=300;
        level0.name = 'Title Screen';
        level0.pointValue=0;
        level0.unlocked = true;
        level0.radars.push(new ODRadarInfo(true,1500, 80, 150, 5, 5));
        level0.radars.push(new ODRadarInfo(true,2000, 80, 150, 5, 5));
        level0.radars.push(new ODRadarInfo(false,1500, 80, 150, 5, 5));
        ODGameInfo.instance.levels.push(level0);

        let level1:ODLevelInfo = new ODLevelInfo();
        level1.cleared=false;
        level1.levelNum=1;
        level1.missileSpeed=300;
        level1.name = 'Level 1';
        level1.pointValue = 1;
        level1.unlocked = true;
        level1.radars.push(new ODRadarInfo(true, 3500, 80, 120, 2, 5));
        level1.radars.push(new ODRadarInfo(false, 3500, 80, 120, 2, 5));
        level1.radars.push(new ODRadarInfo(false, 3500, 80, 120, 2, 5));
        ODGameInfo.instance.levels.push(level1);

        let level2:ODLevelInfo = new ODLevelInfo();
        level2.cleared=false;
        level2.levelNum=2;
        level2.missileSpeed=300;
        level2.name = 'Level 2';
        level2.pointValue = 5;
        level2.unlocked = false;
        level2.radars.push(new ODRadarInfo(true, 3000, 150, 250, 3, 5));
        level2.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        level2.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        ODGameInfo.instance.levels.push(level2);

        //Temp data that needs to be replaced with real level definitions
        let level3:ODLevelInfo = new ODLevelInfo();
        level3.cleared=false;
        level3.levelNum=3;
        level3.missileSpeed=300;
        level3.name = 'Level 3';
        level3.pointValue = 5;
        level3.unlocked = false;
        level3.radars.push(new ODRadarInfo(true, 3000, 150, 250, 3, 5));
        level3.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        level3.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        ODGameInfo.instance.levels.push(level3);

        let level4:ODLevelInfo = new ODLevelInfo();
        level4.cleared=false;
        level4.levelNum=4;
        level4.missileSpeed=300;
        level4.name = 'Level 4';
        level4.pointValue = 5;
        level4.unlocked = false;
        level4.radars.push(new ODRadarInfo(true, 3000, 150, 250, 3, 5));
        level4.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        level4.radars.push(new ODRadarInfo(false, 3500, 100, 200, 2, 5));
        ODGameInfo.instance.levels.push(level4);

        //Temp - Should be removed - allows for cache to be reloaded each time
        this.transitionToScene('TitleScreen');

        // ODCacheManager.setItem('ODGameInfo', ODGameInfo.instance).then((value:any)=>{
        //     this.transitionToScene('TitleScreen');
        // },(error:any)=>{
        //     console.log('Set ODGameInfo from cache was rejected:');
        //     console.log(error);

        //     this.transitionToScene('TitleScreen');
        // }).catch((reason:any)=>{
        //     console.log('Set ODGameInfo from cache exception:');
        //     console.log(reason);

        //     this.transitionToScene('TitleScreen');
        // });
    }
}

export default ODBoot;
