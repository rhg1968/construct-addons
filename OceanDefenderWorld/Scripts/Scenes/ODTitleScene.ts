import ODMonitor from './ODMonitor';
import ODRadar from './ODRadar';
import ODGameInfo from './../Models/ODGameInfo';
import RadarEvent from './../Models/RadarEvent';

class ODTitleScene extends Phaser.Scene
{
    private playButton:Phaser.GameObjects.Sprite;
    private monitor:ODMonitor;
    private fadeTime:number;

    constructor()
    {
        super({
            key: 'TitleScreen'
        });
    }

    init():void
    {
        this.playButton = undefined;
        this.monitor = undefined;
        this.fadeTime = 1000;
        ODGameInfo.instance.world.currLevel=0;
    }

    create():void
    {
        for(let i=0; i<3; i++) {
            let key:string = 'Radar'+(i+1).toString();
            let currLevel:number = ODGameInfo.instance.world.currLevel;

            let radar:ODRadar = new ODRadar(key, 0+(160*i), 500, 160, 212.8, (i==0)?true:false, this.fadeTime);

            this.scene.add(key, radar, true);
            
            radar.SetRadarOn(ODGameInfo.instance.levels[0].radars[i].active);
            radar.StartTimer();
        }

        this.monitor = new ODMonitor('Monitor1', 20, 80, 260, 345.8, this.fadeTime);
        this.scene.add('Monitor1', this.monitor, true);

        this.monitor.SetIgnoreInputOn(true);

        this.playButton = (this.add as any).playbutton(78,787) as Phaser.GameObjects.Sprite;
        let levelButton:Phaser.GameObjects.Sprite = this.add.sprite((this.game.config.width as number)/2.0, 787, 'ODImages', 'levelsbutton.png');
        let settingsButton:Phaser.GameObjects.Sprite = this.add.sprite(400, 787, 'ODImages', 'settingsbutton.png');
        let monitorSwitches:Phaser.GameObjects.Sprite = this.add.sprite(100, 440, 'ODImages', 'monitorswitches.png');
        let monitorLights:Phaser.GameObjects.Sprite = this.add.sprite(220, 440, 'ODImages', 'mintorlights.png').setAngle(90);
        let monitorButtons:Phaser.GameObjects.Sprite = this.add.sprite(150, 470, 'ODImages', 'monitorbuttons.png').setScale(0.75);
        let monitorMeters:Phaser.GameObjects.Sprite = this.add.sprite(350, 190, 'ODImages', 'monitormeters.png');
        let monitorPlainDial:Phaser.GameObjects.Sprite = this.add.sprite(430, 190, 'ODImages', 'monitorplaindial.png');
        let monitorBigButton:Phaser.GameObjects.Sprite = this.add.sprite(380, 320, 'ODImages', 'monitorbigbutton.png');

        levelButton.setInteractive();
        this.playButton.setInteractive();

        levelButton.on('pointerdown', (pointer:any)=>{
            this.levelButtonPressed();
        });

        this.playButton.on('pointerdown', (pointer:any)=>{
            this.playButtonPressed();
        });

        this.add.text(8, 15, 'Ocean Defender II', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 34,
            color: '#16559d'
        });

        this.add.text(285,400, 'RGBZ STUDIOS', {
            fontFamily: 'ethnocentric_rg',
            fontSize: 18,
            color: '#bf662f'
        });

        this.registry.set('ActiveRadar', new RadarEvent('Radar1'));

        this.tweens.timeline({
            targets: [this.playButton],
            loop: -1,
            loopDelay: 2000,
            tweens: [
                {
                    angle: -25,
                    ease: 'Sine.easeInOut',
                    duration: 200
                },
                {
                    offset: 200,
                    angle: 25,
                    ease: 'Sine.easeInOut',
                    duration: 400
                },
                {
                    offset: 600,
                    angle: 0,
                    ease: 'Sine.easeInOut',
                    duration: 200
                }
            ]
        });

        this.time.addEvent({
            delay: 1000,
            loop: true,
            callback: ()=>{
                this.monitor.DemoCreateMissile();
            }
        });

        this.cameras.main.fadeIn(this.fadeTime, ODGameInfo.instance.world.fadeColor.red, ODGameInfo.instance.world.fadeColor.green, ODGameInfo.instance.world.fadeColor.blue);
    }

    update(time:number, dt:number)
    {
        //console.log(dt);
    }

    private levelButtonPressed():void
    {
        this.transitionToScene('LevelSelection');
    }

    private playButtonPressed():void
    {
        console.log('Furthest level unlocked: ' + ODGameInfo.GetFurthestLevelUnlocked().toString());
        ODGameInfo.instance.world.currLevel = ODGameInfo.GetFurthestLevelUnlocked();
        this.transitionToScene('GameScreen');
    }

    private transitionToScene(key:string):void
    {
        this.scene.remove('Radar1');
        this.scene.remove('Radar2');
        this.scene.remove('Radar3');
        this.scene.remove('Monitor1');
        this.registry.events.off('changedata', undefined, undefined, false);
        this.scene.start(key, {resetScore:true});
    }
}

export default ODTitleScene;
