"use strict";

{
	const PLUGIN_CLASS = SDK.Plugins.RGBZ_JsonManager;
	
	PLUGIN_CLASS.Instance = class RGBZ_JsonManagerInstance extends SDK.IInstanceBase
	{
		constructor(sdkType, inst)
		{
			super(sdkType, inst);
		}
		
		Release()
		{
		}
		
		OnCreate()
		{
		}
		
		OnPropertyChanged(id, value)
		{
		}
		
		LoadC2Property(name, valueString)
		{
			return false;		// not handled
		}
	};
}