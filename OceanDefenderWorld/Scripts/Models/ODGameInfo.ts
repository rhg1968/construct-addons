import ODWorldInfo from './GameInfoModels/ODWorldInfo';
import ODLevelInfo from './GameInfoModels/ODLevelInfo';

class ODGameInfo
{
    public world:ODWorldInfo;
    public levels:Array<ODLevelInfo>;
    public static instance:ODGameInfo = new ODGameInfo();

    constructor()
    {
        this.world = new ODWorldInfo();
        this.levels = new Array<ODLevelInfo>();
    }

    public static GetFurthestLevelUnlocked():number
    {
        let foundLevel:number=1;

        for (let level of this.instance.levels)
        {
            if (level.unlocked)
            {
                foundLevel=level.levelNum;
            }
        }

        return foundLevel;
    }
}

export default ODGameInfo;
