"use strict";

{
	const PLUGIN_CLASS = SDK.Plugins.Rex_WaitEvent;
	PLUGIN_CLASS.Type = class Rex_WaitEventType extends SDK.ITypeBase
	{
		constructor(sdkPlugin, iObjectType)
		{
			super(sdkPlugin, iObjectType);
		}
	};
}