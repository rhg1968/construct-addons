"use strict";

{
	C3.Plugins.RGBZ_JsonManager.Exps =
	{
		JsonLoopIndex ()
		{
			let result = this.loopIndexStack.peek();

			return result;
		},
		GetJsonArrayLength (slot, query, withinExistingLoop)
		{
			let result = 0;

			let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);
	
			if (currentPosition && Array.isArray(currentPosition)) {
				result = currentPosition.length;
			}
	
			return result;
		},
		GetJsonValue (slot, query, withinExistingLoop)
		{
			let result = '';

			let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);
	
			if (typeof(currentPosition)!=='undefined') {
				result = currentPosition.toString();
			}
	
			return result;
		},
		GetArrayFromJsonPath (slot, query, withinExistingLoop)
		{
			let result = '';

			let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);
	
			if (currentPosition) {
				let arrayDims = getArrayDimemsions(currentPosition);
	
				if (arrayDims && arrayDims.length>0) {
					let resultObj = {
						"c2array": true,
						"size": arrayDims,
						"data": populateC3Array(currentPosition, arrayDims)
					};
	
					result = JSON.stringify(resultObj);
				}
			}
	
			return result;
		},
		GetDictionaryFromJsonPath (slot, query, withinExistingLoop)
		{
			let result = '';

			let currentPosition = (withinExistingLoop.toLowerCase().trim()=='yes')?getJsonObjectWorker(this.lastJsonObjectSelectedStack.peek(),query):getJsonObject(this.jsonSlots, slot, query);
	
			if (currentPosition) {
				let resultObj = {
					"c2dictionary": true,
					"data": {}
				};
	
				for (let key of Object.keys(currentPosition)) {
					let testType = typeof(currentPosition[key]);
	
					if (testType==='number' || testType==='string') {
						resultObj.data[key] = currentPosition[key];
					}
					else {
						break;
					}
				}
	
				result = JSON.stringify(resultObj);
			}
	
			return result;
		}
	};
}