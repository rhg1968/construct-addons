"use strict";

{
	C3.Plugins.RGBZ_JsonManager.Instance = class RGBZ_JsonManagerInstance extends C3.SDKInstanceBase
	{
		constructor(inst, properties)
		{
			super(inst);

			//Set up storage of json slots
			this.jsonSlots = {};
			this.lastJsonObjectSelectedStack = new rgbzStack();
			this.loopIndexStack = new rgbzStack();
			
			// Initialise object properties
			this._testProperty = 0;
			this.rgbzJsonFiles = '';
			this.rgbzJsonSlots = '';
			
			if (properties && properties.length==2)		// note properties may be null in some cases
			{
				this.rgbzJsonFiles = properties[0];
				this.rgbzJsonSlots = properties[1];
			}

			initializeJSMDB(inst.GetRuntime().GetProjectName());
		}
		
		Release()
		{
			super.Release();
		}
		
		SaveToJson()
		{
			return {
				// data to be saved for savegames
			};
		}
		
		LoadFromJson(o)
		{
			// load state for savegames
		}
	};
}