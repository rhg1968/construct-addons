var rgbzGameInfoD:any = {
  "game": {
		"recycletokens": 0,
    "hiscore": 0,
    "maxlevelcompleted": 0,
    "currlevel": 0,
	"currplanet": "",
	"currbaseindex": 0,
	"guncooldowntimer": 0.4
  },
  "levels": [
      {
        "id": 0,
        "name": "Title Screen",
		"cleared": "true",
		"unlocked": "true",
		"tokens": 5,
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 2,
				  "minspeed": 150,
				  "maxspeed": 250,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 2,
				  "minspeed": 150,
				  "maxspeed": 250,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 2,
				  "minspeed": 150,
				  "maxspeed": 250,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 1,
        "name": "Level 1",
		"cleared": "false",
		"unlocked": "true",
		"tokens": 1,
        "bases": {
          "maxnummissiles": 4
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3.5,
				  "minspeed": 100,
				  "maxspeed": 200,
				  "trashtoclear": 2
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 4,
				  "minspeed": 150,
				  "maxspeed": 250,
				  "trashtoclear": 1
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3.5,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 3
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 2,
        "name": "Level 2",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 150,
				  "maxspeed": 250,
				  "trashtoclear": 3
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 3,
        "name": "Level 3",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 4,
        "name": "Level 4",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 5,
        "name": "Level 5",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 6,
        "name": "Level 6",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 7,
        "name": "Level 7",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 8,
        "name": "Level 8",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 9,
        "name": "Level 9",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 10,
        "name": "Level 10",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 11,
        "name": "Level 11",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      },
      {
        "id": 12,
        "name": "Level 12",
		"cleared": "false",
		"unlocked": "false",
		"tokens": 5,
        "bases": {
          "maxnummissiles": 10
        },
        "missiles": {
          "speed": 300
        },
        "clouds": {
		  "active": "true",
          "minspeed": 8,
          "maxspeed": 40
        },
        "fish": {
		  "active": "true",
          "minspeed": 15,
          "maxspeed": 30
        },
		"monitors": [
			{
				"active": "true",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			},
			{
				"active": "false",
				"trash": {
				  "spawntime": 3,
				  "minspeed": 200,
				  "maxspeed": 300,
				  "trashtoclear": 5
				},
				"ocean": {
					"maxTrash": 5
				}
			}
		]
      }
  ]
}