import ODMissileMaker from './../GameObjects/ODMissileMaker';
import ODState from './../Enums/ODState';
import ODTrashMarker from './../GameObjects/ODTrashMarker';
import ODExplosionMarker from './../GameObjects/ODExplosionMarker';
import MonitorClickEvent from './../Models/MonitorClickEvent';
import ODGameInfo from './../Models/ODGameInfo';
import RadarEvent from './../Models/RadarEvent';
import RadarDetailEvent from './../Models/RadarDetailEvent';
import GameEvent from './../Models/GameEvent';

class ODRadar extends Phaser.Scene
{
    private missileMarkerPool:Phaser.GameObjects.Group;
    private trashMarkerPool:Phaser.GameObjects.Group;
    private explosionMarkerPool:Phaser.GameObjects.Group;
    private cloudPool:Phaser.GameObjects.Group;
    private monitor: Phaser.GameObjects.Sprite;
    private statictv:Phaser.GameObjects.Sprite;
    private radarOn:boolean;
    private fadeTime:number;
    private currLevel:number;
    private radarIndex:number;
    private oceanHealth:number;
    private oceanHealthMax:number;
    private trashTimer:number;
    private trashToClear:number;
    private trashMinSpeed:number;
    private trashMaxSpeed:number;
    private missileSpeed:number;
    private coolDownTime:number;
    private canFire:boolean;
    private flashTimer:Phaser.Time.TimerEvent;
    private coolDownTimer:Phaser.Time.TimerEvent;
    private oceanoutterbar:Phaser.GameObjects.Sprite;
    private oceaninnerbar:Phaser.GameObjects.Sprite;
    private healthRatio:number;

    constructor(public key:string, public xPos:number, public yPos:number, public displayWidth:number,
        public displayHeight:number, public isHighlighted:boolean, fadeTime:number){
        super({
            key
        });

        this.radarOn = true;
        this.fadeTime = fadeTime;
    }

    init():void
    {
        this.currLevel = ODGameInfo.instance.world.currLevel;
        this.radarIndex = parseInt(this.key[this.key.length-1]) - 1;
        this.coolDownTime = ODGameInfo.instance.levels[this.currLevel].coolDownTimer;
        this.oceanHealth = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].oceanMaxDamage;
        this.oceanHealthMax = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].oceanMaxDamage;
        this.trashTimer = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].trashSpawnTime;
        this.trashToClear = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].trashToClear;
        this.trashMinSpeed = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].trashMinSpeed;
        this.trashMaxSpeed = ODGameInfo.instance.levels[this.currLevel].radars[this.radarIndex].trashMaxSpeed;
        this.missileSpeed = ODGameInfo.instance.levels[this.currLevel].missileSpeed;
        this.flashTimer = undefined;
        this.coolDownTimer = undefined;
        this.canFire = true;
    }

    preload():void
    {
        this.explosionMarkerPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'explosionmarker.png',
            classType: ODExplosionMarker as any,
            runChildUpdate: true
        });

        this.missileMarkerPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'missilemarker.png',
            classType: ODMissileMaker as any,
            createCallback: (missile:any)=>{},
            removeCallback: (missile:any)=>{},
            runChildUpdate: false
        });

        this.trashMarkerPool = this.add.group({
            maxSize: 20,
            defaultKey: 'ODImages',
            defaultFrame: 'trashmarker.png',
            classType: ODTrashMarker as any,
            createCallback: (trash:any)=>{},
            removeCallback: (trash:any)=>{},
            runChildUpdate: false
        });

        this.cloudPool = this.add.group({
            maxSize: 10,
            defaultKey: 'ODImages',
            defaultFrame: 'cloud1.png',
            classType: Phaser.GameObjects.Sprite as any,
            createCallback: (trash:any)=>{},
            removeCallback: (trash:any)=>{},
            runChildUpdate: false
        });

        this.cameras.main.fadeIn(this.fadeTime, ODGameInfo.instance.world.fadeColor.red, ODGameInfo.instance.world.fadeColor.green, ODGameInfo.instance.world.fadeColor.blue);
    }

    create():void
    {
        this.cameras.main.setViewport(this.xPos, this.yPos, this.displayWidth, this.displayHeight);

        (this.add as any).sky((this.displayWidth/2.0),(this.displayHeight/2.0)).setDisplaySize(this.displayWidth, this.displayHeight);

        this.cloudPool.addMultiple([
            (new Phaser.GameObjects.Sprite(this, 92.31, 123.08, 'ODImages', 'cloud1.png')).setScale(0.15,0.10),
            (new Phaser.GameObjects.Sprite(this, 49.23, 36.92, 'ODImages', 'cloud1.png')).setScale(0.10,0.15),
            (new Phaser.GameObjects.Sprite(this, 24.62, 73.85, 'ODImages', 'cloud1.png')).setScale(0.05,0.05),
            (new Phaser.GameObjects.Sprite(this, 123.08, 36.92, 'ODImages', 'cloud1.png')).setScale(0.10,0.05),
            (new Phaser.GameObjects.Sprite(this, 61.54, 61.54, 'ODImages', 'cloud2.png')).setScale(0.15,0.10),
            (new Phaser.GameObjects.Sprite(this, 123.08, 73.85, 'ODImages', 'cloud2.png')).setScale(0.25,0.10),
            (new Phaser.GameObjects.Sprite(this, 43.08, 98.46, 'ODImages', 'cloud2.png')).setScale(0.05,0.10),
            (new Phaser.GameObjects.Sprite(this, 123.08, 104.62, 'ODImages', 'cloud2.png')).setScale(0.05,0.05)
        ], true);

        (this.add as any).ocean((this.displayWidth/2.0), this.displayHeight-20).setDisplaySize(this.displayWidth-20, 25).setDepth(1);

        //Add monitor graphic and set to interactive
        this.monitor = (this.add as any).tvmonitorempty(0, 0);
        this.monitor.setDisplaySize(this.displayWidth, this.displayHeight).setDepth(2).setInteractive().play('EmptyRadars', false, (this.isHighlighted)?1:0).on('pointerdown', (pointer:any)=>{
            if (this.radarOn) {
                this.registry.set('ActiveRadar', new RadarEvent(this.key));
                this.registry.set('SetRadarBorder', new MonitorClickEvent(this.key));
            }
        });

        this.statictv = ((this.add as any).statictv(0,0) as Phaser.GameObjects.Sprite).setDisplaySize(this.displayWidth, this.displayHeight).setDepth(3).setActive(false).setVisible(false);

        this.oceanoutterbar = ((this.add as any).oceanouterbar((this.displayWidth/2.0)-35, this.displayHeight-18) as Phaser.GameObjects.Sprite).setDisplaySize(70.84, 10.08).setDepth(1);
        (((this.add)as any).oceanmiddlebar(this.oceanoutterbar.x, this.oceanoutterbar.y)as Phaser.GameObjects.Sprite).setDisplaySize(this.oceanoutterbar.displayWidth, this.oceanoutterbar.displayHeight).setDepth(1);
        this.oceaninnerbar = ((this.add as any).oceaninnerbar(this.oceanoutterbar.x+2, this.oceanoutterbar.y+2) as Phaser.GameObjects.Sprite).setDisplaySize(this.oceanoutterbar.displayWidth-4, this.oceanoutterbar.displayHeight-4).setDepth(1);

        this.healthRatio = this.oceanHealth/this.oceanHealthMax;

        this.registry.events.on('changedata', (parent:any, key:string, data:MonitorClickEvent)=>{
            if (this.radarOn) {
                switch(key)
                {
                    case this.key:
                        this.createMissile(key, data);
                        break;
                    case 'SetRadarBorder':
                        this.monitor.play('EmptyRadars', false, (data.key==this.key)?1:0);
                        break;
                    case 'TrashDestroyed':
                        if (this.currLevel>0 && data.key==this.key){
                            this.trashToClear-=1;

                            if (this.trashToClear==0) {
                                this.SetRadarOn(false);
                                this.registry.set('GameEventRadarWon', new GameEvent(this.key));
                            }
                        }
                        break;
                }
            }
        });
    }

    public StartTimer():void {
        this.time.addEvent({
            delay: this.trashTimer,
            loop: true,
            callback: ()=>{
                if (this.radarOn) {
                    this.createTrash();
                }
            }
        });
    }

    public SetRadarOn(isOn:boolean):void {
        this.radarOn = isOn;

        if (this.radarOn) {
            this.statictv.setActive(false).setVisible(false);
        }
        else {
            this.statictv.setActive(true).setVisible(true).play('StaticTv', false, Phaser.Math.RND.integerInRange(0,6));
        }
    }

    public GetRadarOn():boolean {
        return this.radarOn;
    }

    public GetTrashMarkerPool():Phaser.GameObjects.Group
    {
        return this.trashMarkerPool;
    }

    private createTrash():void
    {
        let marker:ODTrashMarker = (this.trashMarkerPool.get(0,0) as ODTrashMarker).setScale(0.35);
        marker.SetId();
        marker.tag=this.key;

        marker.speed = Phaser.Math.RND.realInRange(this.trashMinSpeed, this.trashMaxSpeed);

        marker.setXY(Phaser.Math.RND.realInRange(15, this.displayWidth-15), 10, 
            Phaser.Math.RND.realInRange(15, this.displayWidth-15), this.displayHeight-30);

        marker.setODState(ODState.VisibleActive);
    }

    private RunCoolDownTimer():void
    {
        this.registry.set('StartCoolDown', new RadarEvent(this.key));

        this.coolDownTimer = this.time.addEvent({
            delay: this.coolDownTime,
            callback: ()=>{
                this.registry.set('EndCoolDown', new RadarEvent(this.key));
                this.canFire = true;
                this.coolDownTimer.destroy();
            }
        });
    }

    private createMissile(radarName:string, event:MonitorClickEvent):void
    {
        if (this.canFire || this.currLevel==0) {
            let marker:ODMissileMaker = (this.missileMarkerPool.get(0,0) as ODMissileMaker).setScale(0.35);
            marker.SetId();
            marker.tag = this.key;
            
            marker.speed = this.missileSpeed;
            
            marker.setXY(event.initialX*(this.displayWidth/event.monitorWidth), 
                event.initialY*(this.displayHeight/event.monitorHeight), 
                event.finalX*(this.displayWidth/event.monitorWidth),
                event.finalY*(this.displayHeight/event.monitorHeight));

            marker.setODState(ODState.VisibleActive);

            if (this.currLevel>0) {
                this.canFire = false;
                this.RunCoolDownTimer();
            }
        }
    }

    private CreateExplosion(x:number, y:number):void {
        let marker:ODExplosionMarker = (this.explosionMarkerPool.get(x, y) as ODExplosionMarker).setScale(0.25);
        marker.SetId();
        marker.tag = this.key;

        marker.setODState(ODState.VisibleActive);

        let event:RadarEvent = new RadarEvent(this.key, this.trashToClear, this.healthRatio);
        event.missiles.push(new RadarDetailEvent(marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));

        this.registry.set('SetMonitorExplosion', event);

        this.time.addEvent({
            delay: 1900,
            loop: false,
            repeat: 0,
            callback: ()=>{
                marker.setODState(ODState.NotVisibleActive)
            }
        });
    }

    update(time:number, dt:number)
    {
        if (this.radarOn) {
            let radarUpdateInfo:RadarEvent = new RadarEvent(this.key, this.trashToClear, this.healthRatio);

            Phaser.Actions.Call(this.trashMarkerPool.getChildren().filter((x:ODTrashMarker)=>x.active==true), (marker:ODTrashMarker)=>{
                marker.x += (marker.direction.x*marker.speed*(dt/1000));
                marker.y += (marker.direction.y*marker.speed*(dt/1000));
        
                if (marker.y>=marker.finalY) {
                    this.ProcessTrashAtWater(marker);
                }
                else {
                    //Broadcast trash position
                    radarUpdateInfo.trash.push(new RadarDetailEvent(marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));
                }
            }, this);

            Phaser.Actions.Call(this.missileMarkerPool.getChildren().filter((x:ODMissileMaker)=>x.active==true), (marker:ODMissileMaker)=>{
                marker.x += (marker.direction.x*marker.speed*(dt/1000));
                marker.y += (marker.direction.y*marker.speed*(dt/1000));
        
                if (marker.y<=marker.finalY) {
                    this.ProcessMissileInFinalPosition(marker);
                }
                else {
                    //Broadcast missile position
                    radarUpdateInfo.missiles.push(new RadarDetailEvent(marker.id, marker.x, marker.y, this.displayWidth, this.displayHeight));
                }
            }, this);

            let counter:number = 0;
            Phaser.Actions.Call(this.cloudPool.getChildren(), (cloud:Phaser.GameObjects.Sprite)=>{
                cloud.x += (((dt/1000)*4.92)+ (0.04*counter));
    
                if (cloud.x - (cloud.displayWidth/2.0) > this.displayWidth) {
                    cloud.x = -cloud.displayWidth/2.0;
                }
    
                counter+=1;
            }, this);

            this.registry.set('RadarUpdateCycle' + this.key[this.key.length-1], radarUpdateInfo);
        }
    }

    private ProcessTrashAtWater(marker:ODTrashMarker):void
    {
        let event:RadarEvent = new RadarEvent(this.key,this.trashToClear,this.healthRatio);
        event.trash.push(new RadarDetailEvent(marker.id));

        this.registry.set('TrashAtWater', event);

        marker.setODState(ODState.NotVisibleActive);

        if(this.currLevel>0)
        {
            this.oceanHealth-=1;

            this.healthRatio = this.oceanHealth/this.oceanHealthMax;
            this.oceaninnerbar.displayWidth = (this.oceanoutterbar.displayWidth-4)*this.healthRatio;

            if (this.oceanHealth==2)
            {
                this.flashTimer = this.time.addEvent({
                    delay: 1000,
                    repeat: -1,
                    callback: ()=>{
                        this.cameras.main.flash(200, 255, 0, 0, false);
                    }
                });
            }
            else if (this.oceanHealth==0)
            {
                if (this.flashTimer)
                {
                    this.flashTimer.destroy();
                    this.flashTimer=undefined;
                }

                this.SetRadarOn(false);
                this.registry.set('GameEventRadarLost', new GameEvent(this.key));
            }
        }
    }

    private ProcessMissileInFinalPosition(marker:ODMissileMaker):void
    {
        let event:RadarEvent = new RadarEvent(this.key);
        event.missiles.push(new RadarDetailEvent(marker.id));

        this.registry.set('MissileInFinalPosition', event);

        marker.setODState(ODState.NotVisibleActive);

        this.CreateExplosion(marker.x, marker.y);
    }
}

export default ODRadar;
