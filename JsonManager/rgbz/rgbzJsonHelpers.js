let getJsonObjectWorker = (currentPosition, query)=>{
    if (typeof(currentPosition)==='undefined' ||!currentPosition) {
        logMessage("Item to query is undefined");

        return undefined;
    }

    let objectDotComponents = query.trim().split('.');

    for (let dotComponent of objectDotComponents) {
        if (!dotComponent.includes('[')) {
            if (dotComponent!='') {
                currentPosition = currentPosition[dotComponent];
            }

            if (objectIsNotDefined(currentPosition)) {
                logMessage('Json parse - Object property not defined.');
                currentPosition=undefined;
                break;
            }
        }
        else {
            //This is an array item, so handle it
            var arraySplit = dotComponent.split(/[\[\]]+/);

            currentPosition = currentPosition[arraySplit[0]];

            if (objectIsNotDefined(currentPosition)) {
                logMessage('Json parse - Object array property not defined.');
                currentPosition=undefined;
                break;
            }

            let exitLoop = false;

            for (var i = 1; i < (arraySplit.length - 1); i++) {
                currentPosition = currentPosition[arraySplit[i]];

                if (objectIsNotDefined(currentPosition)) {
                    exitLoop = true;
                    logMessage('Json parse - Object array subcomponents property not defined.');
                    currentPosition=undefined;
                    break;
                }

                if (exitLoop) {
                    break;
                }
            }
        }
    }

    return currentPosition;
};

let getJsonObject = (allSlots, slot, query)=>{
    if (slot.trim()=='' || objectIsNotDefined(allSlots[slot.trim()])){
        logMessage('slot and query not defined');
        return undefined;
    }

    return getJsonObjectWorker(allSlots[slot.trim()], query);
};
