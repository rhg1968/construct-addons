"use strict";

const C3 = globalThis.C3;

C3.Plugins.RGBZ_PubSub.Acts =
{
	_event: null,
	CreateEvent(name)
	{
		if (this.ConsoleLogging) console.log(`Create Event called.`);
		this._CreateEvent(name);
	},
	AddEventParameter(item)
	{
		if (this.ConsoleLogging) console.log(`Add parmeter called.`);
		this._AddEventParameter(item);
	},
	PublishEvent()
	{
		if (this.ConsoleLogging) console.log(`Publish all called.`);
		this._PublishEvent();
	}
};
