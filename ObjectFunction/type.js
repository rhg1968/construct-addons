"use strict";

{
	const BEHAVIOR_CLASS = SDK.Behaviors.RGBZ_ObjectFunction;
	
	BEHAVIOR_CLASS.Type = class RGBZ_ObjectFunctionType extends SDK.IBehaviorTypeBase
	{
		constructor(sdkPlugin, iBehaviorType)
		{
			super(sdkPlugin, iBehaviorType);
		}
	};
}