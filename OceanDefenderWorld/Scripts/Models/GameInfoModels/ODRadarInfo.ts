class ODRadarInfo
{
    constructor(public active:boolean, public trashSpawnTime:number, public trashMinSpeed:number,
        public trashMaxSpeed:number, public trashToClear:number, public oceanMaxDamage:number)
    {

    }
}

export default ODRadarInfo;
